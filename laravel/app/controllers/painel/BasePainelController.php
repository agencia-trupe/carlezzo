<?php

namespace Painel;

use \View, \ContatoRecebido;

class BasePainelController extends \Controller {

    protected $layout = 'painel.common.template';

    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $recebidos_count = ContatoRecebido::naoLidos()->count();
            View::share(compact('recebidos_count'));
            $this->layout = View::make($this->layout);
        }
    }

    protected function view($path, array $data = [])
    {
        $this->layout->content = View::make($path, $data);
    }

}
