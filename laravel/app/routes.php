<?php

// Front-end

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('escritorio/{slug?}', ['as' => 'escritorio', 'uses' => 'EscritorioController@index']);
Route::get('atuacao/{slug?}', ['as' => 'atuacao', 'uses' => 'AtuacaoController@index']);
Route::get('equipe/{id?}', ['as' => 'equipe', 'uses' => 'EquipeController@index']);

Route::get('noticias/equipe/{id}', ['as' => 'noticias.equipe', 'uses' => 'NoticiasController@equipe']);
Route::get('noticias/{slug?}', ['as' => 'noticias', 'uses' => 'NoticiasController@index']);

Route::get('corporativo/equipe/{id}', ['as' => 'corporativo.equipe', 'uses' => 'CorporativoController@equipe']);
Route::get('corporativo/{slug?}', ['as' => 'corporativo', 'uses' => 'CorporativoController@index']);

Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);

Route::any('busca', ['as' => 'busca', 'uses' => 'BuscaController@index']);


// Idiomas

Route::get('idioma/{sigla_idioma?}', ['as' => 'idioma', function($sigla_idioma){
    if ($sigla_idioma == 'pt' || $sigla_idioma == 'es' || $sigla_idioma == 'en') {
        Session::put('locale', $sigla_idioma);
    }

    return Redirect::to(URL::previous());
}]);


// Painel

Route::get('painel', [
    'before' => 'auth',
    'as'     => 'painel.home',
    'uses'   => 'Painel\HomeController@index'
]);

Route::get('painel/login', [
    'as'   => 'painel.login',
    'uses' => 'Painel\HomeController@login'
]);
Route::post('painel/login', [
    'as'   => 'painel.auth',
    'uses' => 'Painel\HomeController@attempt'
]);
Route::get('painel/logout', [
    'as'   => 'painel.logout',
    'uses' => 'Painel\HomeController@logout'
]);

Route::group(['prefix' => 'painel', 'before' => 'auth'], function() {
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::resource('banners', 'Painel\BannersController');
    Route::resource('escritorio', 'Painel\EscritorioController');

    Route::resource('atuacao/pratica', 'Painel\PraticaController');
    Route::resource('atuacao/fazemos', 'Painel\FazemosController');
    Route::resource('atuacao', 'Painel\AtuacaoController');

    Route::resource('equipe', 'Painel\EquipeController');

    Route::resource('noticias/corporativo', 'Painel\CorporativoController');
    Route::resource('noticias', 'Painel\NoticiasController');

    Route::resource('contato/recebidos', 'Painel\ContatosRecebidosController');
    Route::resource('contato', 'Painel\ContatoController');

    Route::resource('usuarios', 'Painel\UsuariosController');

    Route::post('ajax/order', 'Painel\AjaxController@order');
    Route::post('ckeditor-upload', 'Painel\AjaxController@imageUpload');
});
