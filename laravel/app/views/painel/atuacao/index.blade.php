@section('content')

    <legend>
        <h2><small>Atuação /</small> Atuação</h2>
    </legend>

    {{ Form::model($atuacao, [
        'route' => ['painel.atuacao.update', $atuacao->id],
        'method' => 'patch'])
    }}

        @include('painel.atuacao._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
