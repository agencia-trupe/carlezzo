@section('content')

    <legend>
        <h2>Adicionar Equipe</h2>
    </legend>

    {{ Form::open(['route' => 'painel.equipe.store', 'files' => true]) }}

        @include('painel.equipe._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
