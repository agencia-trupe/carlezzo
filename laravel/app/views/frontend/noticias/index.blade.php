@section('content')

    <div class="menu-sub">
        <a href="{{ route('noticias') }}" class="active">{{ Lang::get('frontend.noticias.noticias') }}</a>
        <a href="{{ route('corporativo') }}">{{ Lang::get('frontend.noticias.corporativo') }}</a>
    </div>

    <div class="conteudo-sub noticias-home">
        <div class="titulo">
            <h2>{{ Lang::get('frontend.noticias.noticias') }}</h2>
            @if(isset($membro))
            <span>{{ Lang::get('frontend.noticias.por') }} {{ $membro }}</span>
            @endif
        </div>

        <div class="conteudo conteudo-noticias">
        @if(count($noticias))
        @foreach($noticias as $noticia)
            <div class="noticia">
                <p class="info">
                    {{ $noticia->data }}
                    @if($noticia->equipe)
                    {{ Lang::get('frontend.noticias.por') }} <a href="{{ route('equipe', $noticia->equipe['id']) }}">{{ $noticia->equipe['nome'] }}</a>
                    @endif
                </p>
                <a href="{{ route('noticias', $noticia->slug) }}" class="noticia-link">
                    <h2>{{ $noticia->titulo }}</h2>
                    <p class="texto">{{ \Tools::cropText(strip_tags($noticia->texto), 250) }}</p>
                    <p class="leiamais">{{ Lang::get('frontend.noticias.leiamais') }} <span>»</span></p>
                </a>
            </div>
        @endforeach

            {{ $noticias->links() }}
        @else
            <p>{{ Lang::get('frontend.noticias.nenhum') }}</p>
        @endif
        </div>
    </div>

@stop
