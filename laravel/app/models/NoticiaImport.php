<?php

class NoticiaImport extends Eloquent
{

    protected $table = 'noticias_import';

    protected $hidden = [];

    protected $guarded = ['id'];

}
