<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatosRecebidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contatos_recebidos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('lido')->default('false');

			$table->string('nome');
			$table->string('email');
			$table->string('telefone');
			$table->text('mensagem');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contatos_recebidos');
	}

}
