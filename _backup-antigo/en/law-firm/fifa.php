<?php
	
	//-------------------- Conecta com o Banco de Dados ---------------------------
	include "../../_includes/conexao.php";	
	
	//Busca not�cias
	$busca_noticias = mysql_query("SELECT * FROM noticias WHERE status = '1' AND titulo_en != '0'  AND destaque != '1' ORDER BY data DESC LIMIT 0, 2");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- META TAGS - AG�NCIA 10CLIC - OTIMIZA��O -->
<meta name="author" content="10Clic - www.10clic.com.br" />
<meta name="description" content="Law firm specialized in sports, entertainment and media, with strong presence in football and international affairs, as well as before FIFA and CBF
" />
<meta name="keywords" content="FIFA, UEFA, Court of arbitration for sport, football, player's agents, athletes, clubs, solidarity mechanism, training compensation, transfers, cinema, sports law, music, concerts, models, celebrities, video-game, media, new technologies, intellectual property, advertising, brands, marketing, Internet, TV, radio, mobile, world cup, Olympic games
" />
<meta name="robots" content="index, follow, archive" />
<meta name="googlebot" content="archive" />
<!-- FIM DAS META TAGS -->
<title>Carlezzo Advogados - Fifa</title>
<!-- FOLHAS DE ESTILO -->
<link href="../../_css/estilo.css" rel="stylesheet" type="text/css" />
<link href="../../_css/nivo-slider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../_ajax/skin.css" rel="stylesheet" type="text/css" />
<!-- FIM DAS FOLHAS DE ESTILO -->
<!-- SCRIPTS -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'49de9f94-6555-4ec3-ba64-437c8a161ebe'});</script>
<script type="text/javascript" src="../../_ajax/jquery.js"></script> 
<script type="text/javascript" src="../../_ajax/menu.jquery.js"></script> 
<!-- FIM DOS SCRIPTS -->
</head>

<body class="interna">
<!-- DEFESA -->
<div id="defesa">
  <!-- TOPO -->
  <div id="topo">
  	
    <!-- LINHA -->  
    <div id="linha">
    
    <form action="../busca.php" method="post" id="busca">
    <input class="campo" name="palavra" type="text" />
    <input name="" type="image" value="ok" src="../../_imagens/botao_buscar.jpg" alt="Search" class="botao" />
    </form>
    <span displayText="" class="st_sharethis_custom"></span>
    <p class="linguas">
    <a href="../pt/<?=basename($_SERVER['PHP_SELF']);?>" title="Portugu&ecirc;s">Portugu&ecirc;s</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" title="Espa&ntilde;ol">Espa&ntilde;ol</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:window.print()" title="Print this page"><img src="../../_imagens/a_print.jpg" alt="Print this page" align="absmiddle" /></a>&nbsp;&nbsp;<a href="mailto:carlezzo@carlezzo.com.br" title="Quick contact"><img src="../../_imagens/a_mail.jpg" alt="Quick contact" align="absmiddle" /></a>
    </p>    
    </div>
    <!-- FIM DA LINHA -->
    
    <!-- LOGO -->
    <h1><a href="../index.php" title="Carlezzo Advogados">Carlezzo Advogados</a></h1>
    <!-- FIM DO LOGO -->
    
    <!-- MENU TOPO -->
    <div id="menu_topo">
    	<ul class="topnav">
            <li><a href="../index.php">Home</a></li>
            <li>
                <a href="../quem-somos.php">About us</a>

          <ul class="subnav">
                    <li><a href="../quem-somos.php">Overview</a></li>
                    <li><a href="../missao-e-valores.php">Mission and Values</a></li>
                    <li><a href="../o-que-esperar-de-nos.php">What you can wait from us</a></li>
                    <li><a href="../aliancas-internacionais.php">International Alliances</a></li>
                    <li><a href="../recrutamento.php">Recruitment</a></li>
                    <li><a href="../pro-bono.php">Pro-bono</a></li>
              </ul>
          </li>
            <li>
                <a href="../setores.php">Sectors</a>
          <ul class="subnav">
                    <li><a href="../setores.php?categoria=esporte">Sport</a></li>
     				<li><a href="../setores.php?categoria=entretenimento">Entertainment</a></li>
      				<li><a href="../setores.php?categoria=midia">Media and New Technologies</a></li>
              </ul>
          </li>
          <li><a href="../equipe.php">Team</a></li>

          <li><a href="../noticias.php">News</a></li>
          <li><a href="../publicacoes.php">Publications</a></li>
          <li class="last"><a href="../onde-estamos.php">Where are we</a></li>
      </ul>
    </div>
    <!-- FIM DO MENU TOPO -->
  
  </div>
  <!-- FIM DO TOPO -->
  
  <!-- CONTE�DO -->
  <div id="conteudo">
    
    <!-- ESQUERDA -->
    <div class="esquerda_interna">
    
    <div class="bloco_sidebar">
      <h2 class="titulo">CARLEZZO ADVOGADOS</h2>
      <ul class="menu_esquerda">
    <li><a href="football.php">Football</a></li>
	<li><a href="fifa.php">FIFA</a></li>
	<li><a href="world-cup-fifa-2014.php">World Cup FIFA 2014</a></li>
	<li><a href="olympic-games-rio-2016.php">Olympic Games Rio 2016</a></li>
	<li><a href="entertainment.php">Entertainment</a></li>
        </ul>
    </div>
    
    
    <!-- CHAMADA -->
    <div class="chamada"><img src="../../_imagens/chamada_quem_somos.jpg" width="242" height="318" /></div>
    <!-- FIM DA CHAMADA -->
        
    <br class="limpa_float" />
    </div>
    <!-- FIM DA ESQUERDA --> 
    
    <!-- DIREITA -->
    <div class="direita_interna">
    
    <!-- TEXTO -->
    <div class="texto">
    <h5 class="square">Fifa</h5>
    <p>The International Federation of Association Football (French: F&eacute;d&eacute;ration Internationale de Football Association), commonly known by the acronym FIFA (usual English pronunciation: /&#712;fi&#720;f&#601;/), is the international governing body of association football, futsal and beach football. Its headquarters are located in Zurich, Switzerland, and its current president is Sepp Blatter. FIFA is responsible for the organisation and governance of football\'s major international tournaments, most notably the FIFA World Cup, held since 1930. Nineteen editions of the FIFA World Cup have been held so far. The next edition is to be held in Brazil in 2014. The Executive Committee consists of a President, elected by the Congress in the year following a FIFA World Cup, eight vice-presidents and 15 members, appointed by the confederations and associations. It meets at least twice a year, with the mandate for each member lasting four years, and its role includes determining the dates, loc ations and format of tournaments, appointing FIFA delegates to the IFAB and electing and dismissing the General Secretary on the proposal of the FIFA President. The FIFA Disciplinary Code (FDC) is a set of codes and regulations promulgated by FIFA\'s judicial bodies which are composed by its \&quot;Disciplinary Committee\&quot; and its \&quot;Appeal Committee\&quot;.[citation needed] The FDC regulates almost all issues related to doping, corruption, arbitration, racism, stadium bans, etc... It also details the code of conduct of football\'s world governing body. [edit] Committees The FDC decisions and regulations engages the following FIFA committees: Player Status Committee Disciplinary Committee Appeal Committee Court of Arbitration for Sport FIFA has 208 member associations, three more than the International Olympic Committee and five fewer than the International Association of Athletics Federations. FIFA is an association established under the Laws of Switzerland. Its headquarters are in Zurich. FIFA\'s supreme body is the FIFA Congress, an assembly made up of representatives from each affiliated member association. The Congress assembles in ordinary session once every year and, additionally, extraordinary sessions have been held once a year since 1998. Only the Congress can pass changes to FIFA\'s statutes. Congress elects the President of FIFA, its General Secretary and the other members of FIFA\'s Executive Committee. The President and General Secretary are the main officeholders of FIFA, and are in charge of its daily administration, carried out by the General Secretariat, with its staff of approximately 280 members. FIFA\'s Executive Committee, chaired by the President, is the main decision-making body of the organisation in the intervals of Congress. FIFA\'s worldwide organisational structure also consists of several other bodies, under authority of the Executive Committee or created by Congress as standing committees. Among those bodies are the Finance Committee, the Disciplinary Committee, the Referees Committee, etc. Beside from its worldwide institutions (presidency, Executive Committee, Congress, etc.) there are six confederations recognised by FIFA which oversee the game in the different continents and regions of the world. National associations, and not the continental confederations, are members of FIFA. The continental confederations are provided for in FIFA\'s statutes, and membership of a confederation is a prerequisite to FIFA membership. AFC &ndash; Asian Football Confederation Australia have been members of the AFC since 2006 CAF &ndash; Conf&eacute;d&eacute;ration Africaine de Football CONCACAF &ndash; Confederation of North, Central American and Caribbean Association Football Guyana, Suriname, and French Guiana are CONCACAF members although they are in South America CONMEBOL &ndash; Confederaci&oacute;n Sudamericana de F&uacute;tbol OFC &ndash; Oceania Football Confederation in Oceania UEFA &ndash; Union of European Football Associations Teams representing Israel and the transcontinental nations of Russia, Turkey, Cyprus, Armenia, Azerbaijan and Georgia are under the auspices of UEFA, as are, since 2002, Kazakhstan. In total, FIFA recognises 208 national associations and their associated men\'s national teams as well as 129 women\'s national teams; see the list of national football teams and their respective country codes. FIFA has more member states than the United Nations, as FIFA recognises several non-sovereign entities as distinct nations, such as the four Home Nations within the United Kingdom or politically disputed territories such as Palestine.[2] Only 8 sovereign entities don\'t belong to FIFA (Monaco, Vatican, Micronesia, Marshall, Kiribati, Tuvalu, Palau and Nauru). The FIFA World Rankings are updated monthly and rank each team based on their performance in international competitions, qualifiers, and friendly matches. There is also a world ranking for women\'s football, updated four times a year. The FIFA World Cup (also called the Football World Cup, the Soccer World Cup, or simply the World Cup) is an international association football competition contested by the senior men\'s national teams of the members of F&eacute;d&eacute;ration Internationale de Football Association (FIFA), the sport\'s global governing body. The championship has been awarded every four years since the inaugural tournament in 1930, except in 1942 and 1946 when it was not held because of the Second World War. The current champions are Spain, who won the 2010 tournament. The current format of the tournament involves 32 teams competing for the title at venues within the host nation(s) over a period of about a month &ndash; this phase is often called the World Cup Finals. A qualification phase, which currently takes place over the preceding three years, is used to determine which teams qualify for the tournament together with the host nation(s). The 19 World Cup tournaments have been won by eight different national teams. Brazil have won five times, and they are the only team to have played in every tournament. The other World Cup winners are Italy, with four titles; Germany, with three titles; Argentina and inaugural winners Uruguay, with two titles each; and England, France, and Spain, with one title each. The World Cup is the world\'s most widely viewed sporting event; an estimated 715.1 million people watched the final match of the 2006 FIFA World Cup held in Germany.[1] The next three World Cups will be hosted by Brazil in 2014, Russia in 2018, and Qatar in 2022. Qualification Main article: FIFA World Cup qualification Since the second World Cup in 1934, qualifying tournaments have been held to thin the field for the final tournament.[28] They are held within the six FIFA continental zones (Africa, Asia, North and Central America and Caribbean, South America, Oceania, and Europe), overseen by their respective confederations. For each tournament, FIFA decides the number of places awarded to each of the continental zones beforehand, generally based on the relative strength of the confederations\' teams. The qualification process can start as early as almost three years before the final tournament and last over a two-year period. The formats of the qualification tournaments differ between confederations. Usually, one or two places are awarded to winners of intercontinental play-offs. For example, the winner of the Oceanian zone and the fifth-placed team from the Asian zone entered a play-off for a spot in the 2010 World Cup.[29] From the 1938 World Cup onwards, host nations received automatic qualification to the final tournament. This right was also granted to the defending champions between 1938 and 2002, but was withdrawn from the 2006 FIFA World Cup onward, requiring the champions to qualify. Brazil, winners in 2002, were the first defending champions to play in a qualifying match.[30] Final tournament For the various formats used in previous tournaments, see History of the FIFA World Cup#Format of each final tournament. The current final tournament features 32 national teams competing over a month in the host nation(s). There are two stages: the group stage followed by the knockout stage.[31] In the group stage, teams compete within eight groups of four teams each. Eight teams are seeded, including the hosts, with the other seeded teams selected using a formula based on the FIFA World Rankings and/or performances in recent World Cups, and drawn to separate groups.[32] The other teams are assigned to different \&quot;pots\&quot;, usually based on geographical criteria, and teams in each pot are drawn at random to the eight groups. Since 1998, constraints have been applied to the draw to ensure that no group contains more than two European teams or more than one team from any other confederation.</p>
    <p></p>
<p></p>
    </div>
    <!-- FIM DO TEXTO -->
    
    </div>
    <!-- FIM DA DIREITA -->  
  
  <br class="limpa_float" />  
  </div>
  <!-- FIM DO CONTE�DO -->
  </div>
  <!-- FIM DA DEFESA -->
  <!-- BASE -->
  <div id="base">
    
    <!-- DEFESA DA BASE -->
    <div id="defesa_base">
      
    <div class="left">
    <p class="endereco">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
      CEP 01.424-001<br />
      Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
      S&atilde;o Paulo/SP - Brasil<br />
      email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
    <p><a href="../index.php">Home</a> |   <a href="../avisos-legais.php">Legal terms</a> |   <a href="../mapa-do-site.php">Sitemap</a> |   <a href="../subscreva.php">Subscribe </a></p>
    </div>
    
    <div class="right">
    <ul id="menu_base">
        <li class="acompanhe">Follow us:</li>
        <li class="social">
        <a class="facebook" href="https://www.facebook.com/carlezzo.advogados.associados" title="Facebook" target="_blank">Facebook</a> 
        <a class="linkedin" href="http://www.linkedin.com/company/1294492" title="Linkedin" target="_blank">Linkedin</a> 
        <a class="youtube" href="http://www.youtube.com/user/carlezzo" title="Youtube" target="_blank">Youtube</a>
        </li>
      </ul>
      <br class="limpa_float" /> 
    <a class="assinatura" href="http://www.agencia10clic.com.br" target="_blank" title="Ag&ecirc;ncia 10Clic">Ag&ecirc;ncia 10Clic</a>
    <p>&copy;2011 Carlezzo Advogados. All rights reserved</p>
    </div>
    
    </div>
    <!-- FIM DA DEFESA DA BASE -->
    
  </div>
  <!-- FIM DA BASE -->
<!-- SCRIPT GOOGLE ANALYTICS - AG�NCIA 10CLIC - OTIMIZA��O -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10514770-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script><br />
<!-- FIM DO SCRIPT GOOGLE ANALYTICS -->
</body>
</html>