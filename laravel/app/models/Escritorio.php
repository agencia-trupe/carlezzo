<?php

class Escritorio extends Eloquent
{

    protected $table = 'escritorio';

    protected $hidden = [];

    protected $guarded = ['id', 'slug'];

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

}
