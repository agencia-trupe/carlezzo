<?php

return array(

    'campos' => [
        'titulo' => 'titulo_pt',
        'texto'  => 'texto_pt',
        'cargo'  => 'cargo_pt'
    ],

    'menu' => [
        'escritorio' => 'O Escritório',
        'atuacao'    => 'Atuação',
        'equipe'     => 'Equipe',
        'noticias'   => 'Notícias',
        'contato'    => 'Contato'
    ],

    'footer' => [
        'copyright' => 'Todos os direitos reservados',
        'criacao'   => 'Criação de sites',
        'voltar'    => 'voltar'
    ],

    'home' => [
        'mais'    => 'Mais',
        'vermais' => 'ver mais'
    ],

    'atuacao' => [
        'atuacao' => 'Atuação',
        'pratica' => 'Prática Legal',
        'fazemos' => 'O que fazemos para'
    ],

    'noticias' => [
        'noticias'    => 'Notícias',
        'corporativo' => 'Corporativo',
        'leiamais'    => 'Leia mais',
        'por'         => 'por',
        'nenhum'      => 'Nenhum registro encontrado.'
    ],

    'contato' => [
        'nome'     => 'Nome',
        'email'    => 'E-mail',
        'telefone' => 'Telefone',
        'mensagem' => 'Mensagem',
        'enviar'   => 'Enviar',
        'erro'     => 'Por favor preencha todos os campos corretamente.',
        'sucesso'  => 'Mensagem enviada com sucesso.'
    ],

    'busca' => [
        'resultados' => 'Resultados para o termo',
        'nenhum'     => 'Nenhum resultado encontrado.'
    ]

);
