@section('content')

    @if(count($banners))
    <div class="banners-wrapper">
        <div id="banners">
            @foreach($banners as $banner)
            <div class="banner-slide" style="background-image: url('{{ asset('assets/img/banners/'.$banner->imagem) }}');">
                <div class="overlay" style="background: rgba({{ $banner->cor }}, .8)">
                    <div class="chamada">
                        <h3>{{ $banner->{Lang::get('frontend.campos.titulo')} }}</h3>
                        <p>{{ $banner->{Lang::get('frontend.campos.texto')} }}</p>
                        <a href="{{ $banner->link }}">{{ Lang::get('frontend.home.mais') }} »</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <a href="#" id="prev">Anterior</a>
        <a href="#" id="next">Próximo</a>
    </div>
    @endif

    @if(count($noticias) || count($corporativo))
    <section class="noticias">
        <div class="box-noticias">
            <div class="title">
                <span>{{ Lang::get('frontend.noticias.noticias') }}</span>
                <a href="{{ route('noticias') }}">{{ Lang::get('frontend.home.vermais') }}</a>
            </div>
            @foreach($noticias as $noticia)
                <a href="{{ route('noticias', $noticia->slug) }}" class="noticia">
                    <span class="data">{{ $noticia->data }}</span>
                    {{ $noticia->titulo }}
                </a>
            @endforeach
        </div>
        <div class="box-corporativo">
            <div class="title">
                <span>{{ Lang::get('frontend.noticias.corporativo') }}</span>
                <a href="{{ route('corporativo') }}">{{ Lang::get('frontend.home.vermais') }}</a>
            </div>
            @foreach($corporativo as $noticia)
                <a href="{{ route('corporativo', $noticia->slug) }}" class="noticia">
                    <span class="data">{{ $noticia->data }}</span>
                    {{ $noticia->titulo }}
                </a>
            @endforeach
        </div>
    </section>
    @endif

@stop
