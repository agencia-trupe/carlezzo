<?php
	
	//-------------------- Conecta com o Banco de Dados ---------------------------
	include "../_includes/conexao.php";	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- META TAGS - AG�NCIA 10CLIC - OTIMIZA��O -->
<meta name="author" content="10Clic - www.10clic.com.br" />
<meta name="description" content="Escrit�rio de advocacia especializado em esporte, entretenimento e m�dia, com ampla atua��o internacional em temas ligados ao futebol, FIFA e atletas.
" />
<meta name="keywords" content="FIFA,UEFA,CAS,court of arbitration for sport,futebol, agentes de jogadores, atletas, mecanismo de solidariedade, compensa��o por forma��o, cinema,m�sica,celebridades, publicidade, marketing,televis�o, propaganda, shows, r�dio,internet, jogos eletr�nicos, gaming,m�dia,novas tecnologias,m�dias sociais,software,fundos de investimento,direito desport" />
<meta name="robots" content="index, follow, archive" />
<meta name="googlebot" content="archive" />
<!-- FIM DAS META TAGS -->
<title>Carlezzo Advogados - Avisos Legais</title>
<!-- FOLHAS DE ESTILO -->
<link href="../_css/estilo.css" rel="stylesheet" type="text/css" />
<link href="../_css/nivo-slider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../_ajax/skin.css" rel="stylesheet" type="text/css" />
<!-- FIM DAS FOLHAS DE ESTILO -->
<!-- SCRIPTS -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'49de9f94-6555-4ec3-ba64-437c8a161ebe'});</script>
<script type="text/javascript" src="../_ajax/jquery.js"></script> 
<script type="text/javascript" src="../_ajax/menu.jquery.js"></script> 
<!-- FIM DOS SCRIPTS -->
</head>

<body class="interna">
<!-- DEFESA -->
<div id="defesa">
  <!-- TOPO -->
  <div id="topo">
  	
    <!-- LINHA -->  
    <div id="linha">
    
    <form action="busca.php" method="post" id="busca">
    <input class="campo" name="palavra" type="text" />
    <input name="" type="image" value="ok" src="../_imagens/botao_buscar.jpg" alt="Buscar" class="botao" />
    </form>
    <span displayText="" class="st_sharethis_custom"></span>
    <p class="linguas_pt">
    <a href="../en/<?=basename($_SERVER['PHP_SELF']);?>" title="English">English</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" title="Espa&ntilde;ol">Espa&ntilde;ol</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:window.print()" title="Imprima esta p&aacute;gina"><img src="../_imagens/a_print.jpg" alt="Imprima esta p&aacute;gina" align="absmiddle" /></a>&nbsp;&nbsp;<a href="mailto:carlezzo@carlezzo.com.br" title="Contato r&aacute;pido"><img src="../_imagens/a_mail.jpg" alt="Contato r&aacute;pido" align="absmiddle" /></a>
    </p>    
    </div>
    <!-- FIM DA LINHA -->
    
    <!-- LOGO -->
    <h1><a href="index.php" title="Carlezzo Advogados">Carlezzo Advogados</a></h1>
    <!-- FIM DO LOGO -->
    
    <!-- MENU TOPO -->
    <div id="menu_topo">
    	<ul class="topnav">
            <li><a href="index.php">Home</a></li>
            <li>
                <a href="quem-somos.php">Quem somos</a>

              <ul class="subnav">
                    <li><a href="quem-somos.php">Vis&atilde;o Geral</a></li>
                    <li><a href="missao-e-valores.php">Miss&atilde;o e Valores</a></li>
                    <li><a href="o-que-esperar-de-nos.php">O qu&ecirc; esperar de n&oacute;s</a></li>
                    <li><a href="aliancas-internacionais.php">Alian&ccedil;as Internacionais</a></li>
                    <li><a href="recrutamento.php">Recrutamento</a></li>
                    <li><a href="pro-bono.php">Pr&oacute;-bono</a></li>
              </ul>
          </li>
            <li>
                <a href="setores.php">Setores</a>
              <ul class="subnav">
                    <li><a href="setores.php?categoria=esporte">Esporte</a></li>
     				<li><a href="setores.php?categoria=entretenimento">Entretenimento</a></li>
      				<li><a href="setores.php?categoria=midia">M&iacute;dia e Novas Tecnologias</a></li>
              </ul>
          </li>
          <li><a href="equipe.php">Equipe</a></li>

          <li><a href="noticias.php">Not&iacute;cias</a></li>
          <li><a href="publicacoes.php">Publica&ccedil;&otilde;es</a></li>
          <li class="last"><a href="onde-estamos.php">Onde Estamos</a></li>
      </ul>
    </div>
    <!-- FIM DO MENU TOPO -->
  
  </div>
  <!-- FIM DO TOPO -->
  
  <!-- CONTE�DO -->
  <div id="conteudo">
    
    <!-- CHAMADA P�GINA -->
    <div class="chamada_pagina"> 
    
    <div class="bloco_chamada">
      <h2 class="titulo">AVISOS LEGAIS</h2>
    </div>
  
    <br class="limpa_float" />
    </div>
    <!-- FIM DA CHAMADA P�GINA -->  
    
    <br class="limpa_float" />
    
    <!-- FULL -->
    <div class="full">
    
    <div class="full_texto">
      <p><strong>Informa&ccedil;&otilde;es Legais e Termos de Uso<br />
      </strong></p>
      <ol>
        <li>O web site www.carlezzo.com.br &eacute; um  servi&ccedil;o online de informa&ccedil;&otilde;es e comunica&ccedil;&atilde;o prestado pelo Carlezzo Advogados  Associados. Por meio do uso deste web site, ou da c&oacute;pia das informa&ccedil;&otilde;es nele  contidas, voc&ecirc; expressa seu consentimento em observar os termos e condi&ccedil;&otilde;es  aqui previstos. <br />
        </li>
        <li>Nenhuma parte do conte&uacute;do  disponibilizado atrav&eacute;s deste site, incluindo boletins institucionais e artigos  escritos por integrantes do escrit&oacute;rio, deve ser interpretada como  aconselhamento ou parecer jur&iacute;dico. Orienta&ccedil;&otilde;es legais devem ser obtidas  atrav&eacute;s de nossos advogados.<br />
        </li>
        <li>Todos os textos, gr&aacute;ficos, interfaces  de usu&aacute;rios, marcas registradas, logos, fotografias, entre outras obras  intelectuais no site s&atilde;o de propriedade, controlados ou licenciados a Carlezzo  Advogados Associados e s&atilde;o protegidos por leis de direitos autorais e de  direito marc&aacute;rio, entre outras leis de propriedade intelectual.<br />
        </li>
        <li>O conte&uacute;do das p&aacute;ginas deste web site  n&atilde;o pode ser copiado, reproduzido, transferido, publicado ou distribu&iacute;do, de  qualquer forma, sem a autoriza&ccedil;&atilde;o pr&eacute;via e por escrito do Carlezzo Advogados  Associados.<br />
        </li>
        <li>O Carlezzo Advogados Associados  autoriza-o a armazenar, em seu computador, ou imprimir c&oacute;pias destas p&aacute;ginas,  desde que exclusivamente para seu uso pessoal.</li>
        <li>Qualquer tipo de informa&ccedil;&atilde;o enviada a  Carlezzo Advogados Associados atrav&eacute;s do correio eletr&ocirc;nico, dispon&iacute;vel no  site, n&atilde;o ser&aacute; tratada como confidencial. Comunica&ccedil;&otilde;es realizadas com o  Carlezzo Advogados Associados tamb&eacute;m via correio eletr&ocirc;nico n&atilde;o constituir&atilde;o  rela&ccedil;&atilde;o cliente - advogado.<br />
        </li>
      </ol>
      O Carlezzo Advogados  Associados n&atilde;o dever&aacute;, em, hip&oacute;tese alguma, ser responsabilizado perante  qualquer pessoa, por qualquer tipo de perda, dano, custo ou despesa resultante  de qualquer erro, omiss&atilde;o, ou altera&ccedil;&atilde;o nas informa&ccedil;&otilde;es aqui fornecidas, nem  tampouco por quaisquer atrasos, inexatid&otilde;es, erros, ou interrup&ccedil;&otilde;es ocasionados  em fun&ccedil;&atilde;o destes eventos, durante o suprimento de qualquer informa&ccedil;&atilde;o atrav&eacute;s  das p&aacute;ginas do site.
<p>&nbsp;</p>
    </div>

    </div>
    <!-- FIM DA FULL -->  
  
  <br class="limpa_float" />  
  </div>
  <!-- FIM DO CONTE�DO -->
  </div>
  <!-- FIM DA DEFESA -->
  <!-- BASE -->
  <div id="base">
    
    <!-- DEFESA DA BASE -->
    <div id="defesa_base">
      
    <div class="left">
    <p class="endereco">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
      CEP 01.424-001<br />
      Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
      S&atilde;o Paulo/SP - Brasil<br />
      email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
    <p><a href="index.php">Home</a> |   <a href="avisos-legais.php">Avisos legais</a> |   <a href="mapa-do-site.php">Mapa do site</a> |   <a href="subscreva.php">Subscreva </a></p>
    </div>
    
    <div class="right">
    <ul id="menu_base">
        <li class="acompanhe">Siga-nos:</li>
        <li class="social">
        <a class="facebook" href="https://www.facebook.com/carlezzo.advogados.associados" title="Facebook" target="_blank">Facebook</a> 
        <a class="linkedin" href="http://www.linkedin.com/company/1294492" title="Linkedin" target="_blank">Linkedin</a> 
        <a class="youtube" href="http://www.youtube.com/user/carlezzo" title="Youtube" target="_blank">Youtube</a>
        </li>
      </ul>
      <br class="limpa_float" /> 
    <a class="assinatura" href="http://www.agencia10clic.com.br" target="_blank" title="Ag&ecirc;ncia 10Clic">Ag&ecirc;ncia 10Clic</a>
    <p>&copy;2011 Carlezzo Advogados. Todos os direitos reservados</p>
    </div>
    
    </div>
    <!-- FIM DA DEFESA DA BASE -->
    
  </div>
  <!-- FIM DA BASE -->
<!-- SCRIPT GOOGLE ANALYTICS - AG�NCIA 10CLIC - OTIMIZA��O -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10514770-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script><br />
<!-- FIM DO SCRIPT GOOGLE ANALYTICS -->
</body>
</html>