<?php
	
	//-------------------- Conecta com o Banco de Dados ---------------------------
	include "../../_includes/conexao.php";	
	
	//Busca not�cias
	$busca_noticias = mysql_query("SELECT * FROM noticias WHERE status = '1' AND titulo_en != '0'  AND destaque != '1' ORDER BY data DESC LIMIT 0, 2");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- META TAGS - AG�NCIA 10CLIC - OTIMIZA��O -->
<meta name="author" content="10Clic - www.10clic.com.br" />
<meta name="description" content="Law firm specialized in sports, entertainment and media, with strong presence in football and international affairs, as well as before FIFA and CBF
" />
<meta name="keywords" content="FIFA, UEFA, Court of arbitration for sport, football, player's agents, athletes, clubs, solidarity mechanism, training compensation, transfers, cinema, sports law, music, concerts, models, celebrities, video-game, media, new technologies, intellectual property, advertising, brands, marketing, Internet, TV, radio, mobile, world cup, Olympic games
" />
<meta name="robots" content="index, follow, archive" />
<meta name="googlebot" content="archive" />
<!-- FIM DAS META TAGS -->
<title>Carlezzo Advogados - Football</title>
<!-- FOLHAS DE ESTILO -->
<link href="../../_css/estilo.css" rel="stylesheet" type="text/css" />
<link href="../../_css/nivo-slider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../_ajax/skin.css" rel="stylesheet" type="text/css" />
<!-- FIM DAS FOLHAS DE ESTILO -->
<!-- SCRIPTS -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'49de9f94-6555-4ec3-ba64-437c8a161ebe'});</script>
<script type="text/javascript" src="../../_ajax/jquery.js"></script> 
<script type="text/javascript" src="../../_ajax/menu.jquery.js"></script> 
<!-- FIM DOS SCRIPTS -->
</head>

<body class="interna">
<!-- DEFESA -->
<div id="defesa">
  <!-- TOPO -->
  <div id="topo">
  	
    <!-- LINHA -->  
    <div id="linha">
    
    <form action="../busca.php" method="post" id="busca">
    <input class="campo" name="palavra" type="text" />
    <input name="" type="image" value="ok" src="../../_imagens/botao_buscar.jpg" alt="Search" class="botao" />
    </form>
    <span displayText="" class="st_sharethis_custom"></span>
    <p class="linguas">
    <a href="../pt/<?=basename($_SERVER['PHP_SELF']);?>" title="Portugu&ecirc;s">Portugu&ecirc;s</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" title="Espa&ntilde;ol">Espa&ntilde;ol</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:window.print()" title="Print this page"><img src="../../_imagens/a_print.jpg" alt="Print this page" align="absmiddle" /></a>&nbsp;&nbsp;<a href="mailto:carlezzo@carlezzo.com.br" title="Quick contact"><img src="../../_imagens/a_mail.jpg" alt="Quick contact" align="absmiddle" /></a>
    </p>    
    </div>
    <!-- FIM DA LINHA -->
    
    <!-- LOGO -->
    <h1><a href="../index.php" title="Carlezzo Advogados">Carlezzo Advogados</a></h1>
    <!-- FIM DO LOGO -->
    
    <!-- MENU TOPO -->
    <div id="menu_topo">
    	<ul class="topnav">
            <li><a href="../index.php">Home</a></li>
            <li>
                <a href="../quem-somos.php">About us</a>

          <ul class="subnav">
                    <li><a href="../quem-somos.php">Overview</a></li>
                    <li><a href="../missao-e-valores.php">Mission and Values</a></li>
                    <li><a href="../o-que-esperar-de-nos.php">What you can wait from us</a></li>
                    <li><a href="../aliancas-internacionais.php">International Alliances</a></li>
                    <li><a href="../recrutamento.php">Recruitment</a></li>
                    <li><a href="../pro-bono.php">Pro-bono</a></li>
              </ul>
          </li>
            <li>
                <a href="../setores.php">Sectors</a>
          <ul class="subnav">
                    <li><a href="../setores.php?categoria=esporte">Sport</a></li>
     				<li><a href="../setores.php?categoria=entretenimento">Entertainment</a></li>
      				<li><a href="../setores.php?categoria=midia">Media and New Technologies</a></li>
              </ul>
          </li>
          <li><a href="../equipe.php">Team</a></li>

          <li><a href="../noticias.php">News</a></li>
          <li><a href="../publicacoes.php">Publications</a></li>
          <li class="last"><a href="../onde-estamos.php">Where are we</a></li>
      </ul>
    </div>
    <!-- FIM DO MENU TOPO -->
  
  </div>
  <!-- FIM DO TOPO -->
  
  <!-- CONTE�DO -->
  <div id="conteudo">
    
    <!-- ESQUERDA -->
    <div class="esquerda_interna">
    
    <div class="bloco_sidebar">
      <h2 class="titulo">CARLEZZO ADVOGADOS</h2>
      <ul class="menu_esquerda">
    <li><a href="football.php">Football</a></li>
	<li><a href="fifa.php">FIFA</a></li>
	<li><a href="world-cup-fifa-2014.php">World Cup FIFA 2014</a></li>
	<li><a href="olympic-games-rio-2016.php">Olympic Games Rio 2016</a></li>
	<li><a href="entertainment.php">Entertainment</a></li>
        </ul>
    </div>
    
    
    <!-- CHAMADA -->
    <div class="chamada"><img src="../../_imagens/chamada_quem_somos.jpg" width="242" height="318" /></div>
    <!-- FIM DA CHAMADA -->
        
    <br class="limpa_float" />
    </div>
    <!-- FIM DA ESQUERDA --> 
    
    <!-- DIREITA -->
    <div class="direita_interna">
    
    <!-- TEXTO -->
    <div class="texto">
    <h5 class="square">Football</h5>
    <p>Association football, commonly known as football or soccer, is a sport played between two teams of eleven players with a spherical ball. At the turn of the 21st century the game was played by over 250 million players in 200 countries making it the world&rsquo;s most popular sport.[1][2][3][4] The game is played on a rectangular field of grass or green artificial turf, with a goal in the middle of each of the short ends. The object of the game is to score by driving the ball into the opposing goal. In general play, the goalkeepers are the only players allowed to touch the ball with their hands or arms, while the field players typically use their feet to kick the ball into position, occasionally using their torso or head to intercept a ball in midair. The team that scores the most goals by the end of the match wins. If the score is tied at the end of the game, either a draw is declared or the game goes into extra time and/or a penalty shootout, depending on the format of the competition. The Laws of the Game were originally codified in England by the Football Association in 1863. The laws have been substantially changed since that time, to such an extent that the game played today bears little resemblance to that of the 1860s. Association football is governed internationally by FIFA, which organises the FIFA World Cup every four years. The recognised international governing body of football (and associated games, such as futsal and beach soccer) is the F&eacute;d&eacute;ration Internationale de Football Association (FIFA). The FIFA headquarters are located in Zurich. Six regional confederations are associated with FIFA; these are:[57] Asia: Asian Football Confederation (AFC) Africa: Confederation of African Football (CAF) Europe: Union of European Football Associations (UEFA) North/Central America &amp; Caribbean: Confederation of North, Central American and Caribbean Association Football (CONCACAF) Oceania: Oceania Football Confederation (OFC) South America: Confederaci&oacute;n Sudamericana de F&uacute;tbol/Confedera&ccedil;&atilde;o Sul-americana de Futebol (South American Football Confederation; CONMEBOL) National associations oversee football within individual countries. These are generally synonymous with sovereign states, (for example: the F&eacute;d&eacute;ration Camerounaise de Football in Cameroon) but also include a smaller number of associations responsible for sub-national entities or autonomous regions (for example the Scottish Football Association in Scotland). 208 national associations are affiliated both with FIFA and with their respective continental confederations.[57] While FIFA is responsible for arranging competitions and most rules related to international competition, the actual Laws of the Game are set by the International Football Association Board, where each of the UK Associations has one vote, while FIFA collectively has four votes. The major international competition in football is the World Cup, organised by FIFA. This competition takes place over a four-year period. More than 190 national teams compete in qualifying tournaments within the scope of continental confederations for a place in the finals. The finals tournament, which is held every four years, involves 32 national teams competing over a four-week period.[58] The most recent tournament, the 2010 FIFA World Cup, was held in South Africa from 11 June to 11 July.[59] There has been a football tournament at every Summer Olympic Games since 1900, except at the 1932 games in Los Angeles.[60] Before the inception of the World Cup, the Olympics (especially during the 1920s) had the same status as the World Cup. Originally, the event was for amateurs only,[25] however, since the 1984 Summer Olympics professional players have been permitted, albeit with certain restrictions which prevent countries from fielding their strongest sides. Currently, the Olympic men\'s tournament is played at Under-23 level. In the past the Olympics have allowed a restricted number of over-age players per team;[61] but that practice ceased in the 2008 Olympics. A women\'s tournament was added in 1996; in contrast to the men\'s event, full international sides without age restrictions play the women&rsquo;s Olympic tournament.[62] After the World Cup, the most important international football competitions are the continental championships, which are organised by each continental confederation and contested between national teams. These are the European Championship (UEFA), the Copa Am&eacute;rica (CONMEBOL), African Cup of Nations (CAF), the Asian Cup (AFC), the CONCACAF Gold Cup (CONCACAF) and the OFC Nations Cup (OFC). The FIFA Confederations Cup is contested by the winners of all 6 continental championships, the current FIFA World Cup champions and the country which is hosting the Confederations Cup. This is generally regarded as a warm up tournament for the upcoming FIFA World Cup and does not carry the same prestige as the World Cup itself. The most prestigious competitions in club football are the respective continental championships, which are generally contested between national champions, for example the UEFA Champions League in Europe and the Copa Libertadores de Am&eacute;rica in South America. The winner s of each continental competition contest the FIFA Club World Cup.[63] The governing bodies in each country operate league systems in a domestic season, normally comprising several divisions, in which the teams gain points throughout the season depending on results. Teams are placed into tables, placing them in order according to points accrued. Most commonly, each team plays every other team in its league at home and away in each season, in a round-robin tournament. At the end of a season, the top team is declared the champion. The top few teams may be promoted to a higher division, and one or more of the teams finishing at the bottom are relegated to a lower division.[64] The teams finishing at the top of a country\'s league may be eligible also to play in international club competitions in the following season. The main exceptions to this system occur in some Latin American leagues, which divide football championships into two sections named Apertura and Clausura (Spanish for Opening and Closing), awarding a champion for each.[65] The majority of countries supplement the league system with one or more \&quot;cup\&quot; competitions organised on a knock-out basis. Some countries\' top divisions feature highly paid star players; in smaller countries and lower divisions, players may be part-timers with a second job, or amateurs. The five top European leagues &ndash; the Premier League (England),[66] La Liga (Spain), Serie A (Italy), the Bundesliga (Germany) and Ligue 1 (France) &ndash; attract most of the world\'s best players and each of the leagues has a total wage cost in excess of &pound;600 million/&euro;763 million/$1.185 billion</p>
    <p></p>
    </div>
    <!-- FIM DO TEXTO -->
    
    </div>
    <!-- FIM DA DIREITA -->  
  
  <br class="limpa_float" />  
  </div>
  <!-- FIM DO CONTE�DO -->
  </div>
  <!-- FIM DA DEFESA -->
  <!-- BASE -->
  <div id="base">
    
    <!-- DEFESA DA BASE -->
    <div id="defesa_base">
      
    <div class="left">
    <p class="endereco">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
      CEP 01.424-001<br />
      Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
      S&atilde;o Paulo/SP - Brasil<br />
      email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
    <p><a href="../index.php">Home</a> |   <a href="../avisos-legais.php">Legal terms</a> |   <a href="../mapa-do-site.php">Sitemap</a> |   <a href="../subscreva.php">Subscribe </a></p>
    </div>
    
    <div class="right">
    <ul id="menu_base">
        <li class="acompanhe">Follow us:</li>
        <li class="social">
        <a class="facebook" href="https://www.facebook.com/carlezzo.advogados.associados" title="Facebook" target="_blank">Facebook</a> 
        <a class="linkedin" href="http://www.linkedin.com/company/1294492" title="Linkedin" target="_blank">Linkedin</a> 
        <a class="youtube" href="http://www.youtube.com/user/carlezzo" title="Youtube" target="_blank">Youtube</a>
        </li>
      </ul>
      <br class="limpa_float" /> 
    <a class="assinatura" href="http://www.agencia10clic.com.br" target="_blank" title="Ag&ecirc;ncia 10Clic">Ag&ecirc;ncia 10Clic</a>
    <p>&copy;2011 Carlezzo Advogados. All rights reserved</p>
    </div>
    
    </div>
    <!-- FIM DA DEFESA DA BASE -->
    
  </div>
  <!-- FIM DA BASE -->
<!-- SCRIPT GOOGLE ANALYTICS - AG�NCIA 10CLIC - OTIMIZA��O -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10514770-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script><br />
<!-- FIM DO SCRIPT GOOGLE ANALYTICS -->
</body>
</html>