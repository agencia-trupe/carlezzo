<?php
	
	//-------------------- Conecta com o Banco de Dados ---------------------------
	include "../../_includes/conexao.php";	
	
	//Busca not�cias
	$busca_noticias = mysql_query("SELECT * FROM noticias WHERE status = '1' AND titulo_en != '0'  AND destaque != '1' ORDER BY data DESC LIMIT 0, 2");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- META TAGS - AG�NCIA 10CLIC - OTIMIZA��O -->
<meta name="author" content="10Clic - www.10clic.com.br" />
<meta name="description" content="Law firm specialized in sports, entertainment and media, with strong presence in football and international affairs, as well as before FIFA and CBF
" />
<meta name="keywords" content="FIFA, UEFA, Court of arbitration for sport, football, player's agents, athletes, clubs, solidarity mechanism, training compensation, transfers, cinema, sports law, music, concerts, models, celebrities, video-game, media, new technologies, intellectual property, advertising, brands, marketing, Internet, TV, radio, mobile, world cup, Olympic games
" />
<meta name="robots" content="index, follow, archive" />
<meta name="googlebot" content="archive" />
<!-- FIM DAS META TAGS -->
<title>Carlezzo Advogados - World Cup FIFA 2014</title>
<!-- FOLHAS DE ESTILO -->
<link href="../../_css/estilo.css" rel="stylesheet" type="text/css" />
<link href="../../_css/nivo-slider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../_ajax/skin.css" rel="stylesheet" type="text/css" />
<!-- FIM DAS FOLHAS DE ESTILO -->
<!-- SCRIPTS -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'49de9f94-6555-4ec3-ba64-437c8a161ebe'});</script>
<script type="text/javascript" src="../../_ajax/jquery.js"></script> 
<script type="text/javascript" src="../../_ajax/menu.jquery.js"></script> 
<!-- FIM DOS SCRIPTS -->
</head>

<body class="interna">
<!-- DEFESA -->
<div id="defesa">
  <!-- TOPO -->
  <div id="topo">
  	
    <!-- LINHA -->  
    <div id="linha">
    
    <form action="../busca.php" method="post" id="busca">
    <input class="campo" name="palavra" type="text" />
    <input name="" type="image" value="ok" src="../../_imagens/botao_buscar.jpg" alt="Search" class="botao" />
    </form>
    <span displayText="" class="st_sharethis_custom"></span>
    <p class="linguas">
    <a href="../pt/<?=basename($_SERVER['PHP_SELF']);?>" title="Portugu&ecirc;s">Portugu&ecirc;s</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" title="Espa&ntilde;ol">Espa&ntilde;ol</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:window.print()" title="Print this page"><img src="../../_imagens/a_print.jpg" alt="Print this page" align="absmiddle" /></a>&nbsp;&nbsp;<a href="mailto:carlezzo@carlezzo.com.br" title="Quick contact"><img src="../../_imagens/a_mail.jpg" alt="Quick contact" align="absmiddle" /></a>
    </p>    
    </div>
    <!-- FIM DA LINHA -->
    
    <!-- LOGO -->
    <h1><a href="../index.php" title="Carlezzo Advogados">Carlezzo Advogados</a></h1>
    <!-- FIM DO LOGO -->
    
    <!-- MENU TOPO -->
    <div id="menu_topo">
    	<ul class="topnav">
            <li><a href="../index.php">Home</a></li>
            <li>
                <a href="../quem-somos.php">About us</a>

          <ul class="subnav">
                    <li><a href="../quem-somos.php">Overview</a></li>
                    <li><a href="../missao-e-valores.php">Mission and Values</a></li>
                    <li><a href="../o-que-esperar-de-nos.php">What you can wait from us</a></li>
                    <li><a href="../aliancas-internacionais.php">International Alliances</a></li>
                    <li><a href="../recrutamento.php">Recruitment</a></li>
                    <li><a href="../pro-bono.php">Pro-bono</a></li>
              </ul>
          </li>
            <li>
                <a href="../setores.php">Sectors</a>
          <ul class="subnav">
                    <li><a href="../setores.php?categoria=esporte">Sport</a></li>
     				<li><a href="../setores.php?categoria=entretenimento">Entertainment</a></li>
      				<li><a href="../setores.php?categoria=midia">Media and New Technologies</a></li>
              </ul>
          </li>
          <li><a href="../equipe.php">Team</a></li>

          <li><a href="../noticias.php">News</a></li>
          <li><a href="../publicacoes.php">Publications</a></li>
          <li class="last"><a href="../onde-estamos.php">Where are we</a></li>
      </ul>
    </div>
    <!-- FIM DO MENU TOPO -->
  
  </div>
  <!-- FIM DO TOPO -->
  
  <!-- CONTE�DO -->
  <div id="conteudo">
    
    <!-- ESQUERDA -->
    <div class="esquerda_interna">
    
    <div class="bloco_sidebar">
      <h2 class="titulo">CARLEZZO ADVOGADOS</h2>
      <ul class="menu_esquerda">
    <li><a href="football.php">Football</a></li>
	<li><a href="fifa.php">FIFA</a></li>
	<li><a href="world-cup-fifa-2014.php">World Cup FIFA 2014</a></li>
	<li><a href="olympic-games-rio-2016.php">Olympic Games Rio 2016</a></li>
	<li><a href="entertainment.php">Entertainment</a></li>
        </ul>
    </div>
    
    
    <!-- CHAMADA -->
    <div class="chamada"><img src="../../_imagens/chamada_quem_somos.jpg" width="242" height="318" /></div>
    <!-- FIM DA CHAMADA -->
        
    <br class="limpa_float" />
    </div>
    <!-- FIM DA ESQUERDA --> 
    
    <!-- DIREITA -->
    <div class="direita_interna">
    
    <!-- TEXTO -->
    <div class="texto">
    <h5 class="square">World Cup FIFA 2014</h5>
    <p>The 2014 FIFA World Cup will be the 20th FIFA World Cup, an international association football tournament that is scheduled to take place in June and July 2014 in Brazil. This will be the second time the country has hosted the competition, the first being the 1950 FIFA World Cup. Brazil will become the fifth country to have hosted the FIFA World Cup twice, after Mexico, Italy, France and Germany. It will be the first World Cup to be held in South America since the 1978 FIFA World Cup in Argentina, the first time two consecutive World Cups are staged outside Europe and the first time two consecutive World Cups are staged in the Southern Hemisphere (the 2010 FIFA World Cup was held in South Africa). On 7 March 2003, FIFA announced that the tournament would be held in South America for the first time since Argentina hosted the competition in 1978, in line with its policy of rotating the right to host the World Cup amongst different confederations. On 3 June 2003, CONMEBOL announced that Argentina, Brazil, and Colombia wanted to host the 2014 World Cup finals.[1] By 17 March 2004, the CONMEBOL associations had voted unanimously to adopt Brazil as their sole candidate.[2] Brazil formally declared its candidacy in December 2006 and Colombia followed a few days later. The Argentina bid never materialized. On 11 April 2007, Colombia officially withdrew its bid, Francisco Santos Calder&oacute;n the vice president of Colombia announced that instead Colombia would be hosting the 2011 FIFA U-20 World Cup. With this development, Brazil became the only official candidate to host the 2014 event.[3] Brazil won the right to host the event on 30 October 2007 as the only country to enter a bid.[4] On 7 March 2003, FIFA announced that the tournament would be held in South America for the first time since Argentina hosted the competition in 1978, in line with its policy of rotating the right to host the World Cup amongst different confederations. On 3 June 2003, CONMEBOL announced that Argentina, Brazil, and Colombia wanted to host the 2014 World Cup finals.[1] By 17 March 2004, the CONMEBOL associations had voted unanimously to adopt Brazil as their sole candidate. [2] Brazil formally declared its candidacy in December 2006 and Colombia followed a few days later. The Argentina bid never materialized. On 11 April 2007, Colombia officially withdrew its bid, Francisco Santos Calder&oacute;n the vice president of Colombia announced that instead Colombia would be hosting the 2011 FIFA U-20 World Cup. With this development, Brazil became the only official candidate to host the 2014 event.[3] Brazil won the right to host the event on 30 October 2007 as the only country to enter a bid.[4] On 7 March 2003, FIFA announced that the tournament would be held in South America for the first time since Argentina hosted the competition in 1978, in line with its policy of rotating the right to host the World Cup amongst different confederations. On 3 June 2003, CONMEBOL announced that Argentina, Brazil, and Colombia wanted to host the 2014 World Cup finals.[1] By 17 March 2004, the CONMEBOL associations had voted unanimously to adopt Brazil as their sole candidate. [2] Brazil formally declared its candidacy in December 2006 and Colombia followed a few days later. The Argentina bid never materialized. On 11 April 2007, Colombia officially withdrew its bid, Francisco Santos Calder&oacute;n the vice president of Colombia announced that instead Colombia would be hosting the 2011 FIFA U-20 World Cup. With this development, Brazil became the only official candidate to host the 2014 event.[3] Brazil won the right to host the event on 30 October 2007 as the only country to enter a bid.[4] Seventeen cities showed interest in being chosen as World Cup host cities: S&atilde;o Paulo, Rio de Janeiro, Belo Horizonte, Porto Alegre, Bras&iacute;lia, Bel&eacute;m, Campo Grande, Cuiab&aacute;, Curitiba, Florian&oacute;polis, Fortaleza, Goi&acirc;nia, Manaus, Natal, Recife (a stadium would be shared by both cities), Rio Branco and Salvador.[8] Macei&oacute; withdrew in January 2009. According to current FIFA practice, no more than one city may use two stadiums, and the number of host cities is limited between eight and ten. The Brazilian Football Confederation (CBF) requested permission to assign 12 cities hosting World Cup Finals.[9] On 26 December 2008, FIFA gave the green light to the 12-city plan.[10] Even before the 12 host cities were selected, there were few doubts that the chosen venue for the final match will be the Maracan&atilde; in Rio de Janeiro, which also hosted the decisive match of the 1950 FIFA World Cup. Originally the CBF\'s intentions were to have the opening match at Est&aacute;dio do Morumbi in S&atilde;o Paulo, the largest city in Brazil. However, on 14 June 2010 the stadium was excluded from hosting games in the tournament due to a failure to provide financial guarantees for the improvements needed to have it as an eligible venue.[11] In the end of August 2010, the CBF announced that the new Corinthians stadium will host the matches in S&atilde;o Paulo. The 12 host cities for the 2014 World Cup were announced on 31 May 2009.[12] Bel&eacute;m, Campo Grande, Florian&oacute;polis, Goi&acirc;nia and Rio Branco were rejected. The CBF estimates that the cost of construction and remodeling of stadiums[18] alone will be approximately over R$ 1.9 billion (US$ 1.1 billion, &pound; 550 million).[19] In addition to the stadium upgrades and renovations, there will be millions more spent on basic infrastructure needs to get the country ready. When informed of the decision to host the tournament, CBF President Ricardo Teixeira said: \&quot;We are a civilized nation, a nation that is going through an excellent phase, and we have got everything prepared to receive adequately the honor to organize an excellent World Cup.\&quot; Teixeira was on hand at FIFA\'s headquarters in Z&uuml;rich when the announcement was made. \&quot;Over the next few years we will have a consistent influx of investments. The 2014 World Cup will enable Brazil to have a modern infrastructure,\&quot; Teixeira said. \&quot;In social terms will be very beneficial. Our objective is to make Brazil become more visible in global arenas,\&quot; he added. \&quot;The World Cup goes far beyond a mere sporting event. It\'s going to be an interesting tool to promote social transformation.\&quot; In September 2008, Brazil\'s Ministry of Transport announced a high-speed rail project for the World Cup connecting Campinas, S&atilde;o Paulo and Rio de Janeiro. This would cost R$11 billion (approx. US$ 6.25 billion, &pound; 4.1 billion).[20] The technology will most likely be provided by companies from France, Japan, South Korea or Germany which will form consortia with Brazilian engineering firms. However, on 2 July 2010, it was announced that the line is now not expected to open before late-2016.[21] Recife International Airport in Recife. On 31 August 2009 the state airport management agency Infraero unveiled a R$ 5.3 billion (approx. US$ 3 billion, &pound; 2 billion) investment plan[22] to upgrade airports of ten of the venue cities, increasing their capacity and comfort for the hundreds of thousands of tourists expected for the Cup. Natal and Salvador are excluded because their upgrade works have been recently completed. A significant amount (55.3%) of the money will be spent overhauling the airports of S&atilde;o Paulo and Rio de Janeiro. The investment figure covers works to be carried out up to 2014. The announcement by Infraero came in reply to criticism made by the Brazilian General Aviation Association, a grouping of private aircraft owners, that Brazil\'s airports currently could not cope with the World Cup inflow. The vice-president of the association, Adalberto Febeliano, told reporters that more than 500,000 football fans were expected, with each one taking between six and fourteen flights during the tournament to get to the games in various cities.[23] The majority of Brazil\'s airports were built before the end of World War II, and several were at saturation point in terms of passengers, according to the association. It added that it should be possible to renovate the facilities \&quot;within three or four years\&quot; if the political will exists. Infraero said in a statement: \&quot;In the race against time, Infraero is making sure that the sixty-seven airports in its network are in perfect condition and can welcome in comfort and security passengers in Brazil and from abroad.\&quot; In May 2010, the Government of Brazil changed the bidding legislation to allow more flexibility to Infraero.[24] The Brazilian federal government has earmarked R$ 3 billion (US$ 1.8 billion, &pound; 1.1 billion) for investment in works relating to the 2014 World Cup, and intends to release a package of works, entitled the FIFA World Cup PAC (Portuguese acronym for Growth Acceleration Programme). According to the Brazilian minister of cities, M&aacute;rcio Fortes, the bulk o f funds should go to works pertaining to the tournament itself, but the total figure will only be defined after a meeting with representatives of the municipalities that will host the matches. \&quot;This is only an initial figure. We have not set a figure yet. These R$3 billion will allow us to take the first step. The total value of projects is not known yet. We are going to hold talks with mayors to learn which projects are priorities,\&quot; said the minister. The funds will be supplied by Pr&oacute;-Transporte, a financing programme funded by the Severance Pay Indemnity Fund (FGTS) whose regulation was passed last year by the fund\'s Board of Curators. According to Fortes, several city councils have already contacted the ministry and showed interest in partnership for carrying out infrastructure work turned exclusively to the Cup that will be held in Brazil. \&quot;For some time now, the city councils that will host the matches have been contacting us. The city councils have had meetings with FIFA and several projects were outlined. Our approach consists of dealing only with projects exclusively turned to the Cup. Our goal right now is not to solve transport-related issues in the city. We are going to help solve the issues pertaining to the events,\&quot; he stated. According to the minister, another factor to be analysed by the Ministry of Cities is usefulness and sustainability of the investment after the competition is over. \&quot;We are not going to deal with huge projects. The cheapest and most efficient means of transport will be used. Of course, each case will be analysed separately,\&quot; he explained. CPTM train in Greater S&atilde;o Paulo Fortes stated that the PAC of the Cup is going to include partnerships with city councils and state governments, as well as some partnerships with the private sector. \&quot;The keyword is partnership. The federal government will not undertake anything by itself. It will be similar to the infrastructure PAC, in which we already have partnerships with city councils and state governments, as well as public-private partnerships. We are going to review the type of investment proposed, analyse their size, and the need for private sector participation, which may take place in different ways. The private sector may build and then lease the assets, or perhaps operate them. All of that will be discussed,\&quot; he stated. The minister also informed that preparations for the World Cup already include the creation of a line of financing for renewing the bus fleet across the country. The line will be made available by the Brazilian Federal Savings Bank with total funds of R$1 billion (US$ 600 million, &pound; 375 million).[25] Brazil\'s federal government announced on 17 May 2010 that it shall be granting tax breaks for the construction and refurbishment of the stadiums for the 2014 World Cup. In a note, the Ministry for the Treasury said that it shall be \&quot;granting tax exemption to the stadiums of the World Cup, which shall not need to pay Industrialized Products Tax (Imposto sobre Produtos Industrializados - IPI), Importation Tax (Imposto de Importa&ccedil;&atilde;o - II) or social contributions (PIS/COFINS).\&quot; In addition, the 12 cities that shall be hosting the World Cup matches shall be able to grant exemption from State Value Added Tax (Imposto sobre Circula&ccedil;&atilde;o de Mercadorias e Servi&ccedil;os - ICMS) on all operations involving merchandise and other goods for the construction or the refurbishment of the stadiums. \&quot;Conditional on the cumulative concession of the benefits involving Importation Tax, IPI and PIS/COFINS, the exemption of ICMS on imports shall only be applicable if the goods do not have a similar product produced nationally,\&quot; the note informed, adding that this decision shall be made feasible through a Law or Provisional Measure. In September 2009, the Brazilian Development Bank (Banco Nacional de Desenvolvimento Econ&ocirc;mico e Social - BNDES) opened a credit line of R$ 4.8 billion (approx. US$ 2.7 billion, &pound; 1.75 billion) for the World Cup stadiums. Each host city shall be able to finance up to R$ 400 million (approx. US$ 225 million, &pound; 145 million) or 75% of the project, with bank funds</p>
    <p></p>
<p></p>
    </div>
    <!-- FIM DO TEXTO -->
    
    </div>
    <!-- FIM DA DIREITA -->  
  
  <br class="limpa_float" />  
  </div>
  <!-- FIM DO CONTE�DO -->
  </div>
  <!-- FIM DA DEFESA -->
  <!-- BASE -->
  <div id="base">
    
    <!-- DEFESA DA BASE -->
    <div id="defesa_base">
      
    <div class="left">
    <p class="endereco">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
      CEP 01.424-001<br />
      Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
      S&atilde;o Paulo/SP - Brasil<br />
      email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
    <p><a href="../index.php">Home</a> |   <a href="../avisos-legais.php">Legal terms</a> |   <a href="../mapa-do-site.php">Sitemap</a> |   <a href="../subscreva.php">Subscribe </a></p>
    </div>
    
    <div class="right">
    <ul id="menu_base">
        <li class="acompanhe">Follow us:</li>
        <li class="social">
        <a class="facebook" href="https://www.facebook.com/carlezzo.advogados.associados" title="Facebook" target="_blank">Facebook</a> 
        <a class="linkedin" href="http://www.linkedin.com/company/1294492" title="Linkedin" target="_blank">Linkedin</a> 
        <a class="youtube" href="http://www.youtube.com/user/carlezzo" title="Youtube" target="_blank">Youtube</a>
        </li>
      </ul>
      <br class="limpa_float" /> 
    <a class="assinatura" href="http://www.agencia10clic.com.br" target="_blank" title="Ag&ecirc;ncia 10Clic">Ag&ecirc;ncia 10Clic</a>
    <p>&copy;2011 Carlezzo Advogados. All rights reserved</p>
    </div>
    
    </div>
    <!-- FIM DA DEFESA DA BASE -->
    
  </div>
  <!-- FIM DA BASE -->
<!-- SCRIPT GOOGLE ANALYTICS - AG�NCIA 10CLIC - OTIMIZA��O -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10514770-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script><br />
<!-- FIM DO SCRIPT GOOGLE ANALYTICS -->
</body>
</html>