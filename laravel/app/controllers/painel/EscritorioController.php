<?php

namespace Painel;

use \Escritorio, \Input, \Validator, \Redirect, \Session, \CropImage;

class EscritorioController extends BasePainelController {

    private $validation_rules = [
        'texto_pt' => 'required',
        'texto_en' => 'required',
        'texto_es' => 'required'
    ];

    public function index()
    {
        $escritorio = Escritorio::all();

        return $this->view('painel.escritorio.index', compact('escritorio'));
    }

    public function edit($id)
    {
        $escritorio = Escritorio::findOrFail($id);

        return $this->view('painel.escritorio.edit', compact('escritorio'));
    }

    public function update($id)
    {
        $escritorio = Escritorio::findOrFail($id);
        $input      = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $escritorio->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.escritorio.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}
