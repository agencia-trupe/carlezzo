@section('content')

    <legend>
        <h2><small>O que fazemos para /</small> Editar Item</h2>
    </legend>

    {{ Form::model($fazemos, [
        'route' => ['painel.atuacao.fazemos.update', $fazemos->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.fazemos._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
