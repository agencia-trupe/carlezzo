<?php

namespace Painel;

use \Atuacao, \Input, \Session, \Redirect, \Validator;

class AtuacaoController extends BasePainelController {

    private $validation_rules = [
        'texto_pt' => 'required',
        'texto_en' => 'required',
        'texto_es' => 'required'
    ];

    public function index()
    {
    	$atuacao = Atuacao::first();

        return $this->view('painel.atuacao.index', compact('atuacao'));
    }

    public function update($id)
    {
        $atuacao = Atuacao::findOrFail($id);
        $input   = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $atuacao->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.atuacao.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}
