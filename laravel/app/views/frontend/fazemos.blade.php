@section('content')

    <div class="menu-sub">
            <a href="{{ route('atuacao') }}">{{ Lang::get('frontend.atuacao.atuacao') }}</a>
            <a href="{{ route('atuacao', 'pratica-legal') }}">{{ Lang::get('frontend.atuacao.pratica') }}</a>
            <a href="{{ route('atuacao', 'o-que-fazemos-para') }}" class="active">{{ Lang::get('frontend.atuacao.fazemos') }}</a>
    </div>

    <div class="conteudo-sub fazemos">
        <div class="titulo">
            <h2>{{ Lang::get('frontend.atuacao.fazemos') }}</h2>
        </div>

        <div class="conteudo">
            @if(count($items))
            @foreach($items as $item)
            <div class="item-handle">
                <a href="#{{ $item->id }}" class="handle" data-id="{{ $item->id }}">{{ $item->{Lang::get('frontend.campos.titulo')} }}</a>
                <div class="item-content">
                    {{ $item->{Lang::get('frontend.campos.texto')} }}
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>

@stop
