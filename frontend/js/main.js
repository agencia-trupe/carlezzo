(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.init = function() {
        $('#banners').cycle({
            fx: 'scrollHorz',
            slides: '>div',
            next: '#next',
            prev: '#prev'
        });

        $('.item-handle a').click(function(e) {
            e.preventDefault();
            var _this = $(this);

            _this.parent().toggleClass('active');
            _this.next().slideToggle();
        });

        $('.busca input[type=text]').focus(function() {
            $('.busca').toggleClass('foco');
        }).blur(function() {
            $('.busca').toggleClass('foco');
        });

        $('#form-contato').on('submit', function(event) {
            event.preventDefault();

            var $formContato = $(this);
            var $formContatoSubmit = $formContato.find('input[type=submit]');
            var $formContatoResposta = $formContato.find('#form-resposta');

            $formContatoResposta.hide();

            $.post(BASE + '/contato', {

                nome     : $('#nome').val(),
                email    : $('#email').val(),
                telefone : $('#telefone').val(),
                mensagem : $('#mensagem').val()

            }, function(data) {

                if (data.status == 'success') $formContato[0].reset();
                $formContatoResposta.hide().text(data.message).fadeIn('slow');

            }, 'json');
        });

        $('#mobile-toggle').on('click touchstart', function(event) {
            event.preventDefault();

            var $handle = $(this),
                $nav    = $('header nav#mobile');

            $nav.slideToggle();
            $handle.toggleClass('close');
        });

        var $handles = $('.item-handle');
        if ($handles.length) {
            var hash = (window.location.hash).replace('#', '');
            if (!hash) return;

            var $el = $('.handle[data-id="' + hash + '"]');
            $el.trigger('click');
            $('html, body').animate({
                scrollTop: $el.offset().top
            }, 1000);
        }
    };

    $(document).ready(App.init);

}(window, document, jQuery));
