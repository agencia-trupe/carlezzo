@section('content')

    <legend>
        <h2><small>Escritório /</small> {{ $escritorio->titulo_pt }}</h2>
    </legend>

    {{ Form::model($escritorio, [
        'route' => ['painel.escritorio.update', $escritorio->id],
        'method' => 'patch'])
    }}

        @include('painel.escritorio._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
