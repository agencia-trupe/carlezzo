<?php

use \Noticia, \Corporativo;

class BuscaController extends BaseController {

    public function index()
    {
        $idioma = Config::get('app.locale');
        $termo  = Input::get('termo');

        $noticias    = Noticia::with('equipe')->idioma($idioma)->busca($termo)->ordenados()->get();
        $corporativo = Corporativo::with('equipe')->idioma($idioma)->busca($termo)->ordenados()->get();

        return $this->view('frontend.busca', compact('termo', 'noticias', 'corporativo'));
    }

}
