@section('content')

    <legend>
        <h2><small>Corporativo /</small> Editar Notícia</h2>
    </legend>

    {{ Form::model($noticia, [
        'route' => ['painel.noticias.corporativo.update', $noticia->id],
        'method' => 'patch'])
    }}

        @include('painel.corporativo._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
