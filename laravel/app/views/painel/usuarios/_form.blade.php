@if($errors->any())
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $errors->first() }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('username', 'Usuário') }}
    {{ Form::text('username', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('email', 'E-mail') }}
    {{ Form::email('email', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('password', 'Senha') }}
    {{ Form::password('password', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('password_confirm', 'Confirmação de Senha') }}
    {{ Form::password('password_confirm', ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.usuarios.index') }}" class="btn btn-default btn-voltar">Voltar</a>