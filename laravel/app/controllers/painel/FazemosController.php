<?php

namespace Painel;

use \Fazemos, \Input, \Validator, \Redirect, \Session, \CropImage;

class FazemosController extends BasePainelController {

    private $validation_rules = [
        'titulo_pt'    => 'required',
        'titulo_en'    => 'required',
        'titulo_es'    => 'required',
        'texto_pt'     => 'required',
        'texto_en'     => 'required',
        'texto_es'     => 'required',
    ];

    public function index()
    {
        $fazemos = Fazemos::ordenados()->get();

        return $this->view('painel.fazemos.index', compact('fazemos'));
    }

    public function create()
    {
        return $this->view('painel.fazemos.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            Fazemos::create($input);
            Session::flash('sucesso', 'Item adicionado com sucesso.');

            return Redirect::route('painel.atuacao.fazemos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar item.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $fazemos = Fazemos::findOrFail($id);

        return $this->view('painel.fazemos.edit', compact('fazemos'));
    }

    public function update($id)
    {
        $fazemos = Fazemos::findOrFail($id);
        $input   = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $fazemos->update($input);
            Session::flash('sucesso', 'Item alterado com sucesso.');

            return Redirect::route('painel.atuacao.fazemos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar item.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Fazemos::destroy($id);
            Session::flash('sucesso', 'Item removido com sucesso.');

            return Redirect::route('painel.atuacao.fazemos.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover item.']);

        }
    }

}
