@section('content')

    <legend>
        <h2>Editar Equipe</h2>
    </legend>

    {{ Form::model($equipe, [
        'route' => ['painel.equipe.update', $equipe->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.equipe._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
