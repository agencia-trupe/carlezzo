<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorporativoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('corporativo', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('data');
			$table->string('idioma');
			$table->integer('equipe_id');

			$table->string('titulo');
			$table->string('slug')->unique();

			$table->text('texto');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('corporativo');
	}

}
