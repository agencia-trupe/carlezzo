<?php

use \Escritorio;

class EscritorioController extends BaseController {

    public function index($slug = null)
    {
        if (!$slug) $slug = 'nossa-historia';

        $escritorio = Escritorio::slug($slug)->first();
        if (!$escritorio) App::abort('404');

        $subs = Escritorio::get(['slug', 'titulo_pt', 'titulo_en', 'titulo_es']);

        return $this->view('frontend.escritorio', compact('escritorio', 'subs'));
    }

}
