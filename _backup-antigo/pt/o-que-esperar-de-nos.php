<?php
	
	//-------------------- Conecta com o Banco de Dados ---------------------------
	include "../_includes/conexao.php";	
	
	//Busca not�cias
	$busca_noticias = mysql_query("SELECT * FROM noticias WHERE status = '1' AND titulo_pt != '0'  AND destaque != '1' ORDER BY data DESC LIMIT 0, 2");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- META TAGS - AG�NCIA 10CLIC - OTIMIZA��O -->
<meta name="author" content="10Clic - www.10clic.com.br" />
<meta name="description" content="Escrit�rio de advocacia especializado em esporte, entretenimento e m�dia, com ampla atua��o internacional em temas ligados ao futebol, FIFA e atletas.
" />
<meta name="keywords" content="FIFA,UEFA,CAS,court of arbitration for sport,futebol, agentes de jogadores, atletas, mecanismo de solidariedade, compensa��o por forma��o, cinema,m�sica,celebridades, publicidade, marketing,televis�o, propaganda, shows, r�dio,internet, jogos eletr�nicos, gaming,m�dia,novas tecnologias,m�dias sociais,software,fundos de investimento,direito desport" />
<meta name="robots" content="index, follow, archive" />
<meta name="googlebot" content="archive" />
<!-- FIM DAS META TAGS -->
<title>Carlezzo Advogados - Miss&atilde;o e Valores</title>
<!-- FOLHAS DE ESTILO -->
<link href="../_css/estilo.css" rel="stylesheet" type="text/css" />
<link href="../_css/nivo-slider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../_ajax/skin.css" rel="stylesheet" type="text/css" />
<!-- FIM DAS FOLHAS DE ESTILO -->
<!-- SCRIPTS -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'49de9f94-6555-4ec3-ba64-437c8a161ebe'});</script>
<script type="text/javascript" src="../_ajax/jquery.js"></script> 
<script type="text/javascript" src="../_ajax/menu.jquery.js"></script> 
<!-- FIM DOS SCRIPTS -->
</head>

<body class="interna">
<!-- DEFESA -->
<div id="defesa">
  <!-- TOPO -->
  <div id="topo">
  	
    <!-- LINHA -->  
    <div id="linha">
    
    <form action="busca.php" method="post" id="busca">
    <input class="campo" name="palavra" type="text" />
    <input name="" type="image" value="ok" src="../_imagens/botao_buscar.jpg" alt="Buscar" class="botao" />
    </form>
    <span displayText="" class="st_sharethis_custom"></span>
    <p class="linguas_pt">
    <a href="../en/<?=basename($_SERVER['PHP_SELF']);?>" title="English">English</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" title="Espa&ntilde;ol">Espa&ntilde;ol</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:window.print()" title="Imprima esta p&aacute;gina"><img src="../_imagens/a_print.jpg" alt="Imprima esta p&aacute;gina" align="absmiddle" /></a>&nbsp;&nbsp;<a href="mailto:carlezzo@carlezzo.com.br" title="Contato r&aacute;pido"><img src="../_imagens/a_mail.jpg" alt="Contato r&aacute;pido" align="absmiddle" /></a>
    </p>    
    </div>
    <!-- FIM DA LINHA -->
    
    <!-- LOGO -->
    <h1><a href="index.php" title="Carlezzo Advogados">Carlezzo Advogados</a></h1>
    <!-- FIM DO LOGO -->
    
    <!-- MENU TOPO -->
    <div id="menu_topo">
    	<ul class="topnav">
            <li><a href="index.php">Home</a></li>
            <li>
                <a href="quem-somos.php">Quem somos</a>

              <ul class="subnav">
                    <li><a href="quem-somos.php">Vis&atilde;o Geral</a></li>
                    <li><a href="missao-e-valores.php">Miss&atilde;o e Valores</a></li>
                    <li><a href="o-que-esperar-de-nos.php">O qu&ecirc; esperar de n&oacute;s</a></li>
                    <li><a href="aliancas-internacionais.php">Alian&ccedil;as Internacionais</a></li>
                    <li><a href="recrutamento.php">Recrutamento</a></li>
                    <li><a href="pro-bono.php">Pr&oacute;-bono</a></li>
              </ul>
          </li>
            <li>
                <a href="setores.php">Setores</a>
              <ul class="subnav">
                    <li><a href="setores.php?categoria=esporte">Esporte</a></li>
     				<li><a href="setores.php?categoria=entretenimento">Entretenimento</a></li>
      				<li><a href="setores.php?categoria=midia">M&iacute;dia e Novas Tecnologias</a></li>
              </ul>
          </li>
          <li><a href="equipe.php">Equipe</a></li>

          <li><a href="noticias.php">Not&iacute;cias</a></li>
          <li><a href="publicacoes.php">Publica&ccedil;&otilde;es</a></li>
          <li class="last"><a href="onde-estamos.php">Onde Estamos</a></li>
      </ul>
    </div>
    <!-- FIM DO MENU TOPO -->
  
  </div>
  <!-- FIM DO TOPO -->
  
  <!-- CONTE�DO -->
  <div id="conteudo">
    
    <!-- ESQUERDA -->
    <div class="esquerda_interna">
    
    <div class="bloco_sidebar">
      <h2 class="titulo">QUEM SOMOS</h2>
      <ul class="menu_esquerda">
            <li><a href="quem-somos.php">Vis&atilde;o Geral</a></li>
            <li><a href="missao-e-valores.php">Miss&atilde;o e Valores</a></li>
            <li><a href="o-que-esperar-de-nos.php">O qu&ecirc; esperar de n&oacute;s</a></li>
            <li><a href="aliancas-internacionais.php">Alian&ccedil;as Internacionais</a></li>
            <li><a href="recrutamento.php">Recrutamento</a></li>
            <li><a href="pro-bono.php">Pr&oacute;-bono</a></li>
        </ul>
    </div>
    
    
    <!-- CHAMADA -->
    <div class="chamada"><img src="../_imagens/chamada_quem_somos.jpg" width="242" height="318" /></div>
    <!-- FIM DA CHAMADA -->
    
    
    
    
    
    <br class="limpa_float" />
    </div>
    <!-- FIM DA ESQUERDA --> 
    
    <!-- DIREITA -->
    <div class="direita_interna">

    
    <!-- TEXTO -->
    <div class="texto">
    <h5 class="square">O QU&Ecirc; ESPERAR<br />
DE N&Oacute;S</h5>
    <h3 class="green">QUALIDADE</h3>
    <p> Princ&iacute;pio b&aacute;sico que rege a presta&ccedil;&atilde;o de servi&ccedil;os  neste escrit&oacute;rio, a qualidade deve estar presente em todas as nossas atividades  operacionais, sejam elas administrativas ou jur&iacute;dicas.</p>
    <h3 class="green">PERSIST&Ecirc;NCIA</h3>
    <p> N&atilde;o  desistimos at&eacute; que nosso cliente esteja satisfeito com o trabalho.  Independentemente da complexidade do caso em que estejamos envolvidos, &eacute;  fundamental para n&oacute;s que o cliente tenha a exata no&ccedil;&atilde;o do que est&aacute; sendo feito  e que, ao fim, tenha ficado satisfeito com nossa presta&ccedil;&atilde;o de servi&ccedil;os.</p>
    <h3 class="green">ATUALIZA&Ccedil;&Atilde;O</h3>
    <p> Estar atento as informa&ccedil;&otilde;es di&aacute;rias, as mudan&ccedil;as  legislativas, aos mercados que atuamos, ao desenvolvimento da economia e as  novas oportunidades de neg&oacute;cios que surjam s&atilde;o elementos chave de nossos servi&ccedil;os.</p>
    <h3 class="green">ABRANG&Ecirc;NCIA INTERNACIONAL</h3>
    <p> Temos a preocupa&ccedil;&atilde;o de p&ocirc;r a disposi&ccedil;&atilde;o de nossos  clientes servi&ccedil;os jur&iacute;dicos em diferentes jurisdi&ccedil;&otilde;es mediante as coopera&ccedil;&otilde;es  que mantemos com escrit&oacute;rios de dezenas de pa&iacute;ses. Assim, quando uma demanda  internacional surgir, caso n&atilde;o possamos atend&ecirc;-la devido a falta de habilita&ccedil;&atilde;o  legal para atuar naquele pa&iacute;s, teremos parceiros estrat&eacute;gicos aptos a assumir o  assunto.</p>
    <h3 class="green">LONGO PRAZO</h3>
    <p> Buscamos todos os dias fornecer solu&ccedil;&otilde;es que n&atilde;o  apenas sejam &uacute;teis no curto prazo, mas sim que consigam tamb&eacute;m atend&ecirc;-lo no  longo prazo.</p>
    <h3 class="green">RESPEITO</h3>
    <p> Respeitamos as especificidades de nossos clientes e  sabemos que cada um merece uma an&aacute;lise absolutamente individualizada do seu  caso. </p>
    <h3 class="green">PENSANDO GLOBAL</h3>
    <p> O exerc&iacute;cio de qualquer profiss&atilde;o atualmente demanda  das pessoas que a executam uma vis&atilde;o global de neg&oacute;cios. O mundo est&aacute; conectado  e as economias s&atilde;o afetadas por informa&ccedil;&otilde;es que espalham-se em minutos pelos  canais cibern&eacute;ticos. Assim, pensar de maneira global &eacute; n&atilde;o apenas estar na  frente, mas significa, sobretudo, estar atualizado e capacitado a entregar ao  cliente um servi&ccedil;o que agregue valor ao seu neg&oacute;cio. N&oacute;s do Carlezzo Advogados  temos a exata no&ccedil;&atilde;o da import&acirc;ncia da inser&ccedil;&atilde;o no mundo global. Por isso nossos  advogados freq&uuml;entemente viajam pelo mundo afora buscando aperfei&ccedil;oar-se,  trazer novas id&eacute;ias, manter atualizado seu entendimento sobre neg&oacute;cios e estar  ciente dos desafios que o dia-a-dia imp&otilde;e.</p>
    <h3 class="green">PENSANDO O NOVO</h3>
    <p> Somos um escrit&oacute;rio que nasceu em meio a esta nova  sociedade da informa&ccedil;&atilde;o, globalizada, r&aacute;pida e eficiente. Adotamos como  filosofia a busca de jovens talentos que se enquadrem neste novo contexto e que  sempre estejam atr&aacute;s de apreender coisas novas. Ao estarmos em constante  aprendizado nossos advogados s&atilde;o estimulados a pensar o novo, a buscar novas  &aacute;reas e novos neg&oacute;cios, a apresentar a nossos clientes solu&ccedil;&otilde;es que eles n&atilde;o  imaginavam que seriam poss&iacute;veis. Inovar &eacute; fundamental.</p>
    <h3 class="green">PENSANDO EM VOC&Ecirc;</h3>
    <p> Tudo o que fazemos tem uma raz&atilde;o: voc&ecirc;, nosso  cliente. &Eacute; pela satisfa&ccedil;&atilde;o de sabermos que os clientes nos honram ao transferir  aos nossos cuidados assuntos que muitas vezes lhe s&atilde;o t&atilde;o sens&iacute;veis que  sentimos profundamente orgulhosos pela confian&ccedil;a depositada.</p>
    <p>&nbsp;</p></div>
    <!-- FIM DO TEXTO -->
    
    </div>
    <!-- FIM DA DIREITA -->  
  
  <br class="limpa_float" />  
  </div>
  <!-- FIM DO CONTE�DO -->
  </div>
  <!-- FIM DA DEFESA -->
  <!-- BASE -->
  <div id="base">
    
    <!-- DEFESA DA BASE -->
    <div id="defesa_base">
      
    <div class="left">
    <p class="endereco">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
      CEP 01.424-001<br />
      Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
      S&atilde;o Paulo/SP - Brasil<br />
      email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
    <p><a href="index.php">Home</a> |   <a href="avisos-legais.php">Avisos legais</a> |   <a href="mapa-do-site.php">Mapa do site</a> |   <a href="subscreva.php">Subscreva </a></p>
    </div>
    
    <div class="right">
    <ul id="menu_base">
        <li class="acompanhe">Siga-nos:</li>
        <li class="social">
        <a class="facebook" href="https://www.facebook.com/carlezzo.advogados.associados" title="Facebook" target="_blank">Facebook</a> 
        <a class="linkedin" href="http://www.linkedin.com/company/1294492" title="Linkedin" target="_blank">Linkedin</a> 
        <a class="youtube" href="http://www.youtube.com/user/carlezzo" title="Youtube" target="_blank">Youtube</a>
        </li>
      </ul>
      <br class="limpa_float" /> 
    <a class="assinatura" href="http://www.agencia10clic.com.br" target="_blank" title="Ag&ecirc;ncia 10Clic">Ag&ecirc;ncia 10Clic</a>
    <p>&copy;2011 Carlezzo Advogados. Todos os direitos reservados</p>
    </div>
    
    </div>
    <!-- FIM DA DEFESA DA BASE -->
    
  </div>
  <!-- FIM DA BASE -->
<!-- SCRIPT GOOGLE ANALYTICS - AG�NCIA 10CLIC - OTIMIZA��O -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10514770-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script><br />
<!-- FIM DO SCRIPT GOOGLE ANALYTICS -->
</body>
</html>