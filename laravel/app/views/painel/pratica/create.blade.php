@section('content')

    <legend>
        <h2><small>Prática Legal /</small> Adicionar Item</h2>
    </legend>

    {{ Form::open(['route' => 'painel.atuacao.pratica.store', 'files' => true]) }}

        @include('painel.pratica._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
