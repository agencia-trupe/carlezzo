<?php

class Corporativo extends Eloquent
{

    protected $table = 'corporativo';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function equipe()
    {
        return $this->belongsTo('Equipe');
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function scopeIdioma($query, $idioma){
        return $query->where('idioma', $idioma);
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon\Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function scopeBusca($query, $termo)
    {
        return empty($termo) ? $query : $query->whereRaw("MATCH(titulo,texto) AGAINST(? IN BOOLEAN MODE)",[$termo]);
    }

}
