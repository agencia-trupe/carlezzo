<?php

namespace Painel;

use \Noticia, \Equipe, \Input, \Validator, \Redirect, \Session, \Str;

class NoticiasController extends BasePainelController {

    private $validation_rules = [
        'data'   => 'required',
        'idioma' => 'required',
        'titulo' => 'required',
        'texto'  => 'required',
    ];

    public function index()
    {
        $idioma = Input::get('idioma');
        if(!Input::has('idioma') || ($idioma != 'pt' && $idioma != 'en' && $idioma != 'es'))
            $idioma = 'pt';

        $noticias = Noticia::idioma($idioma)->ordenados()->paginate(10);

        return $this->view('painel.noticias.index', compact('noticias', 'idioma'));
    }

    public function create()
    {
        $equipe = Equipe::lists('nome', 'id');

        return $this->view('painel.noticias.create', compact('equipe'));
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            Noticia::create($input);
            Session::flash('sucesso', 'Notícia adicionada com sucesso.');

            return Redirect::route('painel.noticias.index', ['idioma' => $input['idioma']]);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar notícia. Verifique se já existe outra notícia com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $noticia = Noticia::findOrFail($id);
        $equipe  = Equipe::lists('nome', 'id');

        return $this->view('painel.noticias.edit', compact('noticia', 'equipe'));
    }

    public function update($id)
    {
        $noticia = Noticia::findOrFail($id);
        $input   = Input::all();

        $input['slug'] = Str::slug(Input::get('titulo'));

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $noticia->update($input);
            Session::flash('sucesso', 'Notícia alterada com sucesso.');

            return Redirect::route('painel.noticias.index', ['idioma' => $input['idioma']]);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar notícia.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            $idioma = Noticia::find($id)->idioma;
            Noticia::destroy($id);
            Session::flash('sucesso', 'Notícia removida com sucesso.');

            return Redirect::route('painel.noticias.index', ['idioma' => $idioma]);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover notícia.']);

        }
    }

}
