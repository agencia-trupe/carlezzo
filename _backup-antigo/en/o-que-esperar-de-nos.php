<?php
	
	//-------------------- Conecta com o Banco de Dados ---------------------------
	include "../_includes/conexao.php";	
	
	//Busca not�cias
	$busca_noticias = mysql_query("SELECT * FROM noticias WHERE status = '1' AND titulo_en != '0'  AND destaque != '1' ORDER BY data DESC LIMIT 0, 2");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- META TAGS - AG�NCIA 10CLIC - OTIMIZA��O -->
<meta name="author" content="10Clic - www.10clic.com.br" />
<meta name="description" content="Law firm specialized in sports, entertainment and media, with strong presence in football and international affairs, as well as before FIFA and CBF
" />
<meta name="keywords" content="FIFA, UEFA, Court of arbitration for sport, football, player's agents, athletes, clubs, solidarity mechanism, training compensation, transfers, cinema, sports law, music, concerts, models, celebrities, video-game, media, new technologies, intellectual property, advertising, brands, marketing, Internet, TV, radio, mobile, world cup, Olympic games
" />
<meta name="robots" content="index, follow, archive" />
<meta name="googlebot" content="archive" />
<!-- FIM DAS META TAGS -->
<title>Carlezzo Advogados - Mission and Values</title>
<!-- FOLHAS DE ESTILO -->
<link href="../_css/estilo.css" rel="stylesheet" type="text/css" />
<link href="../_css/nivo-slider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../_ajax/skin.css" rel="stylesheet" type="text/css" />
<!-- FIM DAS FOLHAS DE ESTILO -->
<!-- SCRIPTS -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'49de9f94-6555-4ec3-ba64-437c8a161ebe'});</script>
<script type="text/javascript" src="../_ajax/jquery.js"></script> 
<script type="text/javascript" src="../_ajax/menu.jquery.js"></script> 
<!-- FIM DOS SCRIPTS -->
</head>

<body class="interna">
<!-- DEFESA -->
<div id="defesa">
  <!-- TOPO -->
  <div id="topo">
  	
    <!-- LINHA -->  
    <div id="linha">
    
    <form action="busca.php" method="post" id="busca">
    <input class="campo" name="palavra" type="text" />
    <input name="" type="image" value="ok" src="../_imagens/botao_buscar.jpg" alt="Search" class="botao" />
    </form>
    <span displayText="" class="st_sharethis_custom"></span>
    <p class="linguas">
    <a href="../pt/<?=basename($_SERVER['PHP_SELF']);?>" title="Portugu&ecirc;s">Portugu&ecirc;s</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" title="Espa&ntilde;ol">Espa&ntilde;ol</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:window.print()" title="Print this page"><img src="../_imagens/a_print.jpg" alt="Print this page" align="absmiddle" /></a>&nbsp;&nbsp;<a href="mailto:carlezzo@carlezzo.com.br" title="Quick contact"><img src="../_imagens/a_mail.jpg" alt="Quick contact" align="absmiddle" /></a>
    </p>    
    </div>
    <!-- FIM DA LINHA -->
    
    <!-- LOGO -->
    <h1><a href="index.php" title="Carlezzo Advogados">Carlezzo Advogados</a></h1>
    <!-- FIM DO LOGO -->
    
    <!-- MENU TOPO -->
    <div id="menu_topo">
    	<ul class="topnav">
            <li><a href="index.php">Home</a></li>
            <li>
                <a href="quem-somos.php">About us</a>

              <ul class="subnav">
                    <li><a href="quem-somos.php">Overview</a></li>
                    <li><a href="missao-e-valores.php">Mission and Values</a></li>
                    <li><a href="o-que-esperar-de-nos.php">What you can wait from us</a></li>
                    <li><a href="aliancas-internacionais.php">International Alliances</a></li>
                    <li><a href="recrutamento.php">Recruitment</a></li>
                    <li><a href="pro-bono.php">Pro-bono</a></li>
              </ul>
          </li>
            <li>
                <a href="setores.php">Sectors</a>
              <ul class="subnav">
                    <li><a href="setores.php?categoria=esporte">Sport</a></li>
     				<li><a href="setores.php?categoria=entretenimento">Entertainment</a></li>
      				<li><a href="setores.php?categoria=midia">Media and New Technologies</a></li>
              </ul>
          </li>
          <li><a href="equipe.php">Team</a></li>

          <li><a href="noticias.php">News</a></li>
          <li><a href="publicacoes.php">Publications</a></li>
          <li class="last"><a href="onde-estamos.php">Where are we</a></li>
      </ul>
    </div>
    <!-- FIM DO MENU TOPO -->
  
  </div>
  <!-- FIM DO TOPO -->
  
  <!-- CONTE�DO -->
  <div id="conteudo">
    
    <!-- ESQUERDA -->
    <div class="esquerda_interna">
    
    <div class="bloco_sidebar">
      <h2 class="titulo">ABOUT US</h2>
      <ul class="menu_esquerda">
            <li><a href="quem-somos.php">Overview</a></li>
            <li><a href="missao-e-valores.php">Mission and Values</a></li>
            <li><a href="o-que-esperar-de-nos.php">What you can wait from us</a></li>
            <li><a href="aliancas-internacionais.php">International Alliances</a></li>
            <li><a href="recrutamento.php">Recruitment</a></li>
            <li><a href="pro-bono.php">Pro-bono</a></li>
        </ul>
    </div>
    
    
    <!-- CHAMADA -->
    <div class="chamada"><img src="../_imagens/chamada_quem_somos.jpg" width="242" height="318" /></div>
    <!-- FIM DA CHAMADA -->
    
    
    
    
    
    <br class="limpa_float" />
    </div>
    <!-- FIM DA ESQUERDA --> 
    
    <!-- DIREITA -->
    <div class="direita_interna">

    
    <!-- TEXTO -->
    <div class="texto">
    <h5 class="square"> WHAT YOU CAN EXPECT FROM US</h5>
    <h3 class="green">QUALITY</h3>
    <p> Basic  principle that governs the delivery of services in this office, quality must be  present in all our operational activities, whether administrative or legal.</p>
    <h3 class="green">PERSISTENCY</h3>
    <p> We  do not give up until our customers are satisfied with the work. Regardless of  the complexity of the case we are involved, it is important to us that the  customer has an exact notion of what is being done and in the end to be  satisfied with our services.</p>
    <h3 class="green">UPDATING </h3>
    <p> The  awareness of daily information, legislative changes, the markets we serve,  development of the economy and new business opportunities that arise are key  elements of our services.</p>
    <h3 class="green">INTERNATIONAL  SCOPE </h3>
    <p> We  are concerned to make legal services available to our clients in different  jurisdictions through the cooperation we have with offices in dozens of  countries. Thus, when an international demand arises, in case we cannot meet it  due to lack of legal authorization to work in that country, we have strategic  partners able to undertake the matter.</p>
    <h3 class="green">LONG  TERM </h3>
    <p> We  strive every day to provide solutions that are not only useful in the short  term, but that can also serve clients in the long term.</p>
    <h3 class="green">RESPECT </h3>
    <p> We  respect the specificities of our clients and we know that each one deserves an  absolutely individualized analysis of his case. </p>
    <h3 class="green">GLOBAL  THINKING</h3>
    <p> The  exercise of any profession nowadays demands a global business vision from  people running it. The world is connected and the economies are affected by  information spread in minutes through the cyber channels. So, thinking globally  is not just being in front, but it means above all being upgraded and able to  provide clients a service that adds value to their businesses. We at Carlezzo  Advogados have an exact notion of the importance of the inclusion in the global  world. So our lawyers frequently travel around the world seeking to improve  themselves, bringing new ideas, keeping up their understanding of business and  being aware of the challenges that the day-to-day requires.</p>
    <h3 class="green">NEW  THINKING</h3>
    <p> We  are an office that was born in the midst of this new information society,  global, fast and efficient. We adopt the philosophy of looking for young  talents who fit this new context and that are always after new things to learn.  By being constantly learning, our lawyers are encouraged to think again, to  seek new areas and new businesses to provide our clients with solutions they  did not think were possible. Innovation is essential.</p>
    <h3 class="green">THINKING  ABOUT YOU</h3>
    <p> Everything  we do has a reason: you, our client. It is for the satisfaction of knowing that  clients honor us by transferring to our care issues that often are so sensitive  that we feel deeply proud for the trust.</p>
    <p>&nbsp;</p></div>
    <!-- FIM DO TEXTO -->
    
    </div>
    <!-- FIM DA DIREITA -->  
  
  <br class="limpa_float" />  
  </div>
  <!-- FIM DO CONTE�DO -->
  </div>
  <!-- FIM DA DEFESA -->
  <!-- BASE -->
  <div id="base">
    
    <!-- DEFESA DA BASE -->
    <div id="defesa_base">
      
    <div class="left">
    <p class="endereco">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
      CEP 01.424-001<br />
      Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
      S&atilde;o Paulo/SP - Brasil<br />
      email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
    <p><a href="index.php">Home</a> |   <a href="avisos-legais.php">Legal terms</a> |   <a href="mapa-do-site.php">Sitemap</a> |   <a href="subscreva.php">Subscribe </a></p>
    </div>
    
    <div class="right">
    <ul id="menu_base">
        <li class="acompanhe">Follow us:</li>
        <li class="social">
        <a class="facebook" href="https://www.facebook.com/carlezzo.advogados.associados" title="Facebook" target="_blank">Facebook</a> 
        <a class="linkedin" href="http://www.linkedin.com/company/1294492" title="Linkedin" target="_blank">Linkedin</a> 
        <a class="youtube" href="http://www.youtube.com/user/carlezzo" title="Youtube" target="_blank">Youtube</a>
        </li>
      </ul>
      <br class="limpa_float" /> 
    <a class="assinatura" href="http://www.agencia10clic.com.br" target="_blank" title="Ag&ecirc;ncia 10Clic">Ag&ecirc;ncia 10Clic</a>
    <p>&copy;2011 Carlezzo Advogados. All rights reserved</p>
    </div>
    
    </div>
    <!-- FIM DA DEFESA DA BASE -->
    
  </div>
  <!-- FIM DA BASE -->
<!-- SCRIPT GOOGLE ANALYTICS - AG�NCIA 10CLIC - OTIMIZA��O -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10514770-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script><br />
<!-- FIM DO SCRIPT GOOGLE ANALYTICS -->
</body>
</html>