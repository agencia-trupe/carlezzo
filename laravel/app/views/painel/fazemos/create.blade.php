@section('content')

    <legend>
        <h2><small>O que fazemos para /</small> Adicionar Item</h2>
    </legend>

    {{ Form::open(['route' => 'painel.atuacao.fazemos.store', 'files' => true]) }}

        @include('painel.fazemos._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
