<?php

use \Equipe, \Corporativo;

class EquipeController extends BaseController {

    public function index($id = null)
    {
        if (!$id) {
            $equipe = Equipe::ordenados()->get();

            return $this->view('frontend.equipe.index', compact('equipe'));
        } else {
            $equipe = Equipe::find($id);
            if (!$equipe) App::abort('404');

            $idioma = Config::get('app.locale');

            $corporativo = Corporativo::where('equipe_id', $equipe->id)->idioma($idioma)->ordenados()->limit(3)->get();

            return $this->view('frontend.equipe.interna', compact('equipe', 'corporativo'));
        }
    }

}
