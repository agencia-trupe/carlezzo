<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
                'telefone'    => '+55 11 3885-6091',
                'fax'         => '+55 11 3052-3610',
                'email'       => 'contato@carlezzo.com.br',
                'endereco'    => '<p>Al. Lorena, 800, 18º andar - Jardins</p><p>01424-001 - São Paulo/SP - Brasil</p>',
                'facebook'    => '',
                'linkedin'    => '',
                'youtube'     => '',
                'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3656.9993591365337!2d-46.6607782!3d-23.5684665!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59c532e241a3%3A0x1f5255e9c93e2be5!2sCondom%C3%ADnio+do+Edif%C3%ADcio+First+Office+Flat+-+Alameda+Lorena%2C+800+-+Jardim+Paulista%2C+S%C3%A3o+Paulo+-+SP%2C+01424-001!5e0!3m2!1spt-BR!2sbr!4v1432058516666" width="600" height="450" frameborder="0" style="border:0"></iframe>'
            )
        );

        DB::table('contato')->insert($data);
    }

}
