@section('content')

    <div class="equipe-home">
@foreach($equipe as $membro)
<a href="{{ route('equipe', $membro->id) }}" class="equipe-thumb">
    <img src="{{ asset('assets/img/equipe/'.$membro->imagem) }}" alt="">
    <div class="overlay">
        <span class="nome">{{ $membro->nome }}</span>
        <span class="cargo">{{ $membro->{Lang::get('frontend.campos.cargo')} }}</span>
    </div>
</a>@endforeach
    </div>

@stop
