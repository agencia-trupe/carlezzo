@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="well form-group">
    {{ HTML::decode(Form::label('imagem', 'Imagem <small>(294x360px)</small>')) }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/equipe/'.$equipe->imagem) }}" class="img-responsive">
@endif
    {{ Form::file('imagem', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('nome', 'Nome') }}
    {{ Form::text('nome', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('email', 'E-mail') }}
    {{ Form::text('email', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('telefone', 'Telefone') }}
    {{ Form::text('telefone', null, ['class' => 'form-control']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('cargo_pt', 'Cargo [PORTUGUÊS]') }}
    {{ Form::text('cargo_pt', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('texto_pt', 'Texto [PORTUGUÊS]') }}
    {{ Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('cargo_en', 'Cargo [INGLÊS]') }}
    {{ Form::text('cargo_en', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('texto_en', 'Texto [INGLÊS]') }}
    {{ Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('cargo_es', 'Cargo [ESPANHOL]') }}
    {{ Form::text('cargo_es', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('texto_es', 'Texto [ESPANHOL]') }}
    {{ Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.equipe.index') }}" class="btn btn-default btn-voltar">Voltar</a>
