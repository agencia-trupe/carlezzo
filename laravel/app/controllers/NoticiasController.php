<?php

use \Noticia, \Equipe;

class NoticiasController extends BaseController {

    public function index($slug = null)
    {
        $idioma = Config::get('app.locale');

        if (!$slug) {

            $noticias = Noticia::with('equipe')->idioma($idioma)->ordenados()->paginate(5);

            return $this->view('frontend.noticias.index', compact('noticias'));

        } else {

            $noticia = Noticia::slug($slug)->first();
            if (!$noticia) App::abort('404');

            return $this->view('frontend.noticias.noticia', compact('noticia'));

        }
    }

    public function equipe($id)
    {
        $idioma = Config::get('app.locale');
        $equipe = Equipe::find($id);
        if(!$equipe) App::abort('404');

        $noticias = Noticia::with('equipe')->idioma($idioma)->where('equipe_id', $id)->ordenados()->paginate(5);
        $membro   = $equipe->nome;

        return $this->view('frontend.noticias.index', compact('noticias', 'membro'));
    }

}
