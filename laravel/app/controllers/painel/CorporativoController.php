<?php

namespace Painel;

use \Corporativo, \Equipe, \Input, \Validator, \Redirect, \Session, \Str;

class CorporativoController extends BasePainelController {

    private $validation_rules = [
        'data'   => 'required',
        'idioma' => 'required',
        'titulo' => 'required',
        'texto'  => 'required',
    ];

    public function index()
    {
        $idioma = Input::get('idioma');
        if(!Input::has('idioma') || ($idioma != 'pt' && $idioma != 'en' && $idioma != 'es'))
            $idioma = 'pt';

        $noticias = Corporativo::idioma($idioma)->ordenados()->paginate(10);

        return $this->view('painel.corporativo.index', compact('noticias', 'idioma'));
    }

    public function create()
    {
        $equipe = Equipe::lists('nome', 'id');

        return $this->view('painel.corporativo.create', compact('equipe'));
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            Corporativo::create($input);
            Session::flash('sucesso', 'Notícia adicionada com sucesso.');

            return Redirect::route('painel.noticias.corporativo.index', ['idioma' => $input['idioma']]);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar notícia. Verifique se já existe outra notícia com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $noticia = Corporativo::findOrFail($id);
        $equipe  = Equipe::lists('nome', 'id');

        return $this->view('painel.corporativo.edit', compact('noticia', 'equipe'));
    }

    public function update($id)
    {
        $noticia = Corporativo::findOrFail($id);
        $input   = Input::all();

        $input['slug'] = Str::slug(Input::get('titulo'));

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $noticia->update($input);
            Session::flash('sucesso', 'Notícia alterada com sucesso.');

            return Redirect::route('painel.noticias.corporativo.index', ['idioma' => $input['idioma']]);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar notícia.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            $idioma = Corporativo::find($id)->idioma;
            Corporativo::destroy($id);
            Session::flash('sucesso', 'Notícia removida com sucesso.');

            return Redirect::route('painel.noticias.corporativo.index', ['idioma' => $idioma]);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover notícia.']);

        }
    }

}
