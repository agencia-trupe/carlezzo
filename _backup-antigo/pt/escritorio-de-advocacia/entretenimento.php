<?php
	
	//-------------------- Conecta com o Banco de Dados ---------------------------
	include "../../_includes/conexao.php";	
	
	//Busca not�cias
	$busca_noticias = mysql_query("SELECT * FROM noticias WHERE status = '1' AND titulo_pt != '0'  AND destaque != '1' ORDER BY data DESC LIMIT 0, 2");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- META TAGS - AG�NCIA 10CLIC - OTIMIZA��O -->
<meta name="author" content="10Clic - www.10clic.com.br" />
<meta name="description" content="Escrit�rio de advocacia especializado em esporte, entretenimento e m�dia, com ampla atua��o internacional em temas ligados ao futebol, FIFA e atletas.
" />
<meta name="keywords" content="FIFA,UEFA,CAS,court of arbitration for sport,futebol, agentes de jogadores, atletas, mecanismo de solidariedade, compensa��o por forma��o, cinema,m�sica,celebridades, publicidade, marketing,televis�o, propaganda, shows, r�dio,internet, jogos eletr�nicos, gaming,m�dia,novas tecnologias,m�dias sociais,software,fundos de investimento,direito desport" />
<meta name="robots" content="index, follow, archive" />
<meta name="googlebot" content="archive" />
<!-- FIM DAS META TAGS -->
<title>Carlezzo Advogados - Entretenimento</title>
<!-- FOLHAS DE ESTILO -->
<link href="../../_css/estilo.css" rel="stylesheet" type="text/css" />
<link href="../../_css/nivo-slider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../_ajax/skin.css" rel="stylesheet" type="text/css" />
<!-- FIM DAS FOLHAS DE ESTILO -->
<!-- SCRIPTS -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'49de9f94-6555-4ec3-ba64-437c8a161ebe'});</script>
<script type="text/javascript" src="../../_ajax/jquery.js"></script> 
<script type="text/javascript" src="../../_ajax/menu.jquery.js"></script> 
<!-- FIM DOS SCRIPTS -->
</head>

<body class="interna">
<!-- DEFESA -->
<div id="defesa">
  <!-- TOPO -->
  <div id="topo">
  	
    <!-- LINHA -->  
    <div id="linha">
    
    <form action="../busca.php" method="post" id="busca">
    <input class="campo" name="palavra" type="text" />
    <input name="" type="image" value="ok" src="../../_imagens/botao_buscar.jpg" alt="Buscar" class="botao" />
    </form>
    <span displayText="" class="st_sharethis_custom"></span>
    <p class="linguas_pt">
    <a href="../en/<?=basename($_SERVER['PHP_SELF']);?>" title="English">English</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" title="Espa&ntilde;ol">Espa&ntilde;ol</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:window.print()" title="Imprima esta p&aacute;gina"><img src="../../_imagens/a_print.jpg" alt="Imprima esta p&aacute;gina" align="absmiddle" /></a>&nbsp;&nbsp;<a href="mailto:carlezzo@carlezzo.com.br" title="Contato r&aacute;pido"><img src="../../_imagens/a_mail.jpg" alt="Contato r&aacute;pido" align="absmiddle" /></a>
    </p>    
    </div>
    <!-- FIM DA LINHA -->
    
    <!-- LOGO -->
    <h1><a href="../index.php" title="Carlezzo Advogados">Carlezzo Advogados</a></h1>
    <!-- FIM DO LOGO -->
    
    <!-- MENU TOPO -->
    <div id="menu_topo">
    	<ul class="topnav">
            <li><a href="../index.php">Home</a></li>
            <li>
                <a href="../quem-somos.php">Quem somos</a>

          <ul class="subnav">
                    <li><a href="../quem-somos.php">Vis&atilde;o Geral</a></li>
                    <li><a href="../missao-e-valores.php">Miss&atilde;o e Valores</a></li>
                    <li><a href="../o-que-esperar-de-nos.php">O qu&ecirc; esperar de n&oacute;s</a></li>
                    <li><a href="../aliancas-internacionais.php">Alian&ccedil;as Internacionais</a></li>
                    <li><a href="../recrutamento.php">Recrutamento</a></li>
                    <li><a href="../pro-bono.php">Pr&oacute;-bono</a></li>
              </ul>
          </li>
            <li>
                <a href="../setores.php">Setores</a>
          <ul class="subnav">
                    <li><a href="../setores.php?categoria=esporte">Esporte</a></li>
     				<li><a href="../setores.php?categoria=entretenimento">Entretenimento</a></li>
      				<li><a href="../setores.php?categoria=midia">M&iacute;dia e Novas Tecnologias</a></li>
              </ul>
          </li>
          <li><a href="../equipe.php">Equipe</a></li>

          <li><a href="../noticias.php">Not&iacute;cias</a></li>
          <li><a href="../publicacoes.php">Publica&ccedil;&otilde;es</a></li>
          <li class="last"><a href="../onde-estamos.php">Onde Estamos</a></li>
      </ul>
    </div>
    <!-- FIM DO MENU TOPO -->
  
  </div>
  <!-- FIM DO TOPO -->
  
  <!-- CONTE�DO -->
  <div id="conteudo">
    
    <!-- ESQUERDA -->
    <div class="esquerda_interna">
    
    <div class="bloco_sidebar">
      <h2 class="titulo">CARLEZZO ADVOGADOS</h2>
      <ul class="menu_esquerda">
       <li><a href="fifa-federation-internationale-de-football-association.php">FIFA</a></li>
	<li><a href="entretenimento.php">Entretenimento</a></li>
	<li><a href="futebol.php">Futebol</a></li>
	<li><a href="jogos-olimpicos.php">Jogos Ol&iacute;mpicos</a></li>
	<li><a href="novas-tecnologias.php">Novas tecnologias</a></li>
        </ul>
    </div>
    
    
    <!-- CHAMADA -->
    <div class="chamada"><img src="../../_imagens/chamada_quem_somos.jpg" width="242" height="318" /></div>
    <!-- FIM DA CHAMADA -->
        
    <br class="limpa_float" />
    </div>
    <!-- FIM DA ESQUERDA --> 
    
    <!-- DIREITA -->
    <div class="direita_interna">
    
    <!-- TEXTO -->
    <div class="texto">
    <h5 class="square">Entretenimento</h5><p>Carlezzo Advogados &eacute; um escrit&oacute;rio especializado no mercado   de entretenimento, com destaque nos segmentos de m&uacute;sica, televis&atilde;o, publicidade,   cinema e jogo eletr&ocirc;nico.</p>
    <p> </p>
    <p>Nossos profissionais possuem s&oacute;lida forma&ccedil;&atilde;o acad&ecirc;mica e   consistente pr&aacute;tica profissional no mercado do entretenimento, com vasta   experi&ecirc;ncia no assessoramento de celebridades, produtoras, empres&aacute;rios   art&iacute;sticos, clubes de futebol e empresas com atua&ccedil;&atilde;o neste mercado, al&eacute;m de   empresas nacionais e multinacionais interessadas em realizar investimentos no   setor ou patrocinar projetos inseridos neste contexto.</p>
    <p> </p>
    <p>Nosso escrit&oacute;rio oferece a seus clientes uma combina&ccedil;&atilde;o de   conhecimentos jur&iacute;dicos, relacionamento com importantes agentes do mercado do   entretenimento e profunda compreens&atilde;o dos neg&oacute;cios dos clientes.</p>
    <p> </p>
    <p>Carlezzo Advogados presta assessoria jur&iacute;dica completa &agrave;   ind&uacute;stria do entretenimento, seja na intermedia&ccedil;&atilde;o de neg&oacute;cios, na elabora&ccedil;&atilde;o e   revis&atilde;o de contratos de entretenimento, na assist&ecirc;ncia &agrave; prote&ccedil;&atilde;o de ativos   imateriais, no clearance de direitos e, ainda, em todos os aspectos legais   relacionados aos neg&oacute;cios de entretenimento no Brasil e no exterior, incluindo a   participa&ccedil;&atilde;o em processos arbitrais e judiciais.</p>
<p></p>
<p></p>
    </div>
    <!-- FIM DO TEXTO -->
    
    </div>
    <!-- FIM DA DIREITA -->  
  
  <br class="limpa_float" />  
  </div>
  <!-- FIM DO CONTE�DO -->
  </div>
  <!-- FIM DA DEFESA -->
  <!-- BASE -->
  <div id="base">
    
    <!-- DEFESA DA BASE -->
    <div id="defesa_base">
      
    <div class="left">
    <p class="endereco">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
      CEP 01.424-001<br />
      Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
      S&atilde;o Paulo/SP - Brasil<br />
      email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
    <p><a href="../index.php">Home</a> |   <a href="../avisos-legais.php">Avisos legais</a> |   <a href="../mapa-do-site.php">Mapa do site</a> |   <a href="../subscreva.php">Subscreva </a></p>
    </div>
    
    <div class="right">
    <ul id="menu_base">
        <li class="acompanhe">Siga-nos:</li>
        <li class="social">
        <a class="facebook" href="https://www.facebook.com/carlezzo.advogados.associados" title="Facebook" target="_blank">Facebook</a> 
        <a class="linkedin" href="http://www.linkedin.com/company/1294492" title="Linkedin" target="_blank">Linkedin</a> 
        <a class="youtube" href="http://www.youtube.com/user/carlezzo" title="Youtube" target="_blank">Youtube</a>
        </li>
      </ul>
      <br class="limpa_float" /> 
    <a class="assinatura" href="http://www.agencia10clic.com.br" target="_blank" title="Ag&ecirc;ncia 10Clic">Ag&ecirc;ncia 10Clic</a>
    <p>&copy;2011 Carlezzo Advogados. Todos os direitos reservados</p>
    </div>
    
    </div>
    <!-- FIM DA DEFESA DA BASE -->
    
  </div>
  <!-- FIM DA BASE -->
<!-- SCRIPT GOOGLE ANALYTICS - AG�NCIA 10CLIC - OTIMIZA��O -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10514770-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script><br />
<!-- FIM DO SCRIPT GOOGLE ANALYTICS -->
</body>
</html>