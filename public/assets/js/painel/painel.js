$('document').ready( function(){

    if ($('#datepicker').length) {
        $.datepicker.regional['pt-BR'] = {
                closeText: 'Fechar',
                prevText: '&#x3c;Anterior',
                nextText: 'Pr&oacute;ximo&#x3e;',
                currentText: 'Hoje',
                monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
                'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
                'Jul','Ago','Set','Out','Nov','Dez'],
                dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
                dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };

        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

        $("#datepicker").datepicker();

        if ($('#datepicker').val() == '') {
            $('#datepicker').datepicker("setDate", new Date());
        }
    }

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	var result = confirm("Deseja Excluir o Registro?");
      	if(result) form.submit();
  	});

    $("table.table-sortable tbody").sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post(BASE + '/painel/ajax/order', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

    var TEXTAREA_CONFIG = {
        padrao: {
            format_tags : 'p;h3',
            toolbar: [['Bold', 'Italic'], ['BulletedList'], ['Format']]
        },

        sub: {
            toolbar: [['Bold', 'Italic'], ['BulletedList']]
        },

        noticia: {
            extraPlugins: 'injectimage',
            allowedContent: true,
            toolbar: [['Bold', 'Italic'], ['BulletedList'], ['Link', 'Unlink'], ['InjectImage']]
        },

        clean: {
            toolbar: []
        }
    };

    $('.ckeditor').each(function (i, obj) {
        CKEDITOR.replace(obj.id, TEXTAREA_CONFIG[obj.dataset.editor]);
    });

});
