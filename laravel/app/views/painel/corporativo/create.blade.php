@section('content')

    <legend>
        <h2><small>Corporativo /</small> Adicionar Notícia</h2>
    </legend>

    {{ Form::open(['route' => 'painel.noticias.corporativo.store']) }}

        @include('painel.corporativo._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
