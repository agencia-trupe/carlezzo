@section('content')

    <div class="equipe-interna">
        <div class="equipe-dados">
            <img src="{{ asset('assets/img/equipe/'.$equipe->imagem) }}" alt="">
            @if($equipe->email)
            <a href="mailto:{{ $equipe->email }}">{{ $equipe->email }}</a>
            @endif
            @if($equipe->telefone)
            <span>{{ $equipe->telefone }}</span>
            @endif

            @if(count($corporativo))
            <div class="box-corporativo">
                <div class="title">
                    <span>{{ Lang::get('frontend.noticias.corporativo') }}</span>
                </div>
                @foreach($corporativo as $noticia)
                    <a href="{{ route('corporativo', $noticia->slug) }}" class="noticia">
                        <span class="data">{{ $noticia->data }}</span>
                        {{ $noticia->titulo }}
                    </a>
                @endforeach
                <div class="title2">
                    <a href="{{ route('corporativo.equipe', $equipe->id) }}">{{ Lang::get('frontend.home.vermais') }}</a>
                </div>
            </div>
            @endif
        </div>

        <div class="equipe-conteudo">
            <h2>{{ $equipe->nome }}</h2>
            <span class="cargo">{{ $equipe->{Lang::get('frontend.campos.cargo')} }}</span>
            <div class="equipe-texto">
                {{ $equipe->{Lang::get('frontend.campos.texto')} }}
            </div>
            <a href="{{ route('equipe') }}" class="voltar">« {{ Lang::get('frontend.footer.voltar') }}</a>
        </div>
    </div>

@stop
