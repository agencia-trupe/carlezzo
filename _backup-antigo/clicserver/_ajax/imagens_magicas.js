// JavaScript Document

initCalled=false;

if (! initCalled) {init()}

function init ()
{
	zindexStart = 1000;  
	initCalled = true;
}

function setzindex(element, effect)
{
	if (effect.direction == Spry.forwards)
	{
	element.style.zIndex=zindexStart;
	
		//seems that IE needs a seperate solution
		if(/MSIE/.test(navigator.userAgent) && /Windows NT/.test(navigator.userAgent))
		{
				Spry.Effect.setStyleProp(	element.parentNode, 'zIndex', zindexStart);
				//element.parentNode.style.zIndex = zindexStart;
		}
	}
	zindexStart++;
}

function resetzindex(element, effect)
{
	if (effect.direction == Spry.backwards)
	{
 		 element.style.zIndex=1;
		
		if(/MSIE/.test(navigator.userAgent) && /Windows NT/.test(navigator.userAgent))
		 {
			Spry.Effect.setStyleProp(	element.parentNode, 'zIndex', 1);
		 }
	}
}
var effects = [];
function toggleThumb(targetElement)
{
	if (typeof effects[targetElement.id] == 'undefined')
	{
		effects[targetElement.id] = new Spry.Effect.Grow(targetElement, {duration: 400, from: '100%', to: '500%', toggle: true, setup:setzindex, finish:resetzindex});
	}
	
	effects[targetElement.id].start();
}
