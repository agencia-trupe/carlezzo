@section('content')

    <div class="menu-sub">
        <a href="{{ route('noticias') }}">{{ Lang::get('frontend.noticias.noticias') }}</a>
        <a href="{{ route('corporativo') }}" class="active">{{ Lang::get('frontend.noticias.corporativo') }}</a>
    </div>

    <div class="conteudo-sub noticias-home">
        <div class="titulo">
            <h2>{{ Lang::get('frontend.noticias.corporativo') }}</h2>
        </div>

        <div class="conteudo conteudo-noticias">
            <div class="noticia">
                <p class="info">
                    {{ $noticia->data }}
                    @if($noticia->equipe)
                    {{ Lang::get('frontend.noticias.por') }} <a href="{{ route('equipe', $noticia->equipe['id']) }}">{{ $noticia->equipe['nome'] }}</a>
                    @endif
                </p>
                <h2>{{ $noticia->titulo }}</h2>
                <div class="noticia-texto">
                    {{ $noticia->texto }}
                </div>
                <a href="{{ route('corporativo') }}" class="voltar">« {{ Lang::get('frontend.footer.voltar') }}</a>
            </div>
        </div>
    </div>

@stop
