<?php

use \Corporativo, \Equipe;

class CorporativoController extends BaseController {

    public function index($slug = null)
    {
        $idioma = Config::get('app.locale');

        if (!$slug) {

            $noticias = Corporativo::with('equipe')->idioma($idioma)->ordenados()->paginate(5);

            return $this->view('frontend.corporativo.index', compact('noticias'));

        } else {

            $noticia = Corporativo::slug($slug)->first();
            if (!$noticia) App::abort('404');

            return $this->view('frontend.corporativo.noticia', compact('noticia'));

        }
    }

    public function equipe($id)
    {
        $idioma = Config::get('app.locale');
        $equipe = Equipe::find($id);
        if(!$equipe) App::abort('404');

        $noticias = Corporativo::with('equipe')->idioma($idioma)->where('equipe_id', $id)->ordenados()->paginate(5);
        $membro   = $equipe->nome;

        return $this->view('frontend.corporativo.index', compact('noticias', 'membro'));
    }

}
