@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('titulo_pt', 'Título [PORTUGUÊS]') }}
    {{ Form::text('titulo_pt', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('texto_pt', 'Texto [PORTUGUÊS]') }}
    {{ Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'sub']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('titulo_en', 'Título [INGLÊS]') }}
    {{ Form::text('titulo_en', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('texto_en', 'Texto [INGLÊS]') }}
    {{ Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'sub']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('titulo_es', 'Título [ESPANHOL]') }}
    {{ Form::text('titulo_es', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('texto_es', 'Texto [ESPANHOL]') }}
    {{ Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'sub']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.atuacao.pratica.index') }}" class="btn btn-default btn-voltar">Voltar</a>
