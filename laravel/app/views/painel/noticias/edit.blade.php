@section('content')

    <legend>
        <h2><small>Notícias /</small> Editar Notícia</h2>
    </legend>

    {{ Form::model($noticia, [
        'route' => ['painel.noticias.update', $noticia->id],
        'method' => 'patch'])
    }}

        @include('painel.noticias._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
