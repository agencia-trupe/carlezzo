<?php

namespace Painel;

use \Equipe, \Input, \Validator, \Redirect, \Session, \CropImage;

class EquipeController extends BasePainelController {

    private $validation_rules = [
        'nome'     => 'required',
        'email'    => 'email',
        'cargo_pt' => 'required',
        'cargo_en' => 'required',
        'cargo_es' => 'required',
        'texto_pt' => 'required',
        'texto_en' => 'required',
        'texto_es' => 'required',
    ];

    private $image_config = [
        'width'  => 294,
        'height' => 360,
        'path'   => 'assets/img/equipe/'
    ];

    public function index()
    {
        $equipe = Equipe::ordenados()->get();

        return $this->view('painel.equipe.index', compact('equipe'));
    }

    public function create()
    {
        return $this->view('painel.equipe.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            Equipe::create($input);
            Session::flash('sucesso', 'Registro adicionado com sucesso.');

            return Redirect::route('painel.equipe.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar registro.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $equipe = Equipe::findOrFail($id);

        return $this->view('painel.equipe.edit', compact('equipe'));
    }

    public function update($id)
    {
        $equipe = Equipe::findOrFail($id);
        $input  = Input::all();

        $this->validation_rules['imagem'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            } else {
                unset($input['imagem']);
            }

            $equipe->update($input);
            Session::flash('sucesso', 'Registro alterado com sucesso.');

            return Redirect::route('painel.equipe.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar registro.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Equipe::destroy($id);
            Session::flash('sucesso', 'Registro removido com sucesso.');

            return Redirect::route('painel.equipe.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover registro.']);

        }
    }

}
