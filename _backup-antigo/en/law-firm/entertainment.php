<?php
	
	//-------------------- Conecta com o Banco de Dados ---------------------------
	include "../../_includes/conexao.php";	
	
	//Busca not�cias
	$busca_noticias = mysql_query("SELECT * FROM noticias WHERE status = '1' AND titulo_en != '0'  AND destaque != '1' ORDER BY data DESC LIMIT 0, 2");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- META TAGS - AG�NCIA 10CLIC - OTIMIZA��O -->
<meta name="author" content="10Clic - www.10clic.com.br" />
<meta name="description" content="Law firm specialized in sports, entertainment and media, with strong presence in football and international affairs, as well as before FIFA and CBF
" />
<meta name="keywords" content="FIFA, UEFA, Court of arbitration for sport, football, player's agents, athletes, clubs, solidarity mechanism, training compensation, transfers, cinema, sports law, music, concerts, models, celebrities, video-game, media, new technologies, intellectual property, advertising, brands, marketing, Internet, TV, radio, mobile, world cup, Olympic games
" />
<meta name="robots" content="index, follow, archive" />
<meta name="googlebot" content="archive" />
<!-- FIM DAS META TAGS -->
<title>Carlezzo Advogados - Entertainment</title>
<!-- FOLHAS DE ESTILO -->
<link href="../../_css/estilo.css" rel="stylesheet" type="text/css" />
<link href="../../_css/nivo-slider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../_ajax/skin.css" rel="stylesheet" type="text/css" />
<!-- FIM DAS FOLHAS DE ESTILO -->
<!-- SCRIPTS -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'49de9f94-6555-4ec3-ba64-437c8a161ebe'});</script>
<script type="text/javascript" src="../../_ajax/jquery.js"></script> 
<script type="text/javascript" src="../../_ajax/menu.jquery.js"></script> 
<!-- FIM DOS SCRIPTS -->
</head>

<body class="interna">
<!-- DEFESA -->
<div id="defesa">
  <!-- TOPO -->
  <div id="topo">
  	
    <!-- LINHA -->  
    <div id="linha">
    
    <form action="../busca.php" method="post" id="busca">
    <input class="campo" name="palavra" type="text" />
    <input name="" type="image" value="ok" src="../../_imagens/botao_buscar.jpg" alt="Search" class="botao" />
    </form>
    <span displayText="" class="st_sharethis_custom"></span>
    <p class="linguas">
    <a href="../pt/<?=basename($_SERVER['PHP_SELF']);?>" title="Portugu&ecirc;s">Portugu&ecirc;s</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" title="Espa&ntilde;ol">Espa&ntilde;ol</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:window.print()" title="Print this page"><img src="../../_imagens/a_print.jpg" alt="Print this page" align="absmiddle" /></a>&nbsp;&nbsp;<a href="mailto:carlezzo@carlezzo.com.br" title="Quick contact"><img src="../../_imagens/a_mail.jpg" alt="Quick contact" align="absmiddle" /></a>
    </p>    
    </div>
    <!-- FIM DA LINHA -->
    
    <!-- LOGO -->
    <h1><a href="../index.php" title="Carlezzo Advogados">Carlezzo Advogados</a></h1>
    <!-- FIM DO LOGO -->
    
    <!-- MENU TOPO -->
    <div id="menu_topo">
    	<ul class="topnav">
            <li><a href="../index.php">Home</a></li>
            <li>
                <a href="../quem-somos.php">About us</a>

          <ul class="subnav">
                    <li><a href="../quem-somos.php">Overview</a></li>
                    <li><a href="../missao-e-valores.php">Mission and Values</a></li>
                    <li><a href="../o-que-esperar-de-nos.php">What you can wait from us</a></li>
                    <li><a href="../aliancas-internacionais.php">International Alliances</a></li>
                    <li><a href="../recrutamento.php">Recruitment</a></li>
                    <li><a href="../pro-bono.php">Pro-bono</a></li>
              </ul>
          </li>
            <li>
                <a href="../setores.php">Sectors</a>
          <ul class="subnav">
                    <li><a href="../setores.php?categoria=esporte">Sport</a></li>
     				<li><a href="../setores.php?categoria=entretenimento">Entertainment</a></li>
      				<li><a href="../setores.php?categoria=midia">Media and New Technologies</a></li>
              </ul>
          </li>
          <li><a href="../equipe.php">Team</a></li>

          <li><a href="../noticias.php">News</a></li>
          <li><a href="../publicacoes.php">Publications</a></li>
          <li class="last"><a href="../onde-estamos.php">Where are we</a></li>
      </ul>
    </div>
    <!-- FIM DO MENU TOPO -->
  
  </div>
  <!-- FIM DO TOPO -->
  
  <!-- CONTE�DO -->
  <div id="conteudo">
    
    <!-- ESQUERDA -->
    <div class="esquerda_interna">
    
    <div class="bloco_sidebar">
      <h2 class="titulo">CARLEZZO ADVOGADOS</h2>
      <ul class="menu_esquerda">
    <li><a href="football.php">Football</a></li>
	<li><a href="fifa.php">FIFA</a></li>
	<li><a href="world-cup-fifa-2014.php">World Cup FIFA 2014</a></li>
	<li><a href="olympic-games-rio-2016.php">Olympic Games Rio 2016</a></li>
	<li><a href="entertainment.php">Entertainment</a></li>
        </ul>
    </div>
    
    
    <!-- CHAMADA -->
    <div class="chamada"><img src="../../_imagens/chamada_quem_somos.jpg" width="242" height="318" /></div>
    <!-- FIM DA CHAMADA -->
        
    <br class="limpa_float" />
    </div>
    <!-- FIM DA ESQUERDA --> 
    
    <!-- DIREITA -->
    <div class="direita_interna">
    
    <!-- TEXTO -->
    <div class="texto">
    <h5 class="square">Entertainment</h5>
    <p>The entertainment industry (also informally known as show business or show biz) consists of a large number of sub-industries devoted to entertainment. However, the term is often used in the mass media to describe the mass media companies that control the distribution and manufacture of mass media entertainment. In the popular parlance, the term show biz in particular connotes the commercially popular performing arts, especially musical theatre, vaudeville, comedy, film, and music. A concert is a live performance (typically of music) before an audience. The performance may be by a single musician, sometimes then called a recital, or by a musical ensemble, such as an orchestra, a choir, or a musical band. Concerts are held in a wide variety and size of settings, from private houses and small nightclubs, dedicated concert halls, entertainment centres and parks to large multipurpose buildings, and even sports stadia. Indoor concerts held in the largest venues are sometimes calle d arena concerts. Regardless of the venue, musicians usually perform on a stage. Before recorded music, concerts would provide the only opportunity one would generally have to hear musicians play. Informal names for a concert include \&quot;show\&quot; and \&quot;gig\&quot;. A nightclub (also known simply as a club, discoth&egrave;que or disco) is an entertainment venue which usually operates late into the night. A nightclub is generally distinguished from bars, pubs or taverns by the inclusion of a dance floor and a DJ booth, where a DJ plays recorded dance, hip hop, rock, reggae and pop music. The music in nightclubs is either live bands or, more commonly, a mix of songs played by a DJ through a powerful PA system. Most clubs or club nights cater to certain music genres, such as techno, house music, trance, heavy metal, garage, hip hop, salsa, dancehall, Drum and Bass, Dubstep or soca music. Many clubs also promote playing the Top 40 which has most of the night playing the most broadcast songs of the previous week. A nightclub (also known simply as a club, discoth&egrave;que or disco) is an entertainment venue which usually operates late into the night. A nightclub is generally distinguished from bars, pubs or taverns by the inclusion of a dance floor and a DJ booth, where a DJ plays recorded dance, hip hop, rock, reggae and pop music. The music in nightclubs is either live bands or, more commonly, a mix of songs played by a DJ through a powerful PA system. Most clubs or club nights cater to certain music genres, such as techno, house music, trance, heavy metal, garage, hip hop, salsa, dancehall, Drum and Bass, Dubstep or soca music. Many clubs also promote playing the Top 40 which has most of the night playing the most broadcast songs of the previous week. The making and showing of motion pictures became a source of profit almost as soon as the process was invented. Upon seeing how successful their new invention, and its product, was in their native France, the Lumi&egrave;res quickly set about touring the Continent to exhibit the first films privately to royalty and publicly to the masses. In each country, they would normally add new, local scenes to their catalogue and, quickly enough, found local entrepreneurs in the various countries of Europe to buy their equipment and photograph, export, import and screen a dditional product commercially. The Oberammergau Passion Play of 1898[citation needed] was the first commercial motion picture ever produced. Other pictures soon followed, and motion pictures became a separate industry that overshadowed the vaudeville world. Dedicated theaters and companies formed specifically to produce and distribute films, while motion picture actors became major celebrities and commanded huge fees for their performances. By 1917 Charlie Chaplin had a contract that called for an annual salary of one million dollars. Animation is the technique in which each frame of a film is produced individually, whether generated as a computer graphic, or by photographing a drawn image, or by repeatedly making small changes to a model unit (see claymation and stop motion), and then photographing the result with a special animation camera. When the frames are strung together and the resulting film is viewed at a speed of 16 or more frames per second, there is an illusion of continuous movement (due to the persistence of vision). Generating such a film is very labor intensive and tedious, though the development of computer animation has greatly sped up the process. Animation is the technique in which each frame of a film is produced individually, whether generated as a computer graphic, or by photographing a drawn image, or by repeatedly making small changes to a model unit (see claymation and stop motion), and then photographing the result with a special animation camera. When the frames are strung together and the resulting film is viewed at a speed of 16 or more frames per second, there is an illusion of continuous movement (due to the persistence of vision). Generating such a film is very labor intensive and tedious, though the development of computer animation has greatly sped up the process. Broadcasting is the distribution of audio and video content to a dispersed audience via radio, television, or other. Receiving parties may include the general public or a relatively large subset of thereof. Broadcasting antenna in Stuttgart The original term broadcast referred to the literal sowing of seeds on farms by scattering them over a wide field.[1] It was first adopted by early radio engineers from the Midwestern United States to refer to the analogous dissemination of radio signals. Broadcasting forms a very large segment of the mass media. Broadcasting to a very narrow range of audience is called narrowcasting. The initial stage (from approximately 1998 to 2001) of the digital music revolution was the emergence of peer-to-peer (P2P) networks that allowed the illegal exchange of music files (such as Kazaa and Napster). By 2001, the cost of hard drive space had dropped to a level that allowed pocket-sized computers to store large libraries of music. The iPod and iTunes system for music storage and playback became immensely popular, and many consumers began to transfer their physical recording media (such as CDs) onto computer hard drives. The iTunes music store offered legal downloads beginning in 2003, and competitors soon followed, offering a variety of online music services, such as internet radio. Digital music distribution was aided by the widespread acceptance of broadband in the middle of the decade.[1] At the same time, recording software (such as Avid\'s ProTools) began to be used almost exclusively to make records, rendering expensive multitrack tape machines (such as the 1967 Studer) almost obsolete. Rise of MP3 players, which are consumer electronics devices that stores, organizes and plays audio files. Some DAPs (digital audio players) are also referred to as portable media players as they have image-viewing and/or video-playing support. The first mass-produced DAP was created in 1997 by SaeHan Information Systems, which domestically sold its &ldquo;MPMan&rdquo; player in the middle of 1998.[41] In October 2001, Apple Computer (now known as Apple Inc.) unveiled the first generation iPod, the 5 GB hard drive based DAP with a 1.8\&quot; Toshiba drive. With the development of a mi nimalistic user interface and a smaller form factor, the iPod was initially notable within users of the Macintosh community. In July 2002, Apple introduced the second generation update to the iPod. It was compatible with Windows computers through Musicmatch Jukebox (now known as Y!Music Musicmatch Jukebox). The iPod series, which grew to include microdrive and flash-based players, has become the market leader in DAPs. New media is a broad term in media studies that emerged in the later part of the 20th century. For example, new media holds out a possibility of on-demand access to content any time, anywhere, on any digital device, as well as interactive user feedback, creative participation and community formation around the media content. Another important promise of New Media is the \&quot;democratization\&quot; of the creation, publishing, distribution and consumption of media content. What distinguishes new media from traditional media is the digitizing of content into bits. There is also a dynamic aspect of content production which can be done in real time. Thus, a high-definition digital television broadcast of a film viewed on a digital plasma TV is still an example of traditional media, while an \&quot;analog\&quot; paper poster of a local rock band that contains a web address where fans can find information and digital music downloads is an example of New media communication.[citation needed] The video game industry (often referred to as interactive entertainment) is the economic sector involved with the development, marketing and sale of video games. It encompasses dozens of job disciplines and employs thousands of people worldwide. Radio is the transmission of signals by modulation of electromagnetic waves with frequencies below those of visible light.[1] Electromagnetic radiation travels by means of oscillating electromagnetic fields that pass through the air and the vacuum of space. Information is carried by systematically changing (modulating) some property of the radiated waves, such as amplitude, frequency, phase, or pulse widt h. When radio waves pass an electrical conductor, the oscillating fields induce an alternating current in the conductor. This can be detected and transformed into sound or other signals that carry information. Leisure, or free time, is time spent away from business, work, and domestic chores. It is also the periods of time before or after necessary activities such as eating, sleeping and, where it is compulsory, education. The distinction between leisure and unavoidable activities is loosely applied, i.e. people sometimes do work-oriented tasks for pleasure as well as for long-term utility.[1] A distinction may also be drawn between free time and leisure. For example, Situationist International maintains that free time is illusory and rarely free; economic and social forces appropriate free time from the individual and sell it back to them as the commodity known as \&quot;leisure\&quot;.[2] Leisure studies is the academic discipline concerned with the study and analysis of leisure.</p>
    <p></p>
<p></p>
<p></p>
<p></p>
    </div>
    <!-- FIM DO TEXTO -->
    
    </div>
    <!-- FIM DA DIREITA -->  
  
  <br class="limpa_float" />  
  </div>
  <!-- FIM DO CONTE�DO -->
  </div>
  <!-- FIM DA DEFESA -->
  <!-- BASE -->
  <div id="base">
    
    <!-- DEFESA DA BASE -->
    <div id="defesa_base">
      
    <div class="left">
    <p class="endereco">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
      CEP 01.424-001<br />
      Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
      S&atilde;o Paulo/SP - Brasil<br />
      email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
    <p><a href="../index.php">Home</a> |   <a href="../avisos-legais.php">Legal terms</a> |   <a href="../mapa-do-site.php">Sitemap</a> |   <a href="../subscreva.php">Subscribe </a></p>
    </div>
    
    <div class="right">
    <ul id="menu_base">
        <li class="acompanhe">Follow us:</li>
        <li class="social">
        <a class="facebook" href="https://www.facebook.com/carlezzo.advogados.associados" title="Facebook" target="_blank">Facebook</a> 
        <a class="linkedin" href="http://www.linkedin.com/company/1294492" title="Linkedin" target="_blank">Linkedin</a> 
        <a class="youtube" href="http://www.youtube.com/user/carlezzo" title="Youtube" target="_blank">Youtube</a>
        </li>
      </ul>
      <br class="limpa_float" /> 
    <a class="assinatura" href="http://www.agencia10clic.com.br" target="_blank" title="Ag&ecirc;ncia 10Clic">Ag&ecirc;ncia 10Clic</a>
    <p>&copy;2011 Carlezzo Advogados. All rights reserved</p>
    </div>
    
    </div>
    <!-- FIM DA DEFESA DA BASE -->
    
  </div>
  <!-- FIM DA BASE -->
<!-- SCRIPT GOOGLE ANALYTICS - AG�NCIA 10CLIC - OTIMIZA��O -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10514770-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script><br />
<!-- FIM DO SCRIPT GOOGLE ANALYTICS -->
</body>
</html>