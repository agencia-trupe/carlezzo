@section('content')

    <div class="menu-sub">
        <p class="busca">{{ Lang::get('frontend.busca.resultados') }} <span>'{{ $termo }}'</span></p>
    </div>

    <div class="conteudo-sub noticias-home">
        <div class="conteudo conteudo-busca">
        @if(count($noticias) || count($corporativo))
        @foreach($corporativo as $noticia)
            <div class="noticia">
                <a href="{{ route('corporativo', $noticia->slug) }}" class="noticia-link">
                    <h2>{{ $noticia->titulo }}</h2>
                    <p class="texto">{{ \Tools::cropText(strip_tags($noticia->texto), 250) }}</p>
                    <p class="leiamais">{{ Lang::get('frontend.noticias.leiamais') }} <span>»</span></p>
                </a>
            </div>
        @endforeach
        @foreach($noticias as $noticia)
            <div class="noticia">
                <a href="{{ route('noticias', $noticia->slug) }}" class="noticia-link">
                    <h2>{{ $noticia->titulo }}</h2>
                    <p class="texto">{{ \Tools::cropText(strip_tags($noticia->texto), 250) }}</p>
                    <p class="leiamais">{{ Lang::get('frontend.noticias.leiamais') }} <span>»</span></p>
                </a>
            </div>
        @endforeach
        @else
            <p>{{ Lang::get('frontend.busca.nenhum') }}</p>
        @endif
        </div>
    </div>

@stop
