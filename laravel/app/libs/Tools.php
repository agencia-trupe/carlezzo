<?php

class Tools
{

    public static function cropText($text = null, $length = null)
    {
        if (!$text || !$length) return false;

        $length = abs((int)$length);

        if(strlen($text) > $length) {
            $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
        }

        return $text;
    }

    public static function formataTelefone($telefone = '')
    {
        if($telefone != ''){
            if(strpos($telefone, ' ') !== FALSE){
                $arr = explode(' ', $telefone);
                if(sizeof($arr) == 2) return $arr[0]." <span>".$arr[1]."</span>";
                if(sizeof($arr) == 3) return $arr[0].' '.$arr[1]." <span>".$arr[2]."</span>";
                if(sizeof($arr) == 4) return $arr[0].' '.$arr[1]." <span>".$arr[2].' '.$arr[3]."</span>";
                return $telefone;
            }
        }else{
            return $telefone;
        }
    }

}
