<?php
	
	//-------------------- Conecta com o Banco de Dados ---------------------------
	include "../../_includes/conexao.php";	
	
	//Busca not�cias
	$busca_noticias = mysql_query("SELECT * FROM noticias WHERE status = '1' AND titulo_pt != '0'  AND destaque != '1' ORDER BY data DESC LIMIT 0, 2");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- META TAGS - AG�NCIA 10CLIC - OTIMIZA��O -->
<meta name="author" content="10Clic - www.10clic.com.br" />
<meta name="description" content="Escrit�rio de advocacia especializado em esporte, entretenimento e m�dia, com ampla atua��o internacional em temas ligados ao futebol, FIFA e atletas.
" />
<meta name="keywords" content="FIFA,UEFA,CAS,court of arbitration for sport,futebol, agentes de jogadores, atletas, mecanismo de solidariedade, compensa��o por forma��o, cinema,m�sica,celebridades, publicidade, marketing,televis�o, propaganda, shows, r�dio,internet, jogos eletr�nicos, gaming,m�dia,novas tecnologias,m�dias sociais,software,fundos de investimento,direito desport" />
<meta name="robots" content="index, follow, archive" />
<meta name="googlebot" content="archive" />
<!-- FIM DAS META TAGS -->
<title>Carlezzo Advogados - Fifa</title>
<!-- FOLHAS DE ESTILO -->
<link href="../../_css/estilo.css" rel="stylesheet" type="text/css" />
<link href="../../_css/nivo-slider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../_ajax/skin.css" rel="stylesheet" type="text/css" />
<!-- FIM DAS FOLHAS DE ESTILO -->
<!-- SCRIPTS -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'49de9f94-6555-4ec3-ba64-437c8a161ebe'});</script>
<script type="text/javascript" src="../../_ajax/jquery.js"></script> 
<script type="text/javascript" src="../../_ajax/menu.jquery.js"></script> 
<!-- FIM DOS SCRIPTS -->
</head>

<body class="interna">
<!-- DEFESA -->
<div id="defesa">
  <!-- TOPO -->
  <div id="topo">
  	
    <!-- LINHA -->  
    <div id="linha">
    
    <form action="../busca.php" method="post" id="busca">
    <input class="campo" name="palavra" type="text" />
    <input name="" type="image" value="ok" src="../../_imagens/botao_buscar.jpg" alt="Buscar" class="botao" />
    </form>
    <span displayText="" class="st_sharethis_custom"></span>
    <p class="linguas_pt">
    <a href="../en/<?=basename($_SERVER['PHP_SELF']);?>" title="English">English</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" title="Espa&ntilde;ol">Espa&ntilde;ol</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:window.print()" title="Imprima esta p&aacute;gina"><img src="../../_imagens/a_print.jpg" alt="Imprima esta p&aacute;gina" align="absmiddle" /></a>&nbsp;&nbsp;<a href="mailto:carlezzo@carlezzo.com.br" title="Contato r&aacute;pido"><img src="../../_imagens/a_mail.jpg" alt="Contato r&aacute;pido" align="absmiddle" /></a>
    </p>    
    </div>
    <!-- FIM DA LINHA -->
    
    <!-- LOGO -->
    <h1><a href="../index.php" title="Carlezzo Advogados">Carlezzo Advogados</a></h1>
    <!-- FIM DO LOGO -->
    
    <!-- MENU TOPO -->
    <div id="menu_topo">
    	<ul class="topnav">
            <li><a href="../index.php">Home</a></li>
            <li>
                <a href="../quem-somos.php">Quem somos</a>

          <ul class="subnav">
                    <li><a href="../quem-somos.php">Vis&atilde;o Geral</a></li>
                    <li><a href="../missao-e-valores.php">Miss&atilde;o e Valores</a></li>
                    <li><a href="../o-que-esperar-de-nos.php">O qu&ecirc; esperar de n&oacute;s</a></li>
                    <li><a href="../aliancas-internacionais.php">Alian&ccedil;as Internacionais</a></li>
                    <li><a href="../recrutamento.php">Recrutamento</a></li>
                    <li><a href="../pro-bono.php">Pr&oacute;-bono</a></li>
              </ul>
          </li>
            <li>
                <a href="../setores.php">Setores</a>
          <ul class="subnav">
                    <li><a href="../setores.php?categoria=esporte">Esporte</a></li>
     				<li><a href="../setores.php?categoria=entretenimento">Entretenimento</a></li>
      				<li><a href="../setores.php?categoria=midia">M&iacute;dia e Novas Tecnologias</a></li>
              </ul>
          </li>
          <li><a href="../equipe.php">Equipe</a></li>

          <li><a href="../noticias.php">Not&iacute;cias</a></li>
          <li><a href="../publicacoes.php">Publica&ccedil;&otilde;es</a></li>
          <li class="last"><a href="../onde-estamos.php">Onde Estamos</a></li>
      </ul>
    </div>
    <!-- FIM DO MENU TOPO -->
  
  </div>
  <!-- FIM DO TOPO -->
  
  <!-- CONTE�DO -->
  <div id="conteudo">
    
    <!-- ESQUERDA -->
    <div class="esquerda_interna">
    
    <div class="bloco_sidebar">
      <h2 class="titulo">CARLEZZO ADVOGADOS</h2>
      <ul class="menu_esquerda">
       <li><a href="fifa-federation-internationale-de-football-association.php">FIFA</a></li>
	<li><a href="entretenimento.php">Entretenimento</a></li>
	<li><a href="futebol.php">Futebol</a></li>
	<li><a href="jogos-olimpicos.php">Jogos Ol&iacute;mpicos</a></li>
	<li><a href="novas-tecnologias.php">Novas tecnologias</a></li>
        </ul>
    </div>
    
    
    <!-- CHAMADA -->
    <div class="chamada"><img src="../../_imagens/chamada_quem_somos.jpg" width="242" height="318" /></div>
    <!-- FIM DA CHAMADA -->
        
    <br class="limpa_float" />
    </div>
    <!-- FIM DA ESQUERDA --> 
    
    <!-- DIREITA -->
    <div class="direita_interna">
    
    <!-- TEXTO -->
    <div class="texto">
    <h5 class="square">FIFA</h5><p>F&eacute;d&eacute;ration Internationale de Football Association, mais   conhecida como FIFA, &eacute; o &oacute;rg&atilde;o gestor do futebol mundial, respons&aacute;vel pela sua   regulamenta&ccedil;&atilde;o e organiza&ccedil;&atilde;o. A entidade possui normas pr&oacute;prias que regem a   pr&aacute;tica do futebol mundo afora, existindo um grande n&uacute;mero de regulamentos que   se aplicam ao dia a dia da modalidade. Neste sentido, desde 2004, Carlezzo   Advogados tem se notabilizado no cen&aacute;rio internacional como um dos escrit&oacute;rios   com maior atividade perante a entidade, tendo conduzido centenas de processos de   diversas naturezas.</p>
    <p> </p>
    <p>A Comiss&atilde;o do Estatuto do Jogador, C&acirc;mara de Resolu&ccedil;&atilde;o de   Disputas e Comit&ecirc; Disciplinar s&atilde;o apenas alguns dos &oacute;rg&atilde;os pelos quais Carlezzo   Advogados conduziu processos de seus clientes em assuntos que v&atilde;o desde a   cobran&ccedil;a do mecanismo de solidariedade e da compensa&ccedil;&atilde;o por forma&ccedil;&atilde;o, passando   pela representa&ccedil;&atilde;o de atletas e libera&ccedil;&atilde;o de v&iacute;nculo desportivo, at&eacute; casos   envolvendo a aplica&ccedil;&atilde;o de san&ccedil;&otilde;es disciplinares a agentes, clubes e   jogadores.</p>
    <p> </p>
    <p>Atualmente, onde o cen&aacute;rio do futebol &eacute; completamente   globalizado e as transfer&ecirc;ncias de atletas entre pa&iacute;ses s&atilde;o uma realidade   incessante, o conhecimento e a experi&ecirc;ncia na aplica&ccedil;&atilde;o do Regulamento sobre o   Estatuto e Transfer&ecirc;ncia de Jogadores da FIFA &eacute; de import&acirc;ncia prec&iacute;pua para   todos aqueles que estejam envolvidos em tais transa&ccedil;&otilde;es. Al&eacute;m desta norma,   tamb&eacute;m possui grande influ&ecirc;ncia pr&aacute;tica o Regulamento de Agente de Jogadores e o   C&oacute;digo Disciplinar.</p>
    <p> </p>
    <p>Especial men&ccedil;&atilde;o deve ser dada ao mecanismo de solidariedade e   a compensa&ccedil;&atilde;o por forma&ccedil;&atilde;o, mecanismo criados pela FIFA para indenizar o clube   formador quando da transfer&ecirc;ncia internacional de um atleta. Tais compensa&ccedil;&otilde;es   possuem a virtude de assegurar a um enorme n&uacute;mero de clubes montantes que os   mesmos nunca poderiam imaginar que iriam receber quando formaram determinado   atleta. A equipe de Carlezzo Advogados est&aacute; amplamente preparada para auxiliar   os clubes de futebol na an&aacute;lise das transfer&ecirc;ncias internacionais e posterior   cobran&ccedil;a da compensa&ccedil;&atilde;o por forma&ccedil;&atilde;o, tendo a experi&ecirc;ncia de j&aacute; ter instru&iacute;do   casos desta natureza junto a maioria dos grandes clubes de futebol do   mundo.</p>
<p></p>
    </div>
    <!-- FIM DO TEXTO -->
    
    </div>
    <!-- FIM DA DIREITA -->  
  
  <br class="limpa_float" />  
  </div>
  <!-- FIM DO CONTE�DO -->
  </div>
  <!-- FIM DA DEFESA -->
  <!-- BASE -->
  <div id="base">
    
    <!-- DEFESA DA BASE -->
    <div id="defesa_base">
      
    <div class="left">
    <p class="endereco">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
      CEP 01.424-001<br />
      Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
      S&atilde;o Paulo/SP - Brasil<br />
      email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
    <p><a href="../index.php">Home</a> |   <a href="../avisos-legais.php">Avisos legais</a> |   <a href="../mapa-do-site.php">Mapa do site</a> |   <a href="../subscreva.php">Subscreva </a></p>
    </div>
    
    <div class="right">
    <ul id="menu_base">
        <li class="acompanhe">Siga-nos:</li>
        <li class="social">
        <a class="facebook" href="https://www.facebook.com/carlezzo.advogados.associados" title="Facebook" target="_blank">Facebook</a> 
        <a class="linkedin" href="http://www.linkedin.com/company/1294492" title="Linkedin" target="_blank">Linkedin</a> 
        <a class="youtube" href="http://www.youtube.com/user/carlezzo" title="Youtube" target="_blank">Youtube</a>
        </li>
      </ul>
      <br class="limpa_float" /> 
    <a class="assinatura" href="http://www.agencia10clic.com.br" target="_blank" title="Ag&ecirc;ncia 10Clic">Ag&ecirc;ncia 10Clic</a>
    <p>&copy;2011 Carlezzo Advogados. Todos os direitos reservados</p>
    </div>
    
    </div>
    <!-- FIM DA DEFESA DA BASE -->
    
  </div>
  <!-- FIM DA BASE -->
<!-- SCRIPT GOOGLE ANALYTICS - AG�NCIA 10CLIC - OTIMIZA��O -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10514770-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script><br />
<!-- FIM DO SCRIPT GOOGLE ANALYTICS -->
</body>
</html>