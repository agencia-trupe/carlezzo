<?php

class Pratica extends Eloquent
{

    protected $table = 'pratica';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
