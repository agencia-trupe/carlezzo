    <footer>
        <div class="info">
            <nav>
                <div class="row">
                    <a href="{{ route('home') }}">Home</a>
                    <a href="{{ route('escritorio') }}">{{ Lang::get('frontend.menu.escritorio') }}</a>
                    <a href="{{ route('atuacao') }}">{{ Lang::get('frontend.menu.atuacao') }}</a>
                </div>
                <div class="row">
                    <a href="{{ route('equipe') }}">{{ Lang::get('frontend.menu.equipe') }}</a>
                    <a href="{{ route('noticias') }}">{{ Lang::get('frontend.menu.noticias') }}</a>
                    <a href="{{ route('contato') }}">{{ Lang::get('frontend.menu.contato') }}</a>
                </div>
                <div class="social">
                    @if($contato->facebook)<a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>@endif
                    @if($contato->linkedin)<a href="{{ $contato->linkedin }}" class="linkedin" target="_blank">linkedin</a>@endif
                    @if($contato->youtube)<a href="{{ $contato->youtube }}" class="youtube" target="_blank">youtube</a>@endif
                </div>
            </nav>
            <div class="contato">
                <div class="endereco">{{ $contato->endereco }}</div>
                <p>
                    @if($contato->telefone)Tel.: {{ $contato->telefone }}@endif
                    @if($contato->telefone && $contato->fax) /@endif
                    @if($contato->fax)Fax.: {{ $contato->fax }}@endif
                </p>
                <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
            </div>
        </div>
        <div class="copyright">
            <p class="left">© {{ date('Y') }} {{ Config::get('projeto.name') }} - {{ Lang::get('frontend.footer.copyright') }}.</p>
            <p class="right">
                <a href="http://www.trupe.net" target="blank">{{ Lang::get('frontend.footer.criacao') }}</a>:
                <a href="http://www.trupe.net" target="blank" class="trupe">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
