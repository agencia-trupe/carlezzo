@section('content')

    <div class="menu-sub">
        @foreach($subs as $sub)
            <a href="{{ route('escritorio', $sub->slug) }}" @if($sub->slug == $escritorio->slug) class='active' @endif>{{ $sub->{Lang::get('frontend.campos.titulo')} }}</a>
        @endforeach
    </div>

    <div class="conteudo-sub escritorio">
        <div class="titulo">
            <h2>{{ $escritorio->{Lang::get('frontend.campos.titulo')} }}</h2>
        </div>

        <div class="conteudo">
            {{ $escritorio->{Lang::get('frontend.campos.texto')} }}
        </div>
    </div>

@stop
