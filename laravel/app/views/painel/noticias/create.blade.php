@section('content')

    <legend>
        <h2><small>Notícias /</small> Adicionar Notícia</h2>
    </legend>

    {{ Form::open(['route' => 'painel.noticias.store']) }}

        @include('painel.noticias._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
