@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>
            <small>Notícias /</small> Notícias
            <a href="{{ route('painel.noticias.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Notícia</a>
        </h2>
    </legend>

    <div class="btn-group">
        <a href="{{URL::route('painel.noticias.index', array('idioma' => 'pt'))}}" class="btn btn-sm @if(isset($idioma) && $idioma == 'pt') btn-info @else btn-default @endif">Notícias em Português</a>
        <a href="{{URL::route('painel.noticias.index', array('idioma' => 'en'))}}" class="btn btn-sm @if(isset($idioma) && $idioma == 'en') btn-info @else btn-default @endif">Notícias em Inglês</a>
        <a href="{{URL::route('painel.noticias.index', array('idioma' => 'es'))}}" class="btn btn-sm @if(isset($idioma) && $idioma == 'es') btn-info @else btn-default @endif">Notícias em Espanhol</a>
    </div>

    <hr>

    @if(count($noticias))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="noticias">
        <thead>
            <tr>
                <th>Data</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($noticias as $noticia)

            <tr class="tr-row" id="id_{{ $noticia->id }}">
                <td>{{ $noticia->data }}</td>
                <td>{{ $noticia->titulo }}</td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.noticias.destroy', $noticia->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.noticias.edit', $noticia->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>

    {{ $noticias->links() }}
    @else
        <div class="alert alert-warning" role="alert">Nenhuma notícia cadastrada.</div>
    @endif

@stop
