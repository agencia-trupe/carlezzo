<?php

use \Banner, \Noticia, \Corporativo;

class HomeController extends BaseController {

	public function index()
	{
        $idioma = Config::get('app.locale');

        $banners     = Banner::all();
        $noticias    = Noticia::idioma($idioma)->ordenados()->limit(5)->get();
        $corporativo = Corporativo::idioma($idioma)->ordenados()->limit(5)->get();

		return $this->view('frontend.home', compact('banners', 'noticias', 'corporativo'));
	}

}
