    <header>
        <h1><a href="{{ route('home') }}">{{ Config::get('projeto.title') }}</a></h1>

        <div class="wrapper">
            <form action="{{ route('busca') }}" method="post" class="busca">
                <input type="text" name="termo" value required>
                <input type="submit" value>
            </form>

            <ul class="lang">
                @if(Config::get('app.locale') != 'pt')
                <li><a href="{{ route('idioma', 'pt') }}">português</a></li>
                @endif
                @if(Config::get('app.locale') != 'en')
                <li><a href="{{ route('idioma', 'en') }}">english</a></li>
                @endif
                @if(Config::get('app.locale') != 'es')
                <li><a href="{{ route('idioma', 'es') }}">español</a></li>
                @endif
            </ul>

            <nav>
                <a href="{{ route('escritorio') }}" @if(str_is('escritorio*', Route::currentRouteName())) class='active' @endif>{{ Lang::get('frontend.menu.escritorio') }}</a>
                <a href="{{ route('atuacao') }}" @if(str_is('atuacao*', Route::currentRouteName())) class='active' @endif>{{ Lang::get('frontend.menu.atuacao') }}</a>
                <a href="{{ route('equipe') }}" @if(str_is('equipe*', Route::currentRouteName())) class='active' @endif>{{ Lang::get('frontend.menu.equipe') }}</a>
                <a href="{{ route('noticias') }}" @if(str_is('noticias*', Route::currentRouteName()) || str_is('corporativo*', Route::currentRouteName())) class='active' @endif>{{ Lang::get('frontend.menu.noticias') }}</a>
                <a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class='active' @endif>{{ Lang::get('frontend.menu.contato') }}</a>
            </nav>
        </div>

        <nav id="mobile">
            <a href="{{ route('escritorio') }}">{{ Lang::get('frontend.menu.escritorio') }}</a>
            <a href="{{ route('atuacao') }}">{{ Lang::get('frontend.menu.atuacao') }}</a>
            <a href="{{ route('equipe') }}">{{ Lang::get('frontend.menu.equipe') }}</a>
            <a href="{{ route('noticias') }}">{{ Lang::get('frontend.menu.noticias') }}</a>
            <a href="{{ route('contato') }}">{{ Lang::get('frontend.menu.contato') }}</a>
            <ul class="lang">
                @if(Config::get('app.locale') != 'pt')
                <li><a href="{{ route('idioma', 'pt') }}">português</a></li>
                @endif
                @if(Config::get('app.locale') != 'en')
                <li><a href="{{ route('idioma', 'en') }}">english</a></li>
                @endif
                @if(Config::get('app.locale') != 'es')
                <li><a href="{{ route('idioma', 'es') }}">español</a></li>
                @endif
            </ul>
        </nav>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </header>
