<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCorporativoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('corporativo', function(Blueprint $table)
		{
			$table->engine = 'MYISAM';
			DB::statement('ALTER TABLE `corporativo` ADD FULLTEXT search(titulo, texto)');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('corporativo', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->dropIndex('search');
		});
	}

}
