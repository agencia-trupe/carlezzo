<?php

return array(

    'name'        => 'Carlezzo Advogados',
    'title'       => 'Carlezzo Advogados',
    'description' => 'Escritório de advocacia especializado em esporte, entretenimento e mídia, com ampla atuação internacional em temas ligados ao futebol, FIFA e atletas.',
    'keywords'    => 'Escritorio advocacia, advogados, FIFA, UEFA, CAS, court of arbitration for sport, futebol, agentes de jogadores, atletas, mecanismo de solidariedade, compensação por formação, cinema,música, celebridades, publicidade, marketing, televisão, propaganda, shows, rádio,internet, jogos eletrônicos, gaming, mídia, novas tecnologias, mídias sociais,software, fundos de investimento, direito desportivo',
    'share_image' => asset('assets/img/logo.png')

);
