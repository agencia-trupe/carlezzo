<?php

use \Atuacao, \Pratica, \Fazemos;

class AtuacaoController extends BaseController {

    public function index($slug = null)
    {
        if (!$slug) {
            $atuacao = Atuacao::first();
            return $this->view('frontend.atuacao', compact('atuacao'));
        } elseif ($slug == 'pratica-legal') {
            $items = Pratica::ordenados()->get();
            return $this->view('frontend.pratica', compact('items'));
        } elseif ($slug == 'o-que-fazemos-para') {
            $items = Fazemos::ordenados()->get();
            return $this->view('frontend.fazemos', compact('items'));
        } else {
            App::abort('404');
        }
    }

}
