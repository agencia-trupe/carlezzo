@section('content')

    <div class="googlemaps">
        {{ $contato->google_maps }}
    </div>

    <div class="conteudo-sub contato">
        <div class="info">
            @if($contato->telefone)
            <p class="tel">Tel.: {{ \Tools::formataTelefone($contato->telefone) }}</p>
            @endif
            @if($contato->fax)
            <p class="tel">Fax.: {{ \Tools::formataTelefone($contato->fax) }}</p>
            @endif
            <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
            <div class="endereco">{{ $contato->endereco }}</div>
        </div>

        <form action="" method="post" id="form-contato">
            <div class="left">
                <input type="text" name="nome" id="nome" placeholder="{{ Lang::get('frontend.contato.nome') }}" required>
                <input type="email" name="email" id="email" placeholder="{{ Lang::get('frontend.contato.email') }}" required>
                <input type="text" name="telefone" id="telefone" placeholder="{{ Lang::get('frontend.contato.telefone') }}">
            </div>
            <textarea name="mensagem" id="mensagem" placeholder="{{ Lang::get('frontend.contato.mensagem') }}" required></textarea>

            <input type="submit" value="{{ Lang::get('frontend.contato.enviar') }} »">

            <div id="form-resposta"></div>
        </form>
    </div>

@stop
