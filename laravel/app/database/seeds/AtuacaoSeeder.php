<?php

class AtuacaoSeeder extends Seeder {

    public function run()
    {
        DB::table('atuacao')->delete();

        $data = array(
            array(
                'texto_pt' => 'português',
                'texto_en' => 'inglês',
                'texto_es' => 'espanhol',
            )
        );

        DB::table('atuacao')->insert($data);
    }

}
