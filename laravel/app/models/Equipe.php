<?php

class Equipe extends Eloquent
{

    protected $table = 'equipe';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
