<?php

return array(

    'campos' => [
        'titulo' => 'titulo_es',
        'texto'  => 'texto_es',
        'cargo'  => 'cargo_es'
    ],

    'menu' => [
        'escritorio' => 'El Estudio',
        'atuacao'    => '¿Qué hacemos?',
        'equipe'     => 'Nuestro Equipo',
        'noticias'   => 'Noticias',
        'contato'    => 'Contacto'
    ],

    'footer' => [
        'copyright' => 'Todos los derechos reservados',
        'criacao'   => 'Sites',
        'voltar'    => 'retorno'
    ],

    'home' => [
        'mais'    => 'Más',
        'vermais' => 'ver más'
    ],

    'atuacao' => [
        'atuacao' => '¿Qué hacemos?',
        'pratica' => 'Práctica Jurídica',
        'fazemos' => 'Qué hacemos para'
    ],

    'noticias' => [
        'noticias'    => 'Noticias',
        'corporativo' => 'Corporativo',
        'leiamais'    => 'Leer más',
        'por'         => 'por',
        'nenhum'      => 'No se encontraron resultados.'
    ],

    'contato' => [
        'nome'     => 'Nombre',
        'email'    => 'E-mail',
        'telefone' => 'Teléfono',
        'mensagem' => 'Mensaje',
        'enviar'   => 'Enviar',
        'erro'     => 'Por favor, rellene todos los campos correctamente.',
        'sucesso'  => 'Mensaje enviado con éxito.'
    ],

    'busca' => [
        'resultados' => 'Resultados para',
        'nenhum'     => 'No se encontraron resultados.'
    ]

);
