<?php

class ContatoController extends BaseController {

    public function index()
    {
        return $this->view('frontend.contato');
    }

    public function envio()
    {
        $contato = Contato::first();

        $nome = Input::get('nome');
        $email = Input::get('email');
        $mensagem = Input::get('mensagem');
        $telefone = Input::get('telefone');
        $telefone = ($telefone ? $telefone : 'não informado');

        $validation = Validator::make(
            array(
                'nome'     => $nome,
                'email'    => $email,
                'mensagem' => $mensagem
            ),
            array(
                'nome'     => 'required',
                'email'    => 'required|email',
                'mensagem' => 'required'
            )
        );

        if ($validation->fails())
        {
            $response = array(
                'status'  => 'fail',
                'message' => Lang::get('frontend.contato.erro')
            );
            return Response::json($response);
        }

        if (isset($contato->email))
        {
            $data = array(
                'nome' => $nome,
                'email' => $email,
                'telefone' => $telefone,
                'mensagem' => $mensagem
            );

            Mail::send('emails.contato', $data, function($message) use ($data, $contato)
            {
                $message->to('carlezzo@carlezzo.com.br', Config::get('projeto.titulo'))
                        ->subject('[CONTATO] '.Config::get('projeto.titulo'))
                        ->replyTo($data['email'], $data['nome']);
            });
        }

        $object = new ContatoRecebido;
        $object->nome = $nome;
        $object->email = $email;
        $object->telefone = $telefone;
        $object->mensagem = $mensagem;
        $object->save();

        $response = array(
            'status'  => 'success',
            'message' => Lang::get('frontend.contato.sucesso')
        );
        return Response::json($response);
    }

}
