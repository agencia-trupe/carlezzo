@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('telefone', 'Telefone') }}
    {{ Form::text('telefone', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('fax', 'Fax') }}
    {{ Form::text('fax', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('email', 'E-mail') }}
    {{ Form::email('email', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('endereco', 'Endereço') }}
    {{ Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('facebook', 'Facebook') }}
    {{ Form::text('facebook', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('linkedin', 'Linkedin') }}
    {{ Form::text('linkedin', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('youtube', 'YouTube') }}
    {{ Form::text('youtube', null, ['class' => 'form-control']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('google_maps', 'Código Google Maps') }}
    {{ Form::text('google_maps', null, ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}
