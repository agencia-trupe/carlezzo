@section('content')

    <legend>
        <h2>Editar Usuário</h2>
    </legend>

    {{ Form::model($usuario, [
        'route' => ['painel.usuarios.update', $usuario->id],
        'method' => 'patch'])
    }}

        @include('painel.usuarios._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop