@section('content')

    <div class="menu-sub">
            <a href="{{ route('atuacao') }}" class="active">{{ Lang::get('frontend.atuacao.atuacao') }}</a>
            <a href="{{ route('atuacao', 'pratica-legal') }}">{{ Lang::get('frontend.atuacao.pratica') }}</a>
            <a href="{{ route('atuacao', 'o-que-fazemos-para') }}">{{ Lang::get('frontend.atuacao.fazemos') }}</a>
    </div>

    <div class="conteudo-sub atuacao">
        <div class="titulo">
            <h2>{{ Lang::get('frontend.atuacao.atuacao') }}</h2>
        </div>

        <div class="conteudo">
            {{ $atuacao->{Lang::get('frontend.campos.texto')} }}
        </div>
    </div>

@stop
