<?php $login = (Route::currentRouteName() == 'painel.login'); ?>
<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ Config::get('projeto.name') }} - Painel Administrativo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('assets/css/painel/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/painel/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/painel/painel.min.css') }}">
    <script>var BASE = '{{ url() }}'</script>
</head>
<body class="{{ ($login ? 'login' : 'painel') }}">
    @if(!$login)
        @include('painel.common.nav')
    @endif

    <div class="{{ ($login ? 'wrapper' : 'main container') }}">
        @yield('content')
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
    <script src="{{ asset('assets/js/painel/vendor.min.js') }}"></script>
    <script src="{{ asset('assets/js/painel/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/js/painel/painel.js') }}"></script>
</body>
</html>
