<?php

use \Noticia, \Corporativo;

class DuplicaNoticiasController extends Controller {

    public function corporativo()
    {
        $import_data = Corporativo::where('idioma', 'pt')->get();
        foreach($import_data as $i) {
            $titulo = $i->titulo.' [es]';
            $corporativo = new Corporativo;
            $corporativo->idioma = 'es';
            $corporativo->titulo = $titulo;
            $corporativo->slug   = Str::slug($titulo);
            $corporativo->texto  = $i->texto;
            $corporativo->data   = $i->data;
            $corporativo->save();
        }

        return 'ok';
    }

    public function noticias()
    {
        $import_data = Noticia::where('idioma', 'pt')->get();
        foreach($import_data as $i) {
            $titulo = $i->titulo.' [es]';
            $noticia = new Noticia;
            $noticia->idioma = 'es';
            $noticia->titulo = $titulo;
            $noticia->slug   = Str::slug($titulo);
            $noticia->texto  = $i->texto;
            $noticia->data   = $i->data;
            $noticia->save();
        }

        return 'ok';
    }

}
