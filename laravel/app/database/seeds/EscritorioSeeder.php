<?php

class EscritorioSeeder extends Seeder {

    public function run()
    {
        DB::table('escritorio')->delete();

        $data = array(
            array(
                'slug'      => 'nossa-historia',
                'titulo_pt' => 'Nossa história',
                'titulo_en' => 'Our history',
                'titulo_es' => 'Nuestra historia',
            ),
            array(
                'slug'      => 'o-que-esperar-de-nos',
                'titulo_pt' => 'O que esperar de nós',
                'titulo_en' => 'What to expect from us',
                'titulo_es' => 'Qué esperar de nosotros',
            ),
            array(
                'slug'      => 'dubai',
                'titulo_pt' => 'Dubai',
                'titulo_en' => 'Dubai',
                'titulo_es' => 'Dubai',
            ),
            array(
                'slug'      => 'aliancas-internacionais',
                'titulo_pt' => 'Alianças internacionais',
                'titulo_en' => 'International alliances',
                'titulo_es' => 'Alianzas internacionales',
            )
        );

        DB::table('escritorio')->insert($data);
    }

}
