@section('content')

    <legend>
        <h2><small>Prática Legal /</small> Editar Item</h2>
    </legend>

    {{ Form::model($pratica, [
        'route' => ['painel.atuacao.pratica.update', $pratica->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.pratica._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
