<?php
	
	//-------------------- Conecta com o Banco de Dados ---------------------------
	include "../_includes/conexao.php";	
	
	//Busca not�cias
	$busca_noticias = mysql_query("SELECT * FROM noticias WHERE status = '1' AND titulo_en != '0'  AND destaque != '1' ORDER BY data DESC LIMIT 0, 2");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- META TAGS - AG�NCIA 10CLIC - OTIMIZA��O -->
<meta name="author" content="10Clic - www.10clic.com.br" />
<meta name="description" content="Law firm specialized in sports, entertainment and media, with strong presence in football and international affairs, as well as before FIFA and CBF
" />
<meta name="keywords" content="FIFA, UEFA, Court of arbitration for sport, football, player's agents, athletes, clubs, solidarity mechanism, training compensation, transfers, cinema, sports law, music, concerts, models, celebrities, video-game, media, new technologies, intellectual property, advertising, brands, marketing, Internet, TV, radio, mobile, world cup, Olympic games
" />
<meta name="robots" content="index, follow, archive" />
<meta name="googlebot" content="archive" />
<!-- FIM DAS META TAGS -->
<title>Carlezzo Advogados - Where we are</title>
<!-- FOLHAS DE ESTILO -->
<link href="../_css/estilo.css" rel="stylesheet" type="text/css" />
<link href="../_css/nivo-slider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../_ajax/skin.css" rel="stylesheet" type="text/css" />
<!-- FIM DAS FOLHAS DE ESTILO -->
<!-- SCRIPTS -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'49de9f94-6555-4ec3-ba64-437c8a161ebe'});</script>
<link href="../_ajax/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="../_ajax/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../_ajax/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<script src="../_ajax/SpryValidationTextarea.js" type="text/javascript"></script>
<script type="text/javascript" src="../_ajax/jquery.js"></script> 
<script type="text/javascript" src="../_ajax/menu.jquery.js"></script> 
<!-- FIM DOS SCRIPTS -->
<!-- GOOGLE MAPS API -->
 <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAArwSgps1dZZSxHzRot2vCXBQ5uuQU9jSRxFDMOKPSMDGB0mUPkhQ79IfCXc2lGM40QEd6OIjUQ2qgvg" type="text/javascript"></script>
<script type="text/javascript">
    //<![CDATA[
    var WINDOW_HTML = '<div id="mapa_info"><br /><strong>CARLEZZO ADVOGADOS</strong><br /><br />Al. Lorena, 800, 18&ordm; andar<br />Jardins<br />S&atilde;o Paulo - SP.</div>';
	function load() {
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("mapa"));
        map.setCenter(new GLatLng(-23.56831, -46.660854), 16);
		map.addControl(new GSmallMapControl());
      var marker = new GMarker(new GLatLng(-23.56831, -46.660854));
      map.addOverlay(marker);GEvent.addListener(marker, "click", function() {    marker.openInfoWindowHtml(WINDOW_HTML);  });  return marker;
      }
    }
    //]]>
    </script>
<!-- FIM DO GOOGLE MAPS API -->
</head>

<body class="interna" onload="load()" onunload="GUnload()">
<!-- DEFESA -->
<div id="defesa">
  <!-- TOPO -->
  <div id="topo">
  	
    <!-- LINHA -->  
    <div id="linha">
    
    <form action="busca.php" method="post" id="busca">
    <input class="campo" name="palavra" type="text" />
    <input name="" type="image" value="ok" src="../_imagens/botao_buscar.jpg" alt="Search" class="botao" />
    </form>
    <span displayText="" class="st_sharethis_custom"></span>
    <p class="linguas">
    <a href="../pt/<?=basename($_SERVER['PHP_SELF']);?>" title="Portugu&ecirc;s">Portugu&ecirc;s</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" title="Espa&ntilde;ol">Espa&ntilde;ol</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:window.print()" title="Print this page"><img src="../_imagens/a_print.jpg" alt="Print this page" align="absmiddle" /></a>&nbsp;&nbsp;<a href="mailto:carlezzo@carlezzo.com.br" title="Quick contact"><img src="../_imagens/a_mail.jpg" alt="Quick contact" align="absmiddle" /></a>
    </p>    
    </div>
    <!-- FIM DA LINHA -->
    
    <!-- LOGO -->
    <h1><a href="index.php" title="Carlezzo Advogados">Carlezzo Advogados</a></h1>
    <!-- FIM DO LOGO -->
    
    <!-- MENU TOPO -->
    <div id="menu_topo">
    	<ul class="topnav">
            <li><a href="index.php">Home</a></li>
            <li>
                <a href="quem-somos.php">About us</a>

              <ul class="subnav">
                    <li><a href="quem-somos.php">Overview</a></li>
                    <li><a href="missao-e-valores.php">Mission and Values</a></li>
                    <li><a href="o-que-esperar-de-nos.php">What you can wait from us</a></li>
                    <li><a href="aliancas-internacionais.php">International Alliances</a></li>
                    <li><a href="recrutamento.php">Recruitment</a></li>
                    <li><a href="pro-bono.php">Pro-bono</a></li>
              </ul>
          </li>
            <li>
                <a href="setores.php">Sectors</a>
              <ul class="subnav">
                    <li><a href="setores.php?categoria=esporte">Sport</a></li>
     				<li><a href="setores.php?categoria=entretenimento">Entertainment</a></li>
      				<li><a href="setores.php?categoria=midia">Media and New Technologies</a></li>
              </ul>
          </li>
          <li><a href="equipe.php">Team</a></li>

          <li><a href="noticias.php">News</a></li>
          <li><a href="publicacoes.php">Publications</a></li>
          <li class="last"><a href="onde-estamos.php">Where are we</a></li>
      </ul>
    </div>
    <!-- FIM DO MENU TOPO -->
  
  </div>
  <!-- FIM DO TOPO -->
  
  <!-- CONTE�DO -->
  <div id="conteudo">
    
    <!-- ESQUERDA -->
    <div class="esquerda_interna">
    
    <div class="bloco_sidebar">
      <h2 class="titulo">WHERE WE ARE</h2>
      <h3 class="subtitulo">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Our Location</h3>
      <p class="mini_texto">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
        CEP 01.424-001<br />
        Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
        S&atilde;o Paulo/SP - Brasil<br />
        email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
      <p>&nbsp;</p>
      <p><img src="../_imagens/p_mapa.jpg" width="177" height="113" /></p>
    </div>
    
    <!-- CHAMADA -->
    <div class="chamada">
      <img src="../_imagens/chamada_onde.jpg" width="242" height="318" />
    </div>
    <!-- FIM DA CHAMADA -->
    
    
    
    
    <br class="limpa_float" />
    </div>
    <!-- FIM DA ESQUERDA --> 
    
    <!-- DIREITA -->
    <div class="direita_texto">
    
    <!-- TEXTO -->
    <div class="texto">
    <br />
    <p>See the map above: </p>
    <div id="mapa"></div>
    
    <!-- CONTATO -->
    <!--<form id="form_geral" name="form_geral" method="post" action="enviar-contato.php">
    
    <p>Utilize o formul&aacute;rio abaixo para entrar em contato.<br />&nbsp;</p>
    <p><label for="nome">Nome*:</label>
    <span id="spry_nome">
    <input name="nome" type="text" id="nome" maxlength="254" class="textfield" />
    <span class="textfieldRequiredMsg">&nbsp;?&nbsp;</span></span></p>
    
    <p>
      <label for="empresa">Empresa*:</label>
    <span id="spry_empresa">
    <input name="empresa" type="text" id="empresa" maxlength="254" class="textfield" />
    <span class="textfieldRequiredMsg">&nbsp;?&nbsp;</span></span></p>
    
    <p><label for="email">E-mail*:</label>
      <span id="spry_email">
      <input name="email" type="text" id="email" maxlength="254" class="textfield" />
      <span class="textfieldRequiredMsg">&nbsp;?&nbsp;</span><span class="textfieldInvalidFormatMsg">&nbsp;?&nbsp;</span></span></p>
    
    <p><label for="telefone">Telefone:</label>
      <span id="spry_telefone">
      <input name="telefone" type="text" id="telefone" maxlength="254" class="textfield_medio" />
      <span class="textfieldRequiredMsg">&nbsp;?&nbsp;</span><span class="textfieldInvalidFormatMsg">&nbsp;?&nbsp;</span></span></p>
      
      
      <p>
      <label for="assunto">Assunto*:</label>
    <span id="spry_assunto">
    <input name="assunto" type="text" id="assunto" maxlength="254" class="textfield" />
    <span class="textfieldRequiredMsg">&nbsp;?&nbsp;</span></span></p>
      
      <p><label for="mensagem">Mensagem*:</label>
      <span id="spry_mensagem">
      <textarea name="mensagem" id="mensagem" class="textarea"></textarea>
      <span class="textareaRequiredMsg">&nbsp;?&nbsp;</span></span>
      </p>
    
   
      
     
      
	<p class="botao_enviar">*campos obrigat&oacute;rios&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input name="" type="image" src="../_imagens/botao_enviar.jpg" /></p>
    </form> -->
    <!-- FIM DO CONTATO -->
    
    <br class="limpa_float" />
    </div>
    <!-- FIM DO TEXTO -->
    
    </div>
    <!-- FIM DA DIREITA -->  
  
  <br class="limpa_float" />  
  </div>
  <!-- FIM DO CONTE�DO -->
  </div>
  <!-- FIM DA DEFESA -->
  <!-- BASE -->
  <div id="base">
    
    <!-- DEFESA DA BASE -->
    <div id="defesa_base">
      
    <div class="left">
    <p class="endereco">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
      CEP 01.424-001<br />
      Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
      S&atilde;o Paulo/SP - Brasil<br />
      email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
    <p><a href="index.php">Home</a> |   <a href="avisos-legais.php">Legal terms</a> |   <a href="mapa-do-site.php">Sitemap</a> |   <a href="subscreva.php">Subscribe </a></p>
    </div>
    
    <div class="right">
    <ul id="menu_base">
        <li class="acompanhe">Follow us:</li>
        <li class="social">
        <a class="facebook" href="https://www.facebook.com/carlezzo.advogados.associados" title="Facebook" target="_blank">Facebook</a> 
        <a class="linkedin" href="http://www.linkedin.com/company/1294492" title="Linkedin" target="_blank">Linkedin</a> 
        <a class="youtube" href="http://www.youtube.com/user/carlezzo" title="Youtube" target="_blank">Youtube</a>
        </li>
      </ul>
      <br class="limpa_float" /> 
    <a class="assinatura" href="http://www.agencia10clic.com.br" target="_blank" title="Ag&ecirc;ncia 10Clic">Ag&ecirc;ncia 10Clic</a>
    <p>&copy;2011 Carlezzo Advogados. All rights reserved</p>
    </div>
    
    </div>
    <!-- FIM DA DEFESA DA BASE -->
    
  </div>
  <!-- FIM DA BASE -->
<!-- SCRIPT GOOGLE ANALYTICS - AG�NCIA 10CLIC - OTIMIZA��O -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10514770-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script><br />
<!-- FIM DO SCRIPT GOOGLE ANALYTICS -->
</body>
</html>