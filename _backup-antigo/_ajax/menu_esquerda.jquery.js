// Menu categoria
$(function(){
$('#menu_setor ul').hide();
$('#menu_setor ul.selecionado').show();
$('#menu_setor li a').click(
  function() {
	$("#menu_setor li a.ativo").removeClass('ativo');  
	var checkElement = $(this).next();
	$(this).addClass('ativo');
	if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
	  return false;
	  }
	if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
	  $('#menu_setor ul:visible').slideUp('normal');
	  checkElement.slideDown('normal');
	  return false;
	  }
	}
  );
});