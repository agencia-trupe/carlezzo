<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">

    <meta name="description" content="{{ Config::get('projeto.description') }}">
    <meta name="keywords" content="{{ Config::get('projeto.keywords') }}">
    <meta property="og:title" content="{{ Config::get('projeto.title') }}">
    <meta property="og:description" content="{{ Config::get('projeto.description') }}">
    <meta property="og:site_name" content="{{ Config::get('projeto.title') }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:image" content="{{ Config::get('projeto.share_image') }}">

    <title>{{ Config::get('projeto.title') }}</title>

    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <script>var BASE = '{{ url() }}'</script>
</head>
<body>
    <div class="center">
    @include('frontend.common.header')

    @yield('content')
    @include('frontend.common.footer')
    </div>
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-9372384-1', 'auto');
      ga('send', 'pageview');
    </script>
</body>
</html>
