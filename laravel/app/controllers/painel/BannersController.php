<?php

namespace Painel;

use \Banner, \Input, \Validator, \Redirect, \Session, \CropImage;

class BannersController extends BasePainelController {

    private $validation_rules = [
        'imagem'       => 'required|image',
        'cor'          => 'required',
        'link'         => 'required',
        'titulo_pt'    => 'required',
        'titulo_en'    => 'required',
        'titulo_es'    => 'required',
        'texto_pt'     => 'required',
        'texto_en'     => 'required',
        'texto_es'     => 'required',
    ];

    private $image_config = [
        'width'  => 1024,
        'height' => 380,
        'path'   => 'assets/img/banners/'
    ];

    public function index()
    {
        $banners = Banner::ordenados()->get();

        return $this->view('painel.banners.index', compact('banners'));
    }

    public function create()
    {
        return $this->view('painel.banners.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            Banner::create($input);
            Session::flash('sucesso', 'Banner adicionado com sucesso.');

            return Redirect::route('painel.banners.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar banner.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $banner = Banner::findOrFail($id);

        return $this->view('painel.banners.edit', compact('banner'));
    }

    public function update($id)
    {
        $banner = Banner::findOrFail($id);
        $input  = Input::all();

        $this->validation_rules['imagem'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            } else {
                unset($input['imagem']);
            }

            $banner->update($input);
            Session::flash('sucesso', 'Banner alterado com sucesso.');

            return Redirect::route('painel.banners.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar banner.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Banner::destroy($id);
            Session::flash('sucesso', 'Banner removido com sucesso.');

            return Redirect::route('painel.banners.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover banner.']);

        }
    }

}
