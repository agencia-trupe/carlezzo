@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="well form-group">
    {{ HTML::decode(Form::label('imagem', 'Imagem <small>(1020x380px)</small>')) }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$banner->imagem) }}" class="img-responsive">
@endif
    {{ Form::file('imagem', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('cor', 'Cor (formato: r, g, b)') }}
    {{ Form::text('cor', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('link', 'Link') }}
    {{ Form::text('link', null, ['class' => 'form-control']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('titulo_pt', 'Título [PORTUGUÊS]') }}
    {{ Form::text('titulo_pt', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('texto_pt', 'Texto [PORTUGUÊS]') }}
    {{ Form::text('texto_pt', null, ['class' => 'form-control']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('titulo_en', 'Título [INGLÊS]') }}
    {{ Form::text('titulo_en', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('texto_en', 'Texto [INGLÊS]') }}
    {{ Form::text('texto_en', null, ['class' => 'form-control']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('titulo_es', 'Título [ESPANHOL]') }}
    {{ Form::text('titulo_es', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('texto_es', 'Texto [ESPANHOL]') }}
    {{ Form::text('texto_es', null, ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
