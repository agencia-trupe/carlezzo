<?php

namespace Painel;

use \Request, \Input, \DB, \Controller, \CropImage;

class AjaxController extends Controller {

    private $image_config = [
        'width'  => 800,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/noticias/'
    ];

    function order()
    {

        if (!Request::ajax()) return false;

        $menu   = Input::get('data');
        $tabela = Input::get('tabela');

        for ($i = 0; $i < count($menu); $i++) {
            DB::table($tabela)->where('id', $menu[$i])->update(array('ordem' => $i));
        }

        return json_encode($menu);

    }

    public function imageUpload()
    {
        $validator = \Validator::make(Request::all(), [
            'imagem' => 'image|required'
        ]);

        if ($validator->fails()) {
            $response = [
                'error'   => true,
                'message' => 'O arquivo deve ser uma imagem.'
            ];
        } else {
            $imagem   = CropImage::make('imagem', $this->image_config);
            $response = [
                'filepath' => asset($this->image_config['path'] . $imagem)
            ];
        }

        return $response;
    }
}