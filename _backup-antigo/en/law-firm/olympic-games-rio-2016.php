<?php
	
	//-------------------- Conecta com o Banco de Dados ---------------------------
	include "../../_includes/conexao.php";	
	
	//Busca not�cias
	$busca_noticias = mysql_query("SELECT * FROM noticias WHERE status = '1' AND titulo_en != '0'  AND destaque != '1' ORDER BY data DESC LIMIT 0, 2");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- META TAGS - AG�NCIA 10CLIC - OTIMIZA��O -->
<meta name="author" content="10Clic - www.10clic.com.br" />
<meta name="description" content="Law firm specialized in sports, entertainment and media, with strong presence in football and international affairs, as well as before FIFA and CBF
" />
<meta name="keywords" content="FIFA, UEFA, Court of arbitration for sport, football, player's agents, athletes, clubs, solidarity mechanism, training compensation, transfers, cinema, sports law, music, concerts, models, celebrities, video-game, media, new technologies, intellectual property, advertising, brands, marketing, Internet, TV, radio, mobile, world cup, Olympic games
" />
<meta name="robots" content="index, follow, archive" />
<meta name="googlebot" content="archive" />
<!-- FIM DAS META TAGS -->
<title>Carlezzo Advogados - Olympic Games Rio 2016</title>
<!-- FOLHAS DE ESTILO -->
<link href="../../_css/estilo.css" rel="stylesheet" type="text/css" />
<link href="../../_css/nivo-slider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../_ajax/skin.css" rel="stylesheet" type="text/css" />
<!-- FIM DAS FOLHAS DE ESTILO -->
<!-- SCRIPTS -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'49de9f94-6555-4ec3-ba64-437c8a161ebe'});</script>
<script type="text/javascript" src="../../_ajax/jquery.js"></script> 
<script type="text/javascript" src="../../_ajax/menu.jquery.js"></script> 
<!-- FIM DOS SCRIPTS -->
</head>

<body class="interna">
<!-- DEFESA -->
<div id="defesa">
  <!-- TOPO -->
  <div id="topo">
  	
    <!-- LINHA -->  
    <div id="linha">
    
    <form action="../busca.php" method="post" id="busca">
    <input class="campo" name="palavra" type="text" />
    <input name="" type="image" value="ok" src="../../_imagens/botao_buscar.jpg" alt="Search" class="botao" />
    </form>
    <span displayText="" class="st_sharethis_custom"></span>
    <p class="linguas">
    <a href="../pt/<?=basename($_SERVER['PHP_SELF']);?>" title="Portugu&ecirc;s">Portugu&ecirc;s</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" title="Espa&ntilde;ol">Espa&ntilde;ol</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:window.print()" title="Print this page"><img src="../../_imagens/a_print.jpg" alt="Print this page" align="absmiddle" /></a>&nbsp;&nbsp;<a href="mailto:carlezzo@carlezzo.com.br" title="Quick contact"><img src="../../_imagens/a_mail.jpg" alt="Quick contact" align="absmiddle" /></a>
    </p>    
    </div>
    <!-- FIM DA LINHA -->
    
    <!-- LOGO -->
    <h1><a href="../index.php" title="Carlezzo Advogados">Carlezzo Advogados</a></h1>
    <!-- FIM DO LOGO -->
    
    <!-- MENU TOPO -->
    <div id="menu_topo">
    	<ul class="topnav">
            <li><a href="../index.php">Home</a></li>
            <li>
                <a href="../quem-somos.php">About us</a>

          <ul class="subnav">
                    <li><a href="../quem-somos.php">Overview</a></li>
                    <li><a href="../missao-e-valores.php">Mission and Values</a></li>
                    <li><a href="../o-que-esperar-de-nos.php">What you can wait from us</a></li>
                    <li><a href="../aliancas-internacionais.php">International Alliances</a></li>
                    <li><a href="../recrutamento.php">Recruitment</a></li>
                    <li><a href="../pro-bono.php">Pro-bono</a></li>
              </ul>
          </li>
            <li>
                <a href="../setores.php">Sectors</a>
          <ul class="subnav">
                    <li><a href="../setores.php?categoria=esporte">Sport</a></li>
     				<li><a href="../setores.php?categoria=entretenimento">Entertainment</a></li>
      				<li><a href="../setores.php?categoria=midia">Media and New Technologies</a></li>
              </ul>
          </li>
          <li><a href="../equipe.php">Team</a></li>

          <li><a href="../noticias.php">News</a></li>
          <li><a href="../publicacoes.php">Publications</a></li>
          <li class="last"><a href="../onde-estamos.php">Where are we</a></li>
      </ul>
    </div>
    <!-- FIM DO MENU TOPO -->
  
  </div>
  <!-- FIM DO TOPO -->
  
  <!-- CONTE�DO -->
  <div id="conteudo">
    
    <!-- ESQUERDA -->
    <div class="esquerda_interna">
    
    <div class="bloco_sidebar">
      <h2 class="titulo">CARLEZZO ADVOGADOS</h2>
      <ul class="menu_esquerda">
    <li><a href="football.php">Football</a></li>
	<li><a href="fifa.php">FIFA</a></li>
	<li><a href="world-cup-fifa-2014.php">World Cup FIFA 2014</a></li>
	<li><a href="olympic-games-rio-2016.php">Olympic Games Rio 2016</a></li>
	<li><a href="entertainment.php">Entertainment</a></li>
        </ul>
    </div>
    
    
    <!-- CHAMADA -->
    <div class="chamada"><img src="../../_imagens/chamada_quem_somos.jpg" width="242" height="318" /></div>
    <!-- FIM DA CHAMADA -->
        
    <br class="limpa_float" />
    </div>
    <!-- FIM DA ESQUERDA --> 
    
    <!-- DIREITA -->
    <div class="direita_interna">
    
    <!-- TEXTO -->
    <div class="texto">
    <h5 class="square">Olympic Games Rio 2016</h5>
    <p>The 2016 Summer Olympics, officially known as the Games of the XXXI Olympiad, are a major international multi-sport event to be celebrated in the tradition of the Olympic Games, as governed by the International Olympic Committee (IOC). The host city of the Games will be Rio de Janeiro as announced at the 121st IOC Session (which is also the 13th Olympic Congress) held in Copenhagen, Denmark, on October 2, 2009. They are scheduled to be held from August 5 to 21, 2016. The 2016 Summer Paralympics will be held in the same city and organized by the same committee, and are scheduled to be held from September 7 to 18. The Rio 2016 Summer Olympic Games will be the first edition held in South America, the second in Latin America (after Mexico City), the third edition held in the southern hemisphere (the first of those three outside of Australia), and the first Games in a lusophone (Portuguese-speaking) country. All venues of the Olympic and Paralympic Games in 2016 will be located i n four zones: Copacabana Beach, Maracan&atilde;, Deodoro and Barra da Tijuca; the latter will also house the Olympic Village. Besides the Maracan&atilde; Stadium, the football matches will also take place in Salvador (Arena Fonte Nova), S&atilde;o Paulo (Est&aacute;dio do Morumbi), Belo Horizonte (Est&aacute;dio Mineir&atilde;o) and Bras&iacute;lia (Est&aacute;dio Nacional de Bras&iacute;lia). All these stadiums, except for the Morumbi Stadium, will also be used in the FIFA World Cup 2014. The 2016 Summer Olympic program is scheduled to feature 28 sports and a total of 38 disciplines. There were two open spots for sports and initially seven sports began the bidding for inclusion in the 2016 program. Baseball and softball, which were dropped from the program in 2005, karate, squash, golf, roller sports (inline speed skating) and rugby union all applied to be included. Leaders of the seven sports held presentations in front of the IOC executive board in June 2009.[5] In August, the executive board initially gave its approval to rugby sevens&mdash;a seven-man version of rugby union&mdash;by a majority vote, thus removing baseball, roller sports, and squash from contention. Among the remaining three&mdash;golf, karate, and softball&mdash;the board approved golf as a result of consultation. A decision regarding the remaining two sports was made on 9 October 2009, the final day of the 121st IOC Session at which Rio de Janeiro was named as host. A new system was in place at this Session; a sport now needs only a simple majority from the full IOC for approval rather than the two-thirds majority previously required.[6] On October 9, 2009 the IOC voted to include rugby sevens and golf on the program for the Games in Rio. The other 26 sports were also confirmed with a large majority of the votes . The International Olympic Committee (IOC) is an international corporation based in Lausanne, Switzerland, created by Pierre de Coubertin on 23 June 1894 with Demetrios Vikelas as its first president. Today its membership consists of the 205 National Olympic Committees. The IOC organizes the modern Olympic Games held in Summer and Winter, every four years. The first Summer Olympics organized by the International Olympic Committee were held in Athens, Greece, in 1896; the first Winter Olympics were in Chamonix, France, in 1924. Until 1992, both Summer and Winter Olympics were held in the same year. After that year, however, the IOC shifted the Winter Olympics to the even years between Summer Games, to help space the planning of the two events two years apart from one another. e IOC Executive Board The IOC Executive Board consists of the President, four Vice-Presidents and ten other members. All members of the IOC Executive Board are elected by the Session, in a secret ballot, by a majority of the votes cast. The IOC Executive Board assumes the general overall responsibility for the administration of the IOC and the management of its affairs. The IOC Session elects, by secret ballot, the IOC President from among its members for a term of eight years renewable once for a term of four years. The current IOC President, Jacques Rogge, was re-elected for a second term that consists of four years on 9 October 2009.[1] Former President Juan Antonio Samaranch has been elected Honorary President For Life. e IOC Executive Board The IOC Executive Board consists of the President, four Vice-Presidents and ten other members. All members of the IOC Executive Board are elected by the Session, in a secret ballot, by a majority of the votes cast. The IOC Executive Board assumes the general overall responsibility for the administration of the IOC and the management of its affairs. The IOC Session elects, by secret ballot, the IOC President from among its members for a term of eight years renewable once for a term of four years. The current IOC President, Jacques Rogge, was re-elected for a second term that consists of four years on 9 October 2009.[1] Former President Juan Antonio Samaranch has been elected Honorary President For Life. The Olympic Charter, last updated February 11, 2010, is a set of rules and guidelines for the organization of the Olympic Games, and for governing the Olympic Movement. Adopted by International Olympic Committee (IOC), it is the codification of the Fundamental Principles, Rules and By-laws. French and English are the official languages of the Olympic Charter. If, at any time, there is a discrepancy between versions of the text, the French text prevails. Throughout the history of the Olympics, the Olympic Charter has often decided the outcome of Olympic controversy. As expressed in its introduction, the Olympic Charter ser ves 3 main purposes: to establish principles and values of Olympism to serve as IOC law to define the rights and obligations of the 4 main constituents of the Olympic movement: the International Olympic Committee (IOC), the International Federations and the National Olympic Committees, and the Organizing Committees for the Olympic Games.</p>
    <p></p>
<p></p>
<p></p>
    </div>
    <!-- FIM DO TEXTO -->
    
    </div>
    <!-- FIM DA DIREITA -->  
  
  <br class="limpa_float" />  
  </div>
  <!-- FIM DO CONTE�DO -->
  </div>
  <!-- FIM DA DEFESA -->
  <!-- BASE -->
  <div id="base">
    
    <!-- DEFESA DA BASE -->
    <div id="defesa_base">
      
    <div class="left">
    <p class="endereco">Al. Lorena, 800, 18&ordm; andar - Jardins<br />
      CEP 01.424-001<br />
      Tel.:+55 11  3885-6091 Fax.:+55 11 3052-3610 <br />
      S&atilde;o Paulo/SP - Brasil<br />
      email: <a href="mailto:contato@carlezzo.com.br">contato@carlezzo.com.br</a></p>
    <p><a href="../index.php">Home</a> |   <a href="../avisos-legais.php">Legal terms</a> |   <a href="../mapa-do-site.php">Sitemap</a> |   <a href="../subscreva.php">Subscribe </a></p>
    </div>
    
    <div class="right">
    <ul id="menu_base">
        <li class="acompanhe">Follow us:</li>
        <li class="social">
        <a class="facebook" href="https://www.facebook.com/carlezzo.advogados.associados" title="Facebook" target="_blank">Facebook</a> 
        <a class="linkedin" href="http://www.linkedin.com/company/1294492" title="Linkedin" target="_blank">Linkedin</a> 
        <a class="youtube" href="http://www.youtube.com/user/carlezzo" title="Youtube" target="_blank">Youtube</a>
        </li>
      </ul>
      <br class="limpa_float" /> 
    <a class="assinatura" href="http://www.agencia10clic.com.br" target="_blank" title="Ag&ecirc;ncia 10Clic">Ag&ecirc;ncia 10Clic</a>
    <p>&copy;2011 Carlezzo Advogados. All rights reserved</p>
    </div>
    
    </div>
    <!-- FIM DA DEFESA DA BASE -->
    
  </div>
  <!-- FIM DA BASE -->
<!-- SCRIPT GOOGLE ANALYTICS - AG�NCIA 10CLIC - OTIMIZA��O -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-10514770-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script><br />
<!-- FIM DO SCRIPT GOOGLE ANALYTICS -->
</body>
</html>