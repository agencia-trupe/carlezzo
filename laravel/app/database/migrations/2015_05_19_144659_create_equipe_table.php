<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('equipe', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ordem')->default(0);

			$table->string('imagem');
			$table->string('nome');
			$table->string('email');
			$table->string('telefone');

			$table->string('cargo_pt');
			$table->string('cargo_en');
			$table->string('cargo_es');

			$table->text('texto_pt');
			$table->text('texto_en');
			$table->text('texto_es');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('equipe');
	}

}
