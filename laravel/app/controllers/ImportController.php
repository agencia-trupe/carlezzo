<?php

use \Noticia, \Corporativo, \NoticiaImport, \CorporativoImport;

class ImportController extends Controller {

    public function corporativo()
    {
        $import_data = CorporativoImport::all();
        foreach($import_data as $i) {

            if ($i->texto_pt && $i->titulo_pt) {
                $corporativo = new Corporativo;
                $corporativo->idioma = 'pt';
                $corporativo->titulo = $i->titulo_pt;
                $corporativo->slug   = Str::slug($i->titulo_pt);
                $corporativo->texto  = strip_tags($i->texto_pt, '<p><img><a><ul><li><br>');
                $corporativo->data   = substr($i->data, 0, 10);
                $corporativo->save();
            }
            if ($i->texto_en && $i->titulo_en) {
                $corporativo = new Corporativo;
                $corporativo->idioma = 'en';
                $corporativo->titulo = $i->titulo_en;
                $corporativo->slug   = Str::slug($i->titulo_en);
                $corporativo->texto  = strip_tags($i->texto_en, '<p><img><a><ul><li><br>');
                $corporativo->data   = substr($i->data, 0, 10);
                if ($i->titulo_en != $i->titulo_pt)
                    $corporativo->save();
            }
            if ($i->texto_es && $i->titulo_es) {
                $corporativo = new Corporativo;
                $corporativo->idioma = 'es';
                $corporativo->titulo = $i->titulo_es;
                $corporativo->slug   = Str::slug($i->titulo_es);
                $corporativo->texto  = strip_tags($i->texto_es, '<p><img><a><ul><li><br>');
                $corporativo->data   = substr($i->data, 0, 10);
                if ($i->titulo_es != $i->titulo_pt)
                    $corporativo->save();
            }
        }

        return 'ok';
    }

    public function noticias()
    {
        $import_data = NoticiaImport::all();
        foreach($import_data as $i) {

            if ($i->texto_pt && $i->titulo_pt) {
                $noticias = new Noticia;
                $noticias->idioma = 'pt';
                $noticias->titulo = $i->titulo_pt;
                $noticias->slug   = Str::slug($i->titulo_pt);
                $noticias->texto  = strip_tags($i->texto_pt, '<p><img><a><ul><li><br>');
                $noticias->data   = substr($i->data, 0, 10);
                $noticias->save();
            }
            if ($i->texto_en && $i->titulo_en) {
                $noticias = new Noticia;
                $noticias->idioma = 'en';
                $noticias->titulo = $i->titulo_en;
                $noticias->slug   = Str::slug($i->titulo_en);
                $noticias->texto  = strip_tags($i->texto_en, '<p><img><a><ul><li><br>');
                $noticias->data   = substr($i->data, 0, 10);
                if ($i->titulo_en != $i->titulo_pt)
                    $noticias->save();
            }
            if ($i->texto_es && $i->titulo_es) {
                $noticias = new Noticia;
                $noticias->idioma = 'es';
                $noticias->titulo = $i->titulo_es;
                $noticias->slug   = Str::slug($i->titulo_es);
                $noticias->texto  = strip_tags($i->texto_es, '<p><img><a><ul><li><br>');
                $noticias->data   = substr($i->data, 0, 10);
                if ($i->titulo_es != $i->titulo_pt)
                    $noticias->save();
            }
        }

        return 'ok';
    }

}
