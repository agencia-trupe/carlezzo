<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePraticaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pratica', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ordem')->default(0);

			$table->string('titulo_pt');
			$table->string('titulo_en');
			$table->string('titulo_es');

			$table->text('texto_pt');
			$table->text('texto_en');
			$table->text('texto_es');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pratica');
	}

}
