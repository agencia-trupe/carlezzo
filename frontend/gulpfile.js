// Require modules

var env         = require('minimist')(process.argv.slice(2)),
    gulp        = require('gulp'),
    jade        = require('gulp-jade'),
    stylus      = require('gulp-stylus'),
    jeet        = require('jeet'),
    rupture     = require('rupture'),
    koutoSwiss  = require('kouto-swiss');
    jshint      = require('gulp-jshint'),
    uglify      = require('gulp-uglify'),
    concat      = require('gulp-concat'),
    imagemin    = require('gulp-imagemin'),
    gulpif      = require('gulp-if'),
    plumber     = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload;


// Path configuration

var path = {
    development: {
        vhost  : 'carlezzo.dev',
        stylus : './stylus/',
        js     : './js/',
        img    : './img/'
    },

    build: {
        css : '../public/assets/css/',
        js  : '../public/assets/js/',
        img : '../public/assets/img/'
    }
};


// Compile Stylus

gulp.task('cssBuild', function() {
    return gulp.src([path.development.stylus + 'main.styl'])
        .pipe(plumber())
        .pipe(stylus({
            use: [ koutoSwiss(), jeet(), rupture() ],
            compress: !env.d
        }))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({ stream: true }))
});


// Validate JS

gulp.task('jsHint', function() {
    return gulp.src(path.development.js + '**/*.js')
        .pipe(plumber())
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
});


// Uglify and concatenate JS

gulp.task('jsBuild', function() {
    return gulp.src(path.development.js + '**/*.js')
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(gulpif(!env.d, uglify()))
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({ stream: true }))
});


// Optimize images

gulp.task('imageMin', function() {
    return gulp.src(path.development.img + '**/*')
        .pipe(plumber())
        .pipe(imagemin({ progressive: true, interlaced: true }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({ stream: true }))
});


// Watch for file changes

gulp.task('watch', function() {
    gulp.watch(path.development.stylus + '**/*.styl', ['cssBuild']);
    gulp.watch(path.development.js + '**/*.js', ['jsHint', 'jsBuild']);
    gulp.watch(path.development.img + '**/*.{jpg,png,gif}', ['imageMin']);
});


// Start browserSync server

gulp.task('browserSync', function() {
    browserSync({
        proxy: path.development.vhost
    });
});


// Default task

gulp.task('default', ['cssBuild', 'jsHint', 'jsBuild', 'imageMin', 'watch', 'browserSync']);
