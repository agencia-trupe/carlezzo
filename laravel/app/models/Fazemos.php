<?php

class Fazemos extends Eloquent
{

    protected $table = 'fazemos';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
