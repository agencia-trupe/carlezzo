<?php

namespace Painel;

use \Pratica, \Input, \Validator, \Redirect, \Session, \CropImage;

class PraticaController extends BasePainelController {

    private $validation_rules = [
        'titulo_pt'    => 'required',
        'titulo_en'    => 'required',
        'titulo_es'    => 'required',
        'texto_pt'     => 'required',
        'texto_en'     => 'required',
        'texto_es'     => 'required',
    ];

    public function index()
    {
        $pratica = Pratica::ordenados()->get();

        return $this->view('painel.pratica.index', compact('pratica'));
    }

    public function create()
    {
        return $this->view('painel.pratica.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            Pratica::create($input);
            Session::flash('sucesso', 'Item adicionado com sucesso.');

            return Redirect::route('painel.atuacao.pratica.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar item.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $pratica = Pratica::findOrFail($id);

        return $this->view('painel.pratica.edit', compact('pratica'));
    }

    public function update($id)
    {
        $pratica = Pratica::findOrFail($id);
        $input   = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $pratica->update($input);
            Session::flash('sucesso', 'Item alterado com sucesso.');

            return Redirect::route('painel.atuacao.pratica.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar item.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Pratica::destroy($id);
            Session::flash('sucesso', 'Item removido com sucesso.');

            return Redirect::route('painel.atuacao.pratica.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover item.']);

        }
    }

}
