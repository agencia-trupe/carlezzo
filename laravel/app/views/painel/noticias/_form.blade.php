@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('idioma', 'Idioma') }}
    {{ Form::select('idioma', ['' => 'Selecione', 'pt' => 'Português', 'en' => 'Inglês', 'es' => 'Espanhol'], null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('data', 'Data') }}
    {{ Form::text('data', null, ['class' => 'form-control ', 'id' => 'datepicker']) }}
</div>

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('equipe_id', 'Autor (opcional)') }}
    {{ Form::select('equipe_id', ['' => 'Selecione'] + $equipe, null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('texto', 'Texto') }}
    {{ Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'noticia']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

@if (isset($noticia))
<a href="{{ route('painel.noticias.index', ['idioma' => $noticia->idioma]) }}" class="btn btn-default btn-voltar">Voltar</a>
@else
<a href="{{ route('painel.noticias.index') }}" class="btn btn-default btn-voltar">Voltar</a>
@endif
