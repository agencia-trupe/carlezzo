<?php

// Dados para Imagem 1
if (@$_POST['imagem_rel'] == "imagem1") {
$var_imagem = $_POST['imagem_rel'];
$div_imagem = "div_imagem_1";
$print_imagem_nome = "imagem_nome_01";
$print_fade_it = "fadeIt";
}

// Prepara a vari�vel caso o formul�rio tenha sido postado
$arquivo = isset($_FILES["$var_imagem"]) ? $_FILES["$var_imagem"] : FALSE;

$config = array();
// Tamano m�ximo da imagem, em bytes
$config["tamanho"] = 5106883;
// Largura M�xima, em pixels
$config["largura"] = 4800;
// Altura M�xima, em pixels
$config["altura"] = 3600;
// Diret�rio onde a imagem ser� salva
$config["diretorio"] = "../../_imagens/publicacoes/";
// Diret�rio onde a imagem ser� movida por ftp
$config["ftp_dir"] = "http://carlezzo.netguestdns.com.br/_imagens/publicacoes/";
// Gera um nome para a imagem e verifica se j� n�o existe, caso exista, gera outro nome e assim sucessivamente..
// Fun��o Recursiva para gerar nome unico
function nome($extensao)
{
    global $config;

    // Gera um nome �nico para a imagem
    $temp = substr(md5(uniqid(time())), 0, 10);
    $imagem_nome = $temp . "." . $extensao;
    
    // Verifica se o arquivo j� existe, caso positivo, chama essa fun��o novamente
    if(file_exists($config["ftp_dir"] . $imagem_nome))
    {
        $imagem_nome = nome($extensao);
    }

    return $imagem_nome;
}

// Fun��o para Redimensionar
function make_thumb ($img_src, $img_th, $tb_sizee, $tb_qualit) {
	$gd_version = 2;
	$thumb_on = 'x';
	$thumb_size = $tb_sizee;
	$quality = $tb_qualit;
				
	$img_size = getimagesize($img_src);
	$img_in = imagecreatefromjpeg($img_src);
				
	if ($thumb_on == 'y') {
		$img_x = ($thumb_size/$img_size[1]) * $img_size[0];
		$img_y = $thumb_size;
	} else {
		$img_y = ($thumb_size/$img_size[0]) * $img_size[1];
		$img_x = $thumb_size;
	}
				
	if ($gd_version == '1') {
		$img_out = imagecreate($img_x, $img_y);
		imagecopyresized($img_out, $img_in, 0, 0, 0, 0, $img_x, $img_y, $img_size[0], $img_size[1]);
	} elseif ($gd_version == '2') {
		$img_out = ImageCreateTrueColor($img_x, $img_y);
		imagecopyresampled($img_out, $img_in, 0, 0, 0, 0, $img_x, $img_y, $img_size[0], $img_size[1]);
	}
				
	imagejpeg($img_out, $img_th, $quality);
	imagedestroy($img_out);
	imagedestroy($img_in);
}

if($arquivo)
{
   
    if(!eregi("^image\/(pjpeg|jpeg)$", $arquivo["type"]))
    { $erro = 1;
	?>
    <script type="text/javascript">
	window.alert('      Arquivo em formato inv�lido!\nA imagem deve possuir extens�o ".jpg"    \n               Envie outro arquivo.');
    </script>
    <?
    }
    else
    {
        // Verifica tamanho do arquivo
        if($arquivo["size"] > $config["tamanho"])
       { $erro = 1;
	?>
    <script type="text/javascript">
	window.alert(' Arquivo em tamanho muito grande!\nA imagem deve ter no m�ximo 5 MB.\n                Envie outro arquivo.');
    </script>
    <?
       }
        
        // Para verificar as dimens�es da imagem
        $tamanhos = getimagesize($arquivo["tmp_name"]);
        
        // Verifica largura
        if($tamanhos[0] > $config["largura"])
        {	$erro = 1;	
		?>
		<script type="text/javascript">
		window.alert('A largura da imagem n�o deve ultrapassar 4800 pixels.\n                         Envie outro arquivo.');
		</script>
		<?
		}

        // Verifica altura
        if($tamanhos[1] > $config["altura"])
        { $erro = 1;
		?>
		<script type="text/javascript">
		window.alert('A altura da imagem n�o deve ultrapassar 3600 pixels.\n                     Envie outro arquivo.');
		</script>
		<?
		}
    }

    if(!@$erro)
    {
        // Pega extens�o do arquivo, o indice 1 do array conter� a extens�o
        preg_match("/\.(jpg|jpeg){1}$/i", $arquivo["name"], $ext);
        
        // Gera nome �nico para a imagem
        $imagem_nome = nome($ext[1]);

        // Caminho de onde a imagem ficar�
        $imagem_dir = $config["diretorio"] . $imagem_nome;

        // Faz o upload da imagem
        move_uploaded_file($arquivo["tmp_name"], $imagem_dir);
		
		// Redimensiona
		make_thumb($imagem_dir, $imagem_dir, '600', '100');
    }
}

if (!@$erro) {
// Mostra a imagem
echo('<div style="width:300px; height:220px; overflow:hidden; text-align:center;">
<img src="../_imagens/publicacoes/'.$imagem_nome.'" height="100%" />
</div>
<div style="width:300px; height:18px; padding-top:2px; float:left; background-color:#EEEEEE; text-align:center">
<a href="#" style="text-decoration:none; color:#3300CC; cursor:pointer;" onclick="'.$print_fade_it.'(\''.$div_imagem.'\',\'_includes/remover_imagem_um.php?imagem='.$imagem_nome.'&id='.$print_imagem_nome.'&gatilho='.$_POST['imagem_rel'].'\'); return false;">Remover/Alterar</a>
<input name="'.$print_imagem_nome.'" type="hidden" value="'.$imagem_nome.'" />
</div>');
} else {
echo('<div style="width:300px; height:220px; overflow:hidden; text-align:center;">
<img src="_imagens/erro.gif" alt="Erro"/>
</div>
<div style="width:300px; height:18px; padding-top:2px; float:left; background-color:#EEEEEE; text-align:center">
<a href="#" style="text-decoration:none; color:#3300CC; cursor:pointer;" onclick="'.$print_fade_it.'(\''.$div_imagem.'\',\'_includes/remover_imagem_um.php?id='.$print_imagem_nome.'&gatilho='.$_POST['imagem_rel'].'\'); return false;">Voltar</a>
</div>');
}
?>