<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banners', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ordem')->default(0);
			$table->string('imagem');
			$table->string('link');
			$table->string('cor');

			$table->string('titulo_pt');
			$table->string('titulo_en');
			$table->string('titulo_es');

			$table->string('texto_pt');
			$table->string('texto_en');
			$table->string('texto_es');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banners');
	}

}
