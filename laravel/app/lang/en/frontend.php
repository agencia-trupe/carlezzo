<?php

return array(

    'campos' => [
        'titulo' => 'titulo_en',
        'texto'  => 'texto_en',
        'cargo'  => 'cargo_en'
    ],

    'menu' => [
        'escritorio' => 'The Firm',
        'atuacao'    => 'What we do',
        'equipe'     => 'Team',
        'noticias'   => 'News',
        'contato'    => 'Contact'
    ],

    'footer' => [
        'copyright' => 'All rights reserved',
        'criacao'   => 'Sites',
        'voltar'    => 'back'
    ],

    'home' => [
        'mais'    => 'More',
        'vermais' => 'see more'
    ],

    'atuacao' => [
        'atuacao' => 'What we do',
        'pratica' => 'Legal Practice',
        'fazemos' => 'What we do for'
    ],

    'noticias' => [
        'noticias'    => 'News',
        'corporativo' => 'Corporate',
        'leiamais'    => 'Read more',
        'por'         => 'by',
        'nenhum'      => 'No record found.'
    ],

    'contato' => [
        'nome'     => 'Name',
        'email'    => 'E-mail',
        'telefone' => 'Phone',
        'mensagem' => 'Message',
        'enviar'   => 'Send',
        'erro'     => 'Please fill all the fields correctly.',
        'sucesso'  => 'Message sent successfully.'
    ],

    'busca' => [
        'resultados' => 'Search results for',
        'nenhum'     => 'No results found.'
    ]

);
